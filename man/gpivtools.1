.TH GPIVTOOLS 1 "26 March 2008"
.SH NAME
gpivtools \- The PIV tool suite.

\fP

.SH DESCRIPTION

\fBgpivtools\fP is a set of command-line driven programs for the
so-called Digital Particle Image Velocimetry (DPIV). It contains
programs for image recording, processing and analysing (resulting into
estimators of particle displacement, i.e. PIV data), PIV data
validation programs and post-processing tools for manipulating and
calculating (scalar) derivatives of the PIV data.

Though the command-line driven tools are mainly intended for
non-interactive processing, its outputs may directly be visualized in
a graphical way with the aid of gnuplot.

The parameters and options to be used for most of the \fBgpivtools\fP
are provided by Libgpiv. They are subsequently searched in
\fI./gpivrc\fP at local directory, at \fI$HOME/.gpivrc\fR (hidden) or
at the system-wide configuration file \fIgpiv.conf\fR, which is mostly
found at \fI/etc\fR for UNIX-like operating systems. Each parameter is
described by the process key: \fBGENPAR\fR (general parameters),
\fBCAM\fR (camera settings), \fBTRIG\fR (trigger settings), \fBIMG\fR
(image header), \fBIMGPROC\fR (image processing), \fBPIV\fR (piv
interrogation), \fBVALID\fR (piv data validation) or \fBPOST\fR (piv
post processing) and the parameter name, separated by a dot (.),
followed by its value. Some of the parameters are optional. The
parameters may be defined in arbitrary order. Blank lines and comment
(starting with a pound sign (#) at the first column) may be included
in the parameter files.  The parameters may be overruled by the
command line options of each tool. Libgpiv provides default parameter
values if they are not defined in one of the configuration files or by
an option key at the command line.

In case stdin and stdout is used, verbose output of the programs is
suppressed in order to avoid contamination of the output data. If
\fIfilename\fR is used for overruling \fIstdin\fR and \fIstdout\fR,
the parameters are written to \fIfilename\fB.par\fR. By renaming this
file to \fI./gpivrc\fR or \fI$HOME/.gpivrc\fR, the parameters may
directly be re-used for identic processing of other images and PIV
data. The parameters defined in \fI$HOME/.gpivrc\fR and in
\fI/etc/gpiv.conf\fR are also used by the Graphic User Interface
program \fBgpiv\fR.

The programs understand different image formats: Portable Network
Graphics (\fIfilename\fB.png\fR), HDF5 (\fIfilename\fB.hdf\fR), raw
binary data (\fIfilename\fB.r\fR) that is accompanied by an ASCII
header file (\fIfilename\fB.h\fR), and LaVision's (tm) uncompressed
image format (\fIfilename\fB.img\fR). Other formats that use lossless
compression (TIF, GIF, PGM, BMP) are converted to PNG on the fly. For
interrogation of double-framed images using cross-correlation, the
second image has to be concatenated after the first one, if this has
not already been done by the recording camera. This might be performed
by \fBgpiv_combing\fR. Image parameters are read from the header or
from the configuration resources (containing the \fBIMG\fR key).


.SH GPIV-TOOLS COMMANDS

Here is the complete list of available \fBgpivtools\fP programs. See their
individual man pages for a more extended description.

 
.TP 
\fBgpiv_aint\fR
Calculates mean image intensity at each interrogation area.
This program uses the \fBIMG\fR, \fBPIV\fR and \fBPOST\fR parameters.

.TP 
\fBgpiv_combing\fR
Combines two images into one image file for cross-correlation.
This program uses the \fBIMG\fR parameters.

.TP
\fBgpiv_errvec\fR
Searches the erroneous vectors in a PIV data file and eventually 
substitutes them with new values.
This program uses the \fBPIV\fR and \fBPOST\fR parameters.

.TP
\fBgpiv_fi-keyline\fR
Filters each line that starts with a keyword.
This program does not use the parameter resources from Libgpiv.

.TP  
\fBgpiv_hdf2piv\fR
Converts hdf5 PIV data (extension .hdf) to ASCII data.
This program does not use the parameter resources from Libgpiv.

.TP 
\fBgpiv_imgproc\fR, \fBgpiv_mktestimg\fR, \fBgpiv_smooth\fR, \fBgpiv_hilo\fR, \fBgpiv_clip\fR, \fBgpiv_fft\fR, \fBgpiv_invfft\fR, \fBgpiv_lowpass\fR, \fBgpiv_highpass\fR, \fBgpiv_getbit\fR, \fBgpiv_mktestimg\fR
(PIV) image processing programs.
These programs use the \fBIMGPROC\fR parameters.

.TP
\fBgpiv_img2gpiv\fR
Depreciated: use \fBgpiv_combing\fR instead. Converts images into raw
data (extension \fB.r\fR) format with the belonging ASCII header
(extension \fB.h\fR) or into hdf format (with extension
\fB.hdf\fR). This program does not use the parameter resources from
Libgpiv.

.TP
\fBgpiv_manipiv\fR, \fBgpiv_fasty\fR, \fBgpiv_flipx\fR, \fBgpiv_flipy\fR, \fBgpiv_revert\fR, \fBgpiv_rot\90\fR, \fBgpiv_rot\180\fR
Simple data manipulation tool for PIV data.
These programs use the \fBPOST\fR parameters.

.TP
\fBgpiv_peaklck\fR
Tests PIV data on the so-called peak-locking effect by
printing/displaying an histogram of the particle displacements at
sub-pixel level.
This program uses the \fBVALID\fR parameters.

.TP
\fBgpiv_piv2gnuplt\fR
Converts PIV data gnuplot data. Besides this, image interrogation and
post-processing programs are able to display their results directly
(mostly with the \fB-g\fR option). This program does not use the
parameter resources from Libgpiv.

.TP
\fBgpiv_piv2grid\fR
Converts PIV data to grid data for generating contour plots with Plotmtv.
This program does not use the parameter resources from Libgpiv.

.TP
\fBgpiv_piv2hdf\fR
Converts ASCII PIV data to hdf5 formatted data (extension \fB.hdf\fR).  
This program does not use the parameter resources from Libgpiv.

.TP
\fBgpiv_piv2vec \fR
Adds header to PIV data for a vector plot with Plotmtv.
This program does not use the parameter resources from Libgpiv.

.TP
\fBgpiv_process-chain\fR
Processes a pipeline of Gpiv-tool command's, forming process a chain
from image recording, image processing, image interrogation, data
validation, data post-processing.  This program does not use the
parameter resources from Libgpiv.

.TP
\fBgpiv_recimg\fR
captures images from a IIDC-compliant CCD camera with IEE1394 connection.
This program uses the \fBIMG\fR parameters.

.TP
\fBgpiv_rr\fR
Interrogates images in order to obtain displacement estimators 
of particles for (Digital) Particle Image Velocimetry.
This program uses the \fBPIV\fR and \fBVALID\fR parameters.

.TP
\fBgpiv_sca2gri\fR
Converts scalar data from gpiv to grid data for contour plotting with Plotmtv
This program does not use the parameter resources from Libgpiv.

.TP
\fBgpiv_scale\fR
Spatial and time scaling program for PIV data.
This program uses the \fBIMG\fR and \fBPOST\fR parameters.

.TP 
\fBgpiv_series\fR
Script for (parallel) processing a series of numbered files.
This program does not use the parameter resources from Libgpiv.

.TP
\fBgpiv_suta\fR
Subtracts time-avaraged velocities (local mean) from the PIV estimators.
This program does not use the parameter resources from Libgpiv.

.TP
\fBgpiv_s-avg\fR
Spatial statistics of PIV data.
This program uses the \fBPOST\fR parameters.

.TP
\fBgpiv_t-avg \fR 
Calculates time-avaraged mean and rms from a series of PIV data.
This program does not use the parameter resources from Libgpiv.

.TP
\fBgpiv_t-avg-img\fR 
Calculates time-averaged values from a series of images at each pixel.
This program uses the \fBGENPAR\fR parameters.

.TP
\fBgpiv_t-corr\fR
Calculates the velocity correlation as function of time 
(Eulerian correlation) from a series PIV data sets
This program does not use the parameter resources from Libgpiv.

.TP
\fBgpiv_trig\fR
Triggers a (double Nd_YAGG) laser on a CCD camera.
This program uses the \fBTRIG\fR parameters.

.TP
\fBgpiv_uhisto\fR
Tests PIV data by printing/displaying an histogram of
the horizontal particle displacements.
This program uses the \fBVALID\fR parameters.

.TP
\fBgpiv_vhisto\fR
Tests PIV data by printing/displaying an histogram of
the vertical particle displacements.
This program uses the \fBVALID\fR parameters.

.TP
\fBgpiv_vorstra\fR, \fBgpiv_vorty\fR, \fBgpiv_nstrain\fR, \fBgpiv_sstrain\fR
Calculates the differential quantities vorticity, shear strain and
normal strain from PIV data.
These programs use the \fBPOST\fR parameters.

.SH SEE ALSO
gpiv, gpiv_control

.SH AUTHOR
Gerber Van der Graaf
