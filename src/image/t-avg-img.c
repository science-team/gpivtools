/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*---------------------------------------------------------------------------
   t-avg-img: calculates time-averaged values from a series if images

   Copyright (C) 2002, 2003, 2004 Gerber van der Graaf

   T-avg-img is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



TODO:

BUGS:

RCS version control
   $Log: t-avg-img.c,v $
   Revision 2.11  2008-10-09 14:45:01  gerber
   paralellized with OMP and MPI

   Revision 2.10  2008-09-25 13:08:34  gerber
   Changed RCID to argv[0] in (error) messages

   Revision 2.9  2008-04-28 12:09:30  gerber
   hdf-formatted files are now with .hdf extension (previously: .gpi)

   Revision 2.8  2007-11-23 16:18:55  gerber
   release 0.5.0: Kafka

   Revision 2.7  2007-01-29 14:26:54  gerber
   Added image formats png, gif, tif, pgm, bpm. Added gpiv_aint

   Revision 2.6  2006/01/31 14:18:03  gerber
   version 0.3.2

   Revision 2.5  2005/06/15 09:32:20  gerber
   check on memory leacks: change of internal structure; gpiv_alloc_pivdata and gpiv_free_pivdata in main optimizing by printing progress only if percentage has been changed

   Revision 2.4  2005/02/26 16:29:19  gerber
   parameter flags (parameter_logic) defined as gboolean, added scale_type to struct __GpivPostPar for inverse scaling

   Revision 2.3  2004/10/15 10:11:41  gerber
   GPIV_ and Gpiv prefix to defines and structure names of libgpiv

   Revision 2.2  2004/06/14 14:39:26  gerber
   addied image filtering and processing program

   Revision 2.1  2003/03/11 21:41:03  gerber
   GePeLled

   Revision 2.0  2002/12/03 12:44:40  gerber
   update version number

   Revision 1.1  2002/12/02 23:36:46  gerber
   init



------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <glib.h>
#include <gpiv.h>

#define PARFILE "t-avg-img.par"	/* Parameter file name */
#define USAGE "\
Usage: imgt-avg [-dx] [-fb file] [-ff int] [fl int] [-h] [-p] \n\
[-s] [-v] < stdin > stdout \n\
\n\
keys: \n\
\n\
-fb file:     file base-name (without .number.r extension) instead of stdin \n\
              and stdout \n\
-ff N:        number of first image file \n\
-fl N:        number of last image file \n\
-fx:          prefix numbering to file base name \n\
-p:           print parameters to stdout \n\
-s:           subtract mean from input data  \n\
-v:           version number \n\
"

/*
 * For the moment: only from a single directory
 */
/* [-db dir] [-df int] [-dl int] \n\ */
/* db dir        directory base- name (without ending number) \n\ */
/* df N          number of first direcory \n\ */
/* dl N          number of first direcory \n\ */
/* dx             prefix numbering to directory base name \n\ */

#define HELP  "\
Calculates time-averaged intensities from a series of images for each pixel. \n\
Images should be numbered at the start or at the end of its name"

#define RCSID "$Id: t-avg-img.c,v 2.11 2008-10-09 14:45:01 gerber Exp $"

/*---------- Global variables ---------------------------------------------*/
gboolean fname__set = FALSE;
gboolean verbose = FALSE;


/*
 * Function prototypes 
 */

void 
command_args (int argc, char *argv[], 
             char fname[GPIV_MAX_CHARS],
             GpivGenPar *gen_par,
             GpivImageProcPar *image_proc_par
             )
/* ----------------------------------------------------------------------------
 * Command line argument handling
 */
{
    char c;
    int argc_next;
    while (--argc > 0 && (*++argv)[0] == '-') {
	argc_next = 0;
/*
 * argc_next is set to 1 if the next cmd line argument has to be searched for; 
 * in case that the command line argument concerns more than one char or cmd 
 * line argument needs a parameter
 */
	while (argc_next == 0 && (c = *++argv[0])) {
	    switch (c) {
	    case 'v':
/*
 * Use Revision Control System (RCS) for version
 */
		printf("\n%s\n", RCSID);
		exit(0);
		break;
	    case 'h':
		printf("\n%s", argv[0]);
		printf("\n%s", HELP);
		printf("\n%s", USAGE);
		exit(0);
		break;

/*
 * direcory name and numbers
 */
	    case 'd':
	      if (strcmp("db", *argv) == 0) {
		strcpy(fname, *++argv);
		fname__set = TRUE;
		argc_next = 1;
		--argc;
	      } else if (strcmp("df", *argv) == 0) {
		    gen_par->first_dir = atoi(*++argv);
		    gen_par->first_dir__set = TRUE;
		    --argc;
		    argc_next = 1;
	      } else if (strcmp("dl", *argv) == 0) {
		    gen_par->last_dir = atoi(*++argv);
		    gen_par->last_dir__set = TRUE;
		    --argc;
		    argc_next = 1;
	      } else if (strcmp("dx", *argv) == 0) {
		   gen_par->dir_prefix = TRUE;
	      }
		break;
/*
 * file name and numbers
 */
	    case 'f':
	      if (strcmp("fb", *argv) == 0) {
		strcpy(fname, *++argv);
		fname__set = TRUE;
		argc_next = 1;
		--argc;
	      } else if (strcmp("ff", *argv) == 0) {
		    gen_par->first_file = atoi(*++argv);
		    gen_par->first_file__set = TRUE;
		    --argc;
		    argc_next = 1;
	      } else if (strcmp("fl", *argv) == 0) {
		    gen_par->last_file = atoi(*++argv);
		    gen_par->last_file__set = TRUE;
		    --argc;
		    argc_next = 1;
	      } else if (strcmp("fx", *argv) == 0) {
		   gen_par->file_prefix = TRUE;
	      }
		break;


	    case 'p':
		    verbose = TRUE;
		break;

/*
 * subtract mean from input data
 */
	    case 's':
		    image_proc_par->smooth_operator = GPIV_IMGOP_SUBTRACT;
		break;

	    default:
		gpiv_error("%s: unknown option: %s", argv[0], *argv);
		break;
	    }
	}
    }

    if (argc != 0) {
	gpiv_error("%s: unknown argument: %s", argv[0], *argv);
    }


}



void 
make_fname_out (gchar *fname_base, 
                gchar *fname_header, 
                gchar *fname_parameter,
                gchar *fname_out
                )
/*-----------------------------------------------------------------------------
 * generates filenames for output
 */
{

  char f_dum[GPIV_MAX_CHARS];
    if (fname__set == FALSE) gpiv_error("%s: Filename has to be set", RCSID);

    gpiv_io_make_fname(fname_base, GPIV_EXT_HEADER, fname_header);
    if (verbose) printf("# Data parameter file: %s\n", fname_header);

    gpiv_io_make_fname(fname_base, GPIV_EXT_PAR, fname_parameter);
    if (verbose) printf("# Data parameter file: %s\n", fname_parameter);

    gpiv_io_make_fname(fname_base, GPIV_EXT_TA, f_dum);
    gpiv_io_make_fname(f_dum, GPIV_EXT_RAW_IMAGE, fname_out);
    if (verbose) printf("# Output file: %s\n", fname_out);

}



void 
make_fname_in (GpivGenPar *gen_par,
	       gchar *fname_base, 
	       int f_number, 
               gchar *fname_in
               )
/*-----------------------------------------------------------------------------
 * generates filenames for input
 */
{
  char f_dum[GPIV_MAX_CHARS];
    if (fname__set == FALSE) gpiv_error("%s: Filename has to be set", RCSID);

    if (gen_par->file_prefix) {
        snprintf(f_dum, GPIV_MAX_CHARS, "%d%s", f_number, fname_base);
        gpiv_io_make_fname(f_dum, GPIV_EXT_RAW_IMAGE, fname_in);
    } else {
        snprintf(f_dum, GPIV_MAX_CHARS, "%s%d", fname_base, f_number);
        gpiv_io_make_fname(f_dum, GPIV_EXT_RAW_IMAGE, fname_in);
    }
      
    if (verbose) printf("# Input file: %s\n", fname_in);
     fprintf(stderr, "MAKE_FNAME_IN:: leaving\n");

}



GpivImage *
imgproc_mean (GpivGenPar *gen_par, 
              GpivImageProcPar *image_proc_par, 
              gchar *fname_base
              )
/*-----------------------------------------------------------------------------
 */
{
/*     FILE *fp = NULL; */

     guint /* i, */ j, k, l;
     guint first_file = gen_par->first_file;
     guint last_file = gen_par->last_file;

    gchar fname_in[GPIV_MAX_CHARS];

     GpivImage *image_sum = NULL, *image_mean = NULL, *image_in = NULL;


     if (verbose) printf ("# Calculating mean from input data\n");
     
/*
 * For the moment: only from single directory and only mean value
 */
/*     for (i = first_dir; i <= last_dir; i++) { */

     for (j = first_file; j <= last_file; j++) {
         if (fname__set == TRUE) {
             make_fname_in (gen_par, fname_base, j, fname_in);
             if ((image_in = gpiv_fread_image (fname_in)) == NULL) {
                 gpiv_error ("local_image_mean: failing gpiv_fread_image %s", 
                             fname_in);
             }
         } else {
             if ((image_in = gpiv_read_png_image (stdin)) == NULL) {
                 gpiv_error ("local_image_mean: failing gpiv_read_png_image %s", 
                             fname_in);
             }
         }
         
/*
 * Allocate image_sum when image dimensions are known
 * Check image dimensions for all subsequent images
 */
         if (j == first_file) {
             if ((image_sum = gpiv_alloc_img (image_in->header)) == NULL) {
                 gpiv_error ("local_image_mean: failing gpiv_alloc_img for image_sum");
             }
         } else {
             if (image_in->header->ncolumns != image_sum->header->ncolumns
                 || image_in->header->nrows != image_sum->header->nrows
                 || image_in->header->depth != image_sum->header->depth
                 || image_in->header->x_corr != image_sum->header->x_corr
                 ) {
                 gpiv_error ("local_image_mean: in-and output images are of different dimensions");
             }
         }

	 for (k = 0; k < image_in->header->nrows; k++) {
             for (l = 0; l < image_in->header->ncolumns; l++) {
                 image_sum->frame1[k][l] += image_in->frame1[k][l];
                 if (image_sum->header->x_corr == TRUE) {
                     image_sum->frame2[k][l] += image_in->frame2[k][l];
                 }
             }
	 }
     }


     if ((image_mean = gpiv_alloc_img (image_sum->header)) == NULL) {
         gpiv_error ("local_image_mean: failing gpiv_alloc_img for image_mean");
     }

     for (k = 0; k < image_in->header->nrows; k++) {
	 for (l = 0; l < image_in->header->ncolumns; l++) {
             gfloat mean = 0.0;
             mean = (gfloat) image_sum->frame1[k][l] / 
                 (gfloat) (last_file - first_file + 1);
             image_mean->frame1[k][l] = (guint16) mean;
             if (image_sum->header->x_corr == TRUE) {
                 mean = 0.0;
                 mean = (gfloat) image_sum->frame2[k][l] / 
                     (gfloat) (last_file - first_file + 1);
                 image_mean->frame2[k][l] = (guint16) mean;
             }
	 }
     }


     gpiv_free_img (image_in);
     gpiv_free_img (image_sum);
     return image_mean;
}




static gchar *
imgproc_subtract (GpivGenPar *gen_par, 
                  gchar *fname_base,
                  GpivImage *image_subtr
                  )
/*-----------------------------------------------------------------------------
 */
{
    gchar *err_msg = NULL;
    FILE *fp = NULL;
    gchar f_dum1[GPIV_MAX_CHARS] = "";
    gchar f_dum2[GPIV_MAX_CHARS] = "";
    gchar fname_out[GPIV_MAX_CHARS] = "";

    guint j, k, l;
/*     guint16 **img_in1 = NULL, **img_in2 = NULL; */
    guint first_file = gen_par->first_file;
    guint last_file = gen_par->last_file;

    guint nrows = image_subtr->header->nrows;
    guint ncolumns = image_subtr->header->ncolumns;
    gchar fname_in[GPIV_MAX_CHARS];

    GpivImage *image_in = NULL;


    if (verbose) printf ("# Subtracting mean from input data\n");

    for (j = first_file; j <= last_file; j++) {

        if (fname__set == TRUE) {
            make_fname_in (gen_par, fname_base, j, fname_in);
            if ((image_in = gpiv_fread_image (fname_in)) == NULL) {
                err_msg = "img_subtract: failing gpiv_fread_image";
                return (err_msg);
            }
        } else {
            if ((image_in = gpiv_read_png_image (stdin)) == NULL) {
                err_msg = "img_subtract: failing gpiv_read_png_image";
                return (err_msg);
            }
        }
         
        for (k = 0; k < nrows; k++) {
            for (l = 0; l < ncolumns; l++) {
                image_in->frame1[k][l] -= image_subtr->frame1[k][l];
                if (image_in->header->x_corr == TRUE) {
                    image_in->frame2[k][l] -= image_subtr->frame2[k][l];
                }
            }
        }


        if (fname__set == TRUE) {
            snprintf (f_dum1, GPIV_MAX_CHARS, "%s%d", fname_base, j);
            gpiv_io_make_fname (f_dum1, GPIV_EXT_TA, f_dum2);
            gpiv_io_make_fname (f_dum2, GPIV_EXT_PNG_IMAGE, fname_out);
            if ((fp = fopen (fname_out, "wb")) == NULL) {
                err_msg = "img_subtract: unable to open file";
                return (err_msg);
            }
            gpiv_write_png_image (fp, image_in, TRUE);
            fclose (fp);

            if (verbose) printf ("# Output file: %s\n", fname_out);
            snprintf (f_dum1, GPIV_MAX_CHARS, " ");;
            snprintf (f_dum2, GPIV_MAX_CHARS, " ");;
            snprintf (fname_out, GPIV_MAX_CHARS, " ");;

        } else {
            gpiv_write_png_image (stdout, image_in, TRUE);
        }

    }
    
    return (err_msg);
}



int 
main (int argc, 
      char *argv[]
      )
/* ----------------------------------------------------------------------------
 * main routine to calculates time-averaged image intensity
 */
{
    FILE *fp = NULL, *fp_par_dat = NULL;
    gchar fname_base[GPIV_MAX_CHARS], 
        fname_header[GPIV_MAX_CHARS],
        fname_out[GPIV_MAX_CHARS],
	fname_parameter[GPIV_MAX_CHARS];
    GpivImage *image_mean = NULL;
    GpivGenPar *gen_par = g_new0 (GpivGenPar, 1);
    GpivImageProcPar *image_proc_par = g_new0 (GpivImageProcPar, 1);


/*
 * Image processing parameter initialization
 */
    gpiv_genpar_parameters_set (gen_par, FALSE);
    gpiv_imgproc_parameters_set (image_proc_par, FALSE);
    command_args (argc, argv, fname_base, gen_par, image_proc_par);
    if (verbose) {
        printf ("# %s\n# Command line options:\n", RCSID);
        gpiv_genpar_print_parameters (stdout, gen_par);
        gpiv_imgproc_print_parameters (stdout, image_proc_par);
    }


    if (fname__set == TRUE) {
/* 
 * Generating proper filenames 
 */
	make_fname_out (fname_base, fname_header, fname_parameter, fname_out);
	if (verbose) 
            printf ("\n# Parameters written to: %s", fname_parameter);


/* 
 * Prints command line parameters to par-file 
 */
	if ((fp_par_dat = fopen (fname_parameter, "a")) == NULL) {
	    gpiv_error ("%s: failure opening %s for input",
                       argv[0], fname_parameter);
	}
	fprintf (fp_par_dat, "\n\n# %s\n# Command line options:\n", argv[0]);
	gpiv_genpar_print_parameters (fp_par_dat, gen_par);
	gpiv_imgproc_print_parameters (fp_par_dat, image_proc_par);

/*
 *  Reading parametes from PARFILE (and writing to data par-file)
 */
        gpiv_scan_parameter (GPIV_GENPAR_KEY, PARFILE, gen_par, verbose);
        gpiv_scan_resourcefiles (GPIV_GENPAR_KEY, gen_par, verbose);
        gpiv_genpar_print_parameters (fp_par_dat, gen_par);

        gpiv_scan_parameter (GPIV_IMGPROCPAR_KEY, PARFILE, image_proc_par, 
			     verbose);
        gpiv_scan_resourcefiles (GPIV_IMGPROCPAR_KEY, image_proc_par, verbose);
        gpiv_imgproc_print_parameters (fp_par_dat, image_proc_par);
	fclose (fp_par_dat);


    } else {
/*
 * use stdin, stdout
 */
        gpiv_scan_parameter (GPIV_GENPAR_KEY, PARFILE, gen_par, verbose);
        gpiv_scan_resourcefiles (GPIV_GENPAR_KEY, image_proc_par, verbose);

        gpiv_scan_parameter (GPIV_IMGPROCPAR_KEY, PARFILE, image_proc_par, 
			    verbose);
        gpiv_scan_resourcefiles (GPIV_IMGPROCPAR_KEY, image_proc_par, verbose);
    }


/*
 * Check parameters on correct values and adjust belonging  variables
 */



/*
 * Here the function calls of imgproc_mean and imgproc_subtract
 */
    if ((image_mean = imgproc_mean (gen_par, image_proc_par, fname_base)) 
        == NULL) {
        gpiv_error ("%s: failing img_mean", argv[0]);
    }

    if (image_proc_par->smooth_operator == GPIV_IMGOP_SUBTRACT)
        imgproc_subtract (gen_par, fname_base, image_mean);


/* 
 * And writing to output 
 */
    if (fname__set == TRUE) {
        if ((fp = fopen (fname_out, "wb")) == NULL) {
            return (1);
        }
        gpiv_write_png_image (fp, image_mean, TRUE);
        fclose (fp);
    } else {
        gpiv_write_png_image (stdout, image_mean, TRUE);
    }

    return 0;
}
