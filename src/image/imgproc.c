/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*---------------------------------------------------------------------------
   imgproc: processes image

   Copyright (C) 2003, 2004 Gerber van der Graaf

   imgproc is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR 'PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



   RCS version control
   $Log: imgproc.c,v $
   Revision 2.10  2008-09-25 13:08:34  gerber
   Changed RCID to argv[0] in (error) messages

   Revision 2.9  2008-04-28 12:09:30  gerber
   hdf-formatted files are now with .hdf extension (previously: .gpi)

   Revision 2.8  2007-11-23 16:18:55  gerber
   release 0.5.0: Kafka

   Revision 2.7  2007-03-06 11:36:45  gerber
   removed -c -r options: obsolete

   Revision 2.6  2007-01-29 14:26:54  gerber
   Added image formats png, gif, tif, pgm, bpm. Added gpiv_aint

   Revision 2.5  2006/01/31 14:18:03  gerber
   version 0.3.2

   Revision 2.4  2005/06/15 09:32:20  gerber
   check on memory leacks: change of internal structure; gpiv_alloc_pivdata and gpiv_free_pivdata in main optimizing by printing progress only if percentage has been changed

   Revision 2.3  2005/02/26 16:29:19  gerber
   parameter flags (parameter_logic) defined as gboolean, added scale_type 
   to struct __GpivPostPar for inverse scaling

   Revision 2.2  2004/10/15 10:11:41  gerber
   GPIV_ and Gpiv prefix to defines and structure names of libgpiv

   Revision 2.1  2004/06/14 14:39:26  gerber
   addied image filtering and processing program




------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <glib.h>
#include <gpiv.h>

#define PARFILE "gpivrc"	/* Parameter file name */

#define USAGE "\
Usage: imgproc | mktestimg | smooth | hilo | clip | fft | lowpass | highpass | getbit \n\
              [-b int] [-h | --help] [-o | --operation] [-p | --print] [-w int] \n\
              [-f | --filter] [-t | --threshold int] \n\
              [-v] [filename] < stdin > stdout \n\
\n\
keys: \n\
\n\
-b bit:           for getbit: bit number [0, .., 7] \n\
-f | --filter:    only for imgproc: image filter to be performed: \n\
                  make test image (0), smoothing (1), high-low filtering (2), \n\
                  clipping (3), fft (4), inverse fft (5), lowpass filter (8), \n\
                  highpass filter (9), getbit (10) \n\
-h | --help:      this on-line help \n\
-p | --print:     print parameters to stdout \n\
-o | --operation: for smoothing: set pixel equal to the mean of the \n\
                  window value (0), subtract mean from from pixel (1) \n\
                  add (2), multiply (3) or divide (4)\n\
-t | --threshold: only for clip: threshold value to be set to zero \n\
-w size:          window size (default: 15) \n\
-v | --version:   version number \n\
filename:         full image filename. Overrides stdin and stdout.  \n\
                  If stdin and stdout are used, input is expected to be .png \n\
"


#define HELP  "\
Image processing program for PIV images."

#define RCSID "$Id: imgproc.c,v 2.10 2008-09-25 13:08:34 gerber Exp $"

/*
 * Global variables
 */
gboolean use_stdin_stdout = FALSE;
/* gboolean print_par = FALSE; */
gboolean verbose = FALSE;



static void 
command_args (int argc, 
              char *argv[], 
              char fname[GPIV_MAX_CHARS],
              GpivImageProcPar *image_proc_par
              )
/* ----------------------------------------------------------------------------
 * Command line argument handling */
{
    char c;
    int argc_next;
    while (--argc > 0 && (*++argv)[0] == '-') {
	argc_next = 0;
/*
 * argc_next is set to 1 if the next cmd line argument has to be searched for; 
 * in case that the command line argument concerns more than one char or cmd 
 * line argument needs a parameter
 */
	while (argc_next == 0 && (c = *++argv[0])) {
	    switch (c) {
	    case 'v':
/*
 * Use Revision Control System (RCS) for version
 */
		printf("\n%s\n", RCSID);
		exit(0);
		break;
	    case 'h':
		printf("\n%s", argv[0]);
		printf("\n%s", HELP);
		printf("\n%s", USAGE);
		exit(0);
		break;
/*
 * bitnumber for getbit
 */
	    case 'b':
		 image_proc_par->bit = atoi(*++argv);
		 image_proc_par->bit__set = TRUE;
		 argc_next = 1;
		 --argc;
		break;
/*
 * file name
 */
	    case 'f':
                image_proc_par->filter = atoi(*++argv);
                image_proc_par->filter__set = TRUE;
                --argc;
		argc_next = 1;
		break;


/*
 * whar to do with the mean value of a window: operation
 */
	    case 'o':
		 image_proc_par->smooth_operator = atoi(*++argv);
		 image_proc_par->smooth_operator__set = TRUE;
		 --argc;
		 argc_next = 1;
		 break;

/*
 * print pararameters
 */
	    case 'p':
		    verbose = TRUE;
		break;
/*
 * threshold
 */
	    case 't':
                image_proc_par->threshold =  atoi(*++argv);
                image_proc_par->threshold__set = TRUE;
                --argc;
                argc_next = 1;
		break;

/*
 * window size
 */
	    case 'w':
                image_proc_par->window =  atoi(*++argv);
                image_proc_par->window__set = TRUE;
                --argc;
                argc_next = 1;
		break;

/*
 * long option keys
 */
	    case '-':
		if (strcmp("-help", *argv) == 0) {
                    printf("\n%s", argv[0]);
                    printf("\n%s", HELP);
                    printf("\n%s", USAGE);
                    exit(0);
                    
                } else if (strcmp("-print", *argv) == 0) {
		    verbose = TRUE;
                    
                } else if (strcmp("-filter", *argv) == 0) {
		    image_proc_par->filter = atoi(*++argv);
		    image_proc_par->filter__set = TRUE;
                    --argc;
                    
                } else if (strcmp("-version", *argv) == 0) {
                    printf("%s\n", RCSID);
                    exit(0);
                    
                } else if (strcmp("-operation", *argv) == 0) {
                    image_proc_par->smooth_operator = atoi(*++argv);
		 image_proc_par->smooth_operator__set = TRUE;
		 --argc;

                } else if (strcmp("-threshold", *argv) == 0) {
		    image_proc_par->threshold =  atoi(*++argv);
                    image_proc_par->threshold__set = TRUE;
                     --argc;
                    argc_next = 1;

                } else {
		    gpiv_error("%s: unknown option: %s", argv[0], *argv);
		}
		argc_next = 1;
		break;

	    default:
		gpiv_error("%s: unknown option: %s", argv[0], *argv);
		break;
	    }
	}
    }

    if (argc == 1) {
        strcpy(fname, argv[argc - 1]);
        use_stdin_stdout = FALSE;
    } else if (argc == 0) {
        use_stdin_stdout = TRUE;
        verbose = FALSE;
    } else {
	gpiv_error("%s: unknown argument: %s", argv[0], *argv);
    }

}



static gchar *
make_fname (char *fname, 
            char *fname_parameter, 
            char *fname_out
            )
/*-----------------------------------------------------------------------------
 * generates filenames for output
 */
{
    gchar *err_msg = NULL;
    gchar *fname_base = NULL;

    if (fname == NULL ) {
        err_msg = "make_fname: \"fname_in == NULL\"";
        return (err_msg);
    }

    fname_base = g_strdup(fname);
    strtok(fname_base, ".");
    gpiv_io_make_fname(fname_base, GPIV_EXT_PAR, fname_parameter);
    if (verbose) printf("# Parameter file: %s\n", fname_parameter);

    gpiv_io_make_fname(fname_base, GPIV_EXT_PNG_IMAGE_PROC, fname_out);
    if (verbose) printf("# Output image file: %s\n", fname_out);

    g_free (fname_base);
    return (err_msg);
}



/*
 * Image processing functions
 */

/* static void */
/* imgproc_invfft(GpivImagePar image_par, */
/*                GpivImageProcPar image_proc_par, */
/*                guint16 **img_in,  */
/*                guint16 **img_out); */




int 
main (int argc, 
      char *argv[]
      )
/* ----------------------------------------------------------------------------
 *  main routine to process images for PIV 
 */
{
    FILE *fp = NULL, *fp_par = NULL;
    gchar *err_msg = NULL;
    gchar fname_in[GPIV_MAX_CHARS],
        fname_out[GPIV_MAX_CHARS],
	fname_parameter[GPIV_MAX_CHARS];

    GpivImage *image = NULL;
    
    GpivImageProcPar *image_proc_par = g_new0 (GpivImageProcPar, 1);
    GpivImageProcPar *image_proc_par_default = g_new0 (GpivImageProcPar, 1);

    char *c = NULL;

/*
 * Image process parameter initialization
 */
    gpiv_imgproc_parameters_set (image_proc_par, FALSE);
    gpiv_imgproc_default_parameters (image_proc_par_default, TRUE);

/*
 * Image processing parameter initialization
 * Define operation type from program name, which is a symbolic link to imgproc
 */
    if ((c = strstr(argv[0], "imgproc")) != NULL) {
        image_proc_par->filter = 0;
        image_proc_par->filter__set = FALSE;
    } else if ((c = strstr(argv[0], "mktestimg")) != NULL) {
        image_proc_par->filter = GPIV_IMGFI_MKTESTIMG;
        image_proc_par->filter__set = TRUE;
    } else if ((c = strstr(argv[0], "smooth")) != NULL) {
        image_proc_par->filter = GPIV_IMGFI_SMOOTH;
        image_proc_par->filter__set = TRUE;
    } else if ((c = strstr(argv[0], "hilo")) != NULL) {
        image_proc_par->filter = GPIV_IMGFI_HILO;
        image_proc_par->filter__set = TRUE;
    } else if ((c = strstr(argv[0], "clip")) != NULL) {
        image_proc_par->filter = GPIV_IMGFI_CLIP;
        image_proc_par->filter__set = TRUE;
    } else if ((c = strstr(argv[0], "fft")) != NULL) {
        image_proc_par->filter = GPIV_IMGFI_FFT;
        image_proc_par->filter__set = TRUE;
/*     } else if ((c = strstr(argv[0], "invfft")) != NULL) { */
/*         image_proc_par->filter = GPIV_IMGFI_INVFFT; */
/*         image_proc_par->filter__set = TRUE; */
    } else if ((c = strstr(argv[0], "lowpass")) != NULL) {
        image_proc_par->filter = GPIV_IMGFI_LOWPASS;
        image_proc_par->filter__set = TRUE;
    } else if ((c = strstr(argv[0], "highpass")) != NULL) {
        image_proc_par->filter = GPIV_IMGFI_HIGHPASS;
        image_proc_par->filter__set = TRUE;
    } else if ((c = strstr(argv[0], "getbit")) != NULL) {
        image_proc_par->filter = GPIV_IMGFI_GETBIT;
        image_proc_par->filter__set = TRUE;
    } else {
        gpiv_error("imgproc: unvalid program name or symlink");
    }


/*
 * Reads command line arguments.
 * Reads image header and image processing parametes from resource files 
 * if not overridden by the commandline options
 */
    command_args (argc, argv, fname_in, image_proc_par);
    gpiv_scan_parameter (GPIV_IMGPROCPAR_KEY, PARFILE, image_proc_par, verbose);
    gpiv_scan_resourcefiles (GPIV_IMGPROCPAR_KEY, image_proc_par, verbose);
 
    if (use_stdin_stdout == FALSE) {

/* 
 * Generating filenames 
 */
	make_fname(fname_in, fname_parameter, fname_out);
	if (verbose) 
            printf("\n# Parameters written to: %s", fname_parameter);

    }


/*
 * Check if all image process parameters have been read
 */
    g_message ("main:: 0a");
    if ((err_msg = 
         gpiv_imgproc_check_parameters_read (image_proc_par, image_proc_par_default))
        != NULL) gpiv_warning ("%s: %s", argv[0], err_msg);

    g_message ("main:: 0b");
    if (use_stdin_stdout == FALSE) {
	if ((fp_par = fopen (fname_parameter, "a")) == NULL) {
	    gpiv_error ("%s: failure opening %s for input",
                       argv[0], fname_parameter);
	}
	gpiv_imgproc_print_parameters (fp_par, image_proc_par);
        fclose (fp_par);
    }

    if (verbose) gpiv_imgproc_print_parameters (stdout, image_proc_par);

/*
 * reads images
 */
    g_message ("main:: 2");
    if (image_proc_par->filter != GPIV_IMGFI_MKTESTIMG) {
        if (use_stdin_stdout) {
            if ((image = gpiv_read_image (stdin)) == NULL) {
                gpiv_error ("%s: gpiv_fread_image_ni", argv[0]);
            }
            
        } else {
            if ((image = gpiv_fread_image (fname_in)) == NULL) {
                gpiv_error ("%s: %s", argv[0], err_msg);
            }
        }
    }

    g_message ("main:: 3");
/* 
 * Here the function calls for the image processing
 */
    if (image_proc_par->filter == GPIV_IMGFI_MKTESTIMG) {
        GpivImagePar *image_par = g_new0 (GpivImagePar, 1);
        GpivImagePar *image_par_default = g_new0 (GpivImagePar, 1);

        gpiv_img_parameters_set (image_par, FALSE);
        gpiv_img_default_parameters (image_par_default, TRUE);
        gpiv_scan_parameter (GPIV_IMGPAR_KEY, PARFILE, image_par, verbose);
        gpiv_scan_resourcefiles (GPIV_IMGPAR_KEY, image_par, verbose);
        if (gpiv_img_check_header_required_read (image_par) != NULL) {
            gpiv_img_ovwrt_parameters (image_par_default, image_par);
        }
        if (verbose) gpiv_img_print_header (stdout, image_par);

        if ((image = gpiv_imgproc_mktestimg (image_par, image_proc_par)) == NULL) {
            gpiv_error ("%s: %s", argv[0], err_msg);
        }
        

    } else if (image_proc_par->filter == GPIV_IMGFI_HILO) {
        if ((err_msg = gpiv_imgproc_highlow (image, image_proc_par)) != NULL) {
            gpiv_error ("%s: %s", argv[0], err_msg);
        }
        

    } else if (image_proc_par->filter == GPIV_IMGFI_SMOOTH) {
        if ((err_msg = gpiv_imgproc_smooth (image, image_proc_par)) != NULL) {
            gpiv_error ("%s: %s", argv[0], err_msg);
        }
        

    } else if (image_proc_par->filter == GPIV_IMGFI_CLIP) {
        if ((err_msg = gpiv_imgproc_clip (image, image_proc_par)) != NULL) {
            gpiv_error ("%s: %s", argv[0], err_msg);
        }
        

    } else if (image_proc_par->filter == GPIV_IMGFI_FFT) {
        if ((err_msg = gpiv_imgproc_fft (image, image_proc_par)) != NULL) {
            gpiv_error ("%s: %s", argv[0], err_msg);
        }
        
        /*     } else if (image_proc_par->filter == GPIV_FI_INVFFT) { */
        /*         gpiv_imgproc_invfft (image_par, image_proc_par, img1_in, img1_out)) */
        /*         if (image_par->x_corr) { */
        /*             gpiv_imgproc_invfft(image_par, image_proc_par, img2_in, img2_out)) */
            

/* BUGFIX: Libgpiv TODO */
/* #ifndef USE_FFTW3 */
/*
    } else  if (image_proc_par->filter == GPIV_IMGFI_CORR) {
        if ((err_msg = 
             gpiv_imgproc_correlate (image_par, image_proc_par, img1_in, img2_in, 
                                    &img1_out))
            != NULL) gpiv_error ("%s: %s", argv[0], err_msg);
        

    } else if (image_proc_par->filter == GPIV_IMGFI_CONV) {
        if ((err_msg = 
             gpiv_imgproc_convolve (image_par, image_proc_par, img1_in, img2_in, 
                                   &img1_out))
            != NULL) gpiv_error ("%s: %s", argv[0], err_msg);
        

    } else if (image_proc_par->filter == GPIV_IMGFI_LOWPASS) {
        if ((err_msg = 
             gpiv_imgproc_lowpass (image_par, image_proc_par, img1_in))
            != NULL) gpiv_error ("%s: %s", argv[0], err_msg);
        img1_out = img1_in;
        if (image_par->x_corr) {
            if ((err_msg = 
                 gpiv_imgproc_lowpass(image_par, image_proc_par, img2_in))
                != NULL) gpiv_error ("%s: %s", argv[0], err_msg);
            img2_out = img2_in;
        }
        

*/
/* #endif USE_FFTW3      */   
    } else if (image_proc_par->filter == GPIV_IMGFI_HIGHPASS) {
        if ((err_msg = gpiv_imgproc_highpass (image, image_proc_par)) != NULL) {
            gpiv_error ("%s: %s", argv[0], err_msg);
        }


    } else if (image_proc_par->filter == GPIV_IMGFI_GETBIT) {
        if ((err_msg = gpiv_imgproc_getbit (image, image_proc_par)) != NULL) {
            gpiv_error ("%s: %s", argv[0], err_msg);
        }
        

    } else {
        gpiv_error("img_proc: non-existing filter %d", image_proc_par->filter);
    }


/*
 * And writing to output
 */
    if (use_stdin_stdout == TRUE) {
        fp = stdout;
        if ((err_msg = gpiv_write_png_image (fp, image, TRUE)) != NULL) {
            gpiv_error ("%s: %s\n", argv[0], err_msg);
        }


    } else {
        if ((fp = fopen (fname_out, "wb")) == NULL) {
            gpiv_error ("%s: unable to open %s", argv[0], fname_out);
        }

        if ((err_msg = gpiv_write_png_image (fp, image, TRUE)) != NULL) {
            gpiv_error ("%s: %s\n", argv[0], err_msg);
        }

        fclose(fp);
    }


    return 0;
}
