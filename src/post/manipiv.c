/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*---------------------------------------------------------------------------

   manipiv - manipulates (flipping / rotating, etc) PIV data

   Copyright (C) 2002, 2003, 2004 Gerber van der Graaf

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

-----------------------------------------------------------------------

  Flipx; flips data left - right
  Flipy; flips data up - down


------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <glib.h>
#include <gpiv.h>

/* #define PARFILE "manipiv.par" */	/* Parameter file name */
#define PARFILE "gpivrc"	/* Parameter file name */
#define USAGE "\
Usage: manipiv | fasty | flipx | flipy | revert | rot90 | rot180 \n\
               [-fi x0 y0 x1 y1] [-no_fi | --pass x0 y0 x1 y1] \n\
               [-fy] [-h | --help] [-i] [-p | --print] [-r] [--rev] \n\
               [-v | --version] [-x] [-y] [filename] < stdin > stdout \n\
\n\
keys: \n\
-fi x0 y0 x1 y1:       filters out all data from (x0,y0) to (x1,y1) \n\
-no_fi | --pass x0 y0 x1 y1: passes through the data from (x0,y0) to (x1,y1) \n\
-fy:                   fast y; returns fast running y-positions of data with \n\
                       fast running x\n\
-h | --help:           this on-line help \n\
-i:                    interchanges indexes of output data for increasing \n\
                       order \n\
-p | --print:          prints parameters to stdout \n\
-r:                    rotates over 90 degrees \n\
--rev:                 reverts array indexes of output data for getting \n\
                       reversed order \n\
-v | --version:        version number \n\
-x:                    flips data in x-direction (vertical axis) \n\
-y:                    flips data in y-direction (horizontal axis) \n\
filename:              Input PIV data file. Substitutes stdin and stdout\n\
"

#ifdef DEBUG
#define USAGE_DEBUG "\
Developers version also contains:  \n\
               [-p_main][-p_median_residu][-p_subst_residu] \n\
               [-p_check_residu][-p_residu_statistic] \n\
keys: \n\
-p_'function' N: prints data to be generated in the function; the \n\
                 higher N, the more detailed the output. \n\
                 For N = 10, err_vec will exit at the end of the function"
#endif


#define HELP  "\
manipulates order and positions of PIV data, flips in horizontal \n\
or vertical direction and rotates over 90 or 180 degrees"

#define RCSID "$Id: manipiv.c,v 2.13 2008-09-25 13:08:34 gerber Exp $"

/*
 * Global variables 
*/
gboolean use_stdin_stdout = FALSE;
gboolean verbose = FALSE;

#ifdef DEBUG
/*
 * Variables for development version 
 */
int print_main = 0, print_flip = 0, print_fasty = 0, print_fread_pivdata =
    0;
#endif



void 
command_args(int argc, 
             char *argv[], 
             char fname[GPIV_MAX_CHARS],
             GpivPostPar *piv_post_par
             )
/* ----------------------------------------------------------------------------
 * Command line argument handling
 */
{
    char c;
    int argc_next;
    while (--argc > 0 && (*++argv)[0] == '-') {
	argc_next = 0;

/*
 * argc_next is set to 1 if the next cmd line argument has to be searched for; 
 * in case that the command line argument concerns more than one char or cmd
 * line argument needs a parameter
 */
	while (argc_next == 0 && (c = *++argv[0])) {
	    switch (c) {
	    case 'v':
/*
 * Use Revision Control System (RCS) for version
 */
		printf("\n%s\n", RCSID);
		exit(0);
		break;
	    case 'h':
		printf("\n%s", argv[0]);
		printf("\n%s", HELP);
		printf("\n%s", USAGE);
#ifdef DEBUG
		printf("\n%s\n", USAGE_DEBUG);
#endif
		exit(0);
		break;
	    case 'r':
		    piv_post_par->operator_manipiv = GPIV_ROT90;
		    piv_post_par->operator_manipiv__set = 1;
		break;
	    case 'x':
		piv_post_par->operator_manipiv = GPIV_FLIP_X;
		piv_post_par->operator_manipiv__set = 1;
		break;
	    case 'y':
		piv_post_par->operator_manipiv = GPIV_FLIP_Y;
		piv_post_par->operator_manipiv__set = 1;
		break;
	    case 'f':
		if (strcmp("fy", *argv) != 0 && strcmp("fi", *argv) != 0) {
		} else if (strcmp("fi", *argv) == 0) {
/* 
 * filters block of data from data stream
 */
		    piv_post_par->operator_manipiv = GPIV_FILTER_BLOCK;
		    piv_post_par->operator_manipiv__set = 1;
		    piv_post_par->block->x_1 = atoi(*++argv);
		    --argc;
		    piv_post_par->block->y_1 = atoi(*++argv);
		    --argc;
		    piv_post_par->block->x_2 = atoi(*++argv);
		    --argc;
		    piv_post_par->block->y_2 = atoi(*++argv);
                    piv_post_par->block__set = TRUE;
		    --argc;
		    argc_next = 1;
		} else if (strcmp("fy", *argv) == 0) {
		    piv_post_par->operator_manipiv = GPIV_FAST_Y;
		    piv_post_par->operator_manipiv__set = 1;
		}
		break;
	    case 'p':
#ifdef DEBUG
	      if (
		       (strcmp(*argv, "p_main") != 0) &&
		       (strcmp(*argv, "p_flip") != 0) &&
		       (strcmp(*argv, "p_fasty") != 0)
		    ) {
#endif /* DEBUG */
		    verbose = TRUE;
#ifdef DEBUG
		} else if (strcmp("p_main", *argv) == 0) {
		    print_main = atoi(*++argv);
		    --argc;
		    argc_next = 1;
		} else if (strcmp("p_flip", *argv) == 0) {
		    print_flip = atoi(*++argv);
		    --argc;
		    argc_next = 1;
		} else if (strcmp("p_fasty", *argv) == 0) {
		    print_fasty = atoi(*++argv);
		    --argc;
		    argc_next = 1;
		} else if (strcmp("p_fread_pivdata", *argv) == 0) {
		    print_fread_pivdata = atoi(*++argv);
		    --argc;
		    argc_next = 1;
		}
#endif /* DEBUG */
		break;
/*
 * negotion of settings
 */
	    case 'n':
                if (strcmp(*argv, "no_fi") == 0) {
                    piv_post_par->operator_manipiv = GPIV_PASS_BLOCK;
                    piv_post_par->operator_manipiv__set = 1;
                    piv_post_par->block->x_1 = atoi(*++argv);
                    --argc;
                    piv_post_par->block->y_1 = atoi(*++argv);
                    --argc;
                    piv_post_par->block->x_2 = atoi(*++argv);
                    --argc;
                    piv_post_par->block->y_2 = atoi(*++argv);
                    piv_post_par->block__set = TRUE;
                    --argc;
                    argc_next = 1;
                } else {
		    gpiv_error("%s: unknown option: %s", argv[0], *argv);
		}
		argc_next = 1;
                break;

/*
 * long option keys
 */
	    case '-':
		if (strcmp("-help", *argv) == 0) {
                    printf("\n%s", argv[0]);
                    printf("\n%s", HELP);
                    printf("\n%s", USAGE);
                    exit(0);
                } else if (strcmp("-print", *argv) == 0) {
		    verbose = TRUE;
		} else if (strcmp("-rev", *argv) == 0) {
/*
 * reverts indexes of output data for increasing order
 */
		    piv_post_par->operator_manipiv = GPIV_REVERT;
		    piv_post_par->operator_manipiv__set = 1;
                } else if (strcmp("-version", *argv) == 0) {
                    printf("%s\n", RCSID);
                    exit(0);
                } else if (strcmp("-pass", *argv) == 0) {
                    piv_post_par->operator_manipiv = GPIV_PASS_BLOCK;
                    piv_post_par->operator_manipiv__set = 1;
                    piv_post_par->block->x_1 = atoi(*++argv);
                    --argc;
                    piv_post_par->block->y_1 = atoi(*++argv);
                    --argc;
                    piv_post_par->block->x_2 = atoi(*++argv);
                    --argc;
                    piv_post_par->block->y_2 = atoi(*++argv);
                    piv_post_par->block__set = TRUE;
                    --argc;
                    argc_next = 1;
                } else {
		    gpiv_error("%s: unknown option: %s", argv[0], *argv);
		}
		argc_next = 1;
		break;

	    default:
		gpiv_error("%s: unknown option: %s", argv[0], *argv);
		break;
	    }
	}
    }

    /*
     * Check if filename or stdin / stdout is used
     */
    if (argc == 1) {
        use_stdin_stdout = FALSE;
        strcpy(fname, argv[argc - 1]);
    } else if (argc == 0) {
        use_stdin_stdout = TRUE;
        verbose = FALSE;
    } else {
	gpiv_error("%s: unknown argument: %s", argv[0], *argv);
    }


}



static gchar *
make_fname(char *fname_in, 
           char *fname_parameter,
           char *fname_out
           )
/*-----------------------------------------------------------------------------
 * generate filenames
 */
{
    gchar *err_msg = NULL;
    gchar *fname_base = NULL;

    if (fname_in == NULL ) {
        err_msg = "make_fname: \"fname_in == NULL\"";
        return (err_msg);
    }
 
/*
 * Stripping filename
 */
    fname_base = g_strdup(fname_in);
    strtok(fname_base, ".");

/*
 * filenames for output
 */
    gpiv_io_make_fname(fname_base, GPIV_EXT_PAR, fname_parameter);
    if (verbose) printf("# Data parameter file: %s\n", fname_parameter);

    gpiv_io_make_fname(fname_base, GPIV_EXT_MANI, fname_out);
    if (verbose) printf("# Output file: %s\n", fname_out);

    return (err_msg);
}



int 
main(int argc, 
     char *argv[]
     )
/*-----------------------------------------------------------------------------
*/
{
    gchar *err_msg = NULL, *c = NULL;
    FILE *fp, *fp_par_dat;
    gchar fname_out[GPIV_MAX_CHARS],
	fname_parameter[GPIV_MAX_CHARS], fname_in_piv[GPIV_MAX_CHARS];

    GpivPostPar *piv_post_par = g_new (GpivPostPar, 1);
    GpivRoi *block = g_new (GpivRoi, 1);
    GpivPivData *piv_data = NULL;


    piv_post_par->block = block;
    gpiv_post_parameters_set (piv_post_par, FALSE);

/*
 * Define GpivOperation type from program name, which is a symbolic link to 
 * gpiv_manipiv
 */
    if ((c = strstr (argv[0], "manipiv")) != NULL) {
	piv_post_par->operator_manipiv = GPIV_FLIP_X;
	piv_post_par->operator_manipiv__set = FALSE;
    } else if ((c = strstr (argv[0], "flipx")) != NULL) {
	piv_post_par->operator_manipiv = GPIV_FLIP_X;
	piv_post_par->operator_manipiv__set = TRUE;
    } else if ((c = strstr (argv[0], "flipy")) != NULL) {
	piv_post_par->operator_manipiv = GPIV_FLIP_Y;
	piv_post_par->operator_manipiv__set = TRUE;
    } else if ((c = strstr (argv[0], "rot180")) != NULL) {
	piv_post_par->operator_manipiv = GPIV_ROT180;
	piv_post_par->operator_manipiv__set = TRUE;
    } else if ((c = strstr (argv[0], "rot90")) != NULL) {
	piv_post_par->operator_manipiv = GPIV_ROT90;
	piv_post_par->operator_manipiv__set = TRUE;
    } else if ((c = strstr (argv[0], "revert")) != NULL) {
	piv_post_par->operator_manipiv = GPIV_REVERT;
	piv_post_par->operator_manipiv__set = TRUE;
    } else if ((c = strstr (argv[0], "fasty")) != NULL) {
	piv_post_par->operator_manipiv = GPIV_FAST_Y;
	piv_post_par->operator_manipiv__set = TRUE;
    } else {
        gpiv_error("manipiv: unvalid program name or symlink");
    }

    command_args(argc, argv, fname_in_piv, piv_post_par);
    if (verbose) {
      printf("# %s\n# Command line options:\n", RCSID);
      gpiv_post_print_parameters (NULL, piv_post_par);
    }



    if (use_stdin_stdout == FALSE) {
	if ((err_msg = 
             make_fname (fname_in_piv, fname_parameter, fname_out))
            != NULL) 
            gpiv_error("%s: Failure calling make_fname",
		    argv[0]);

/*
 * Prints command line parameters to par-file
 */
	if ((fp_par_dat = fopen(fname_parameter, "a")) == NULL) {
	    gpiv_error("%s: failure opening %s for input",
		    argv[0], fname_parameter);
	}
	fprintf(fp_par_dat, "\n\n# %s\n# Command line options:\n", argv[0]);
	gpiv_post_print_parameters (fp_par_dat, piv_post_par);


/*
 * Reading parametes from PARFILE (and writing to data par-file)
 */
        gpiv_scan_parameter (GPIV_POSTPAR_KEY, PARFILE, piv_post_par, verbose);
        if ((err_msg =
             gpiv_scan_resourcefiles (GPIV_POSTPAR_KEY, piv_post_par, verbose))
            != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);
        gpiv_post_print_parameters (fp_par_dat, piv_post_par);
	fclose(fp_par_dat);


    } else {
        gpiv_scan_parameter (GPIV_POSTPAR_KEY, PARFILE, piv_post_par, verbose);
        if ((err_msg =
             gpiv_scan_resourcefiles (GPIV_POSTPAR_KEY, piv_post_par, verbose))
            != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);
    }

    gpiv_post_check_parameters_read (piv_post_par, NULL);


/*
 * Reading PIV data set.
 * Input PIV data might be in reverse order
 */
    if (use_stdin_stdout == TRUE) {
        fp = stdin;
    } else {
        if ((fp = fopen (fname_in_piv, "r")) == NULL) { 
            gpiv_error ("%s: Failure opening %s for input", 
                        argv[0], fname_in_piv); 
        }
    }

    if (piv_post_par->operator_manipiv == GPIV_FAST_Y) {
        if ((piv_data = gpiv_read_pivdata_fastx (fp)) == NULL) {
            err_msg = "GPIV_POST_MANIPIV: Failure calling read_pivdata_fast_x";
            gpiv_error ("%s: %s", argv[0], err_msg);
        }
        
    } else {
        if ((piv_data = gpiv_read_pivdata (fp)) == NULL) {
            err_msg = "GPIV_POST_MANIPIV: Failure calling gpiv_read_pivdata";
            gpiv_error ("%s: %s", argv[0], err_msg);
        }
        
    }
    if (use_stdin_stdout == FALSE) fclose (fp);

/*
 * Here the library function call of the post-processing.
 */
    if ((err_msg = gpiv_post_manipiv (piv_data, piv_post_par)) != NULL) {
        gpiv_error ("%s: ", argv[0], err_msg);
    }

/*
 * Adding comment to the data
 * And writing to output
 */
    g_free (piv_data->comment);
    piv_data->comment = g_strdup_printf ("# Software: %s\n", RCSID);
    piv_data->comment = gpiv_add_datetime_to_comment (piv_data->comment);

    if (use_stdin_stdout == TRUE) {
        fp = stdout;
    } else {
        if ((fp = fopen (fname_out, "w")) == NULL) { 
            gpiv_error ("%s: Failure opening %s for output", 
                        argv[0], fname_out); 
        }
    }

    if ((err_msg = gpiv_write_pivdata (fp, piv_data, TRUE)) != NULL) {
        fclose (fp);
        gpiv_error ("%s: %s", argv[0], err_msg);
    }
    if (use_stdin_stdout == FALSE) fclose (fp);


/*
 * Freeing allocated memory of matrices, if needed
 */
    if (verbose == TRUE)
	printf("\n");

    exit (0);
}







