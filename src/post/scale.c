/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*---------------------------------------------------------------------------

   scale - applies spatial and time scale to PIV data

   Copyright (C) 2002, 2003, 2004 Gerber van der Graaf

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

------------------------------------------------------------------------*/

#include <stdio.h> 
#include <stdlib.h> 
#include <glib.h>
#include <gpiv.h>

/* #define PARFILE "scale.par"    */  /* Parameter file name */
#define PARFILE "gpivrc"     /* Parameter file name */
#define USAGE "\
Usage: scale [ -h | --help] [-p | --print] [-s M | -S M] \n\
             [-t dt | -T dt] [-v | --version] [-z px0 py0] [filename] \n\
             < stdin > stdout \n\
\n\
keys: \n\
-h | --help:           this on-line help \n\
-p | --print:          print parameters to stdout \n\
-s M:                  spatial scaling with magnifation factor M [mm/px]  \n\
-S M:                  inverse spatial scaling \n\
-t dt:                 time scaling with dt time between subsequent \n\
                       recordings [s] \n\
-T dt:                 inverse time scaling \n\
-v | --version:        version number \n\
-z px0 py0:            zero offset of image position \n\
filename:              input PIV file. Substitutes stdin and stdout \n\
"

#ifdef DEBUG
#define USAGE_DEBUG "\
Developers version also contains:  \n\
               [-p_main] \n\
keys: \n\
-p_'function' N; prints data to be generated in the function; the \n\
                   higher N, the more detailed the output. \n\
                   For N = 10, err_vec will exit at the end of the function"
#endif


#define HELP  "\
scale applies spatial and time scaling to PIV data"

#define RCSID "$Id: scale.c,v 2.14 2008-09-25 13:08:35 gerber Exp $"

/*
 * Global variables
 */
gboolean use_stdin_stdout = FALSE;
gboolean verbose = FALSE;

#ifdef DEBUG
/*
 * Variables for development version 
 */
int print_main=0;

#endif 


static void 
command_args(int argc, 
             char *argv[], 
             char fname[GPIV_MAX_CHARS],
             GpivImagePar *image_par,
             GpivPostPar *post_par
             )
/* ----------------------------------------------------------------------------
 * Command line argument handling 
 */
{
     char c;
     int argc_next;


     while (--argc > 0 && (*++argv)[0] == '-') {
	  argc_next=0;
/*
 * argc_next is set to 1 if the next cmd line argument has to be searched for; 
 * in case that the command line argument concerns more than one char or cmd 
 * line argument needs a parameter
 */
	  while (argc_next == 0 && (c = *++argv[0])) {
	       switch (c) {

	       case 'v':
/*
 * Use Revision Control System (RCS) for version
 */
		    printf("%s\n", RCSID); 
		    exit(0);
		    break;

	       case 'h':
		    printf("%s\n", argv[0]); 
		    printf("%s\n",HELP);
		    printf("%s\n",USAGE);
#ifdef DEBUG
		    printf ("\n%s\n",USAGE_DEBUG);
#endif
		    exit(0);
		    break;

/*
 * Spatial scaling
 */
	       case 's':
		    image_par->s_scale = atof(*++argv);
                    image_par->s_scale__set = TRUE;
                    post_par->scale_type = GPIV_SCALE;
		    argc_next = 1;	
		    --argc;
		    break;

/*
 * Inverse spatial scaling
 */
	       case 'S':                 
		    image_par->s_scale = atof(*++argv);
                    image_par->s_scale__set = TRUE;
                    post_par->scale_type = GPIV_SCALE_INV;
		    argc_next = 1;	
		    --argc;
		    break;

/*
 * Time scaling
 */
	       case 't':                 
		    image_par->t_scale = atof(*++argv);
		    image_par->t_scale__set = TRUE;
                    post_par->scale_type = GPIV_SCALE;
		    argc_next = 1;	
		    --argc;
		    break;

/*
 * Inverse time scaling
 */
	       case 'T':                 
		    image_par->t_scale = atof(*++argv);
		    image_par->t_scale__set = TRUE;
                    post_par->scale_type = GPIV_SCALE_INV;
		    argc_next = 1;	
		    --argc;
		    break;

/*
 * Zero-offset x0 y0
 */
	       case 'z':                 
			 image_par->z_off_x = atof(*++argv);
			 image_par->z_off_y = atof(*++argv);
			 image_par->z_off_x__set = TRUE;
			 image_par->z_off_y__set = TRUE;
			 argc_next = 1;	
			 --argc;
			 --argc;
		    break;

	       case 'p':
#ifdef DEBUG
		    if ((strcmp(*argv,"p_main" ) !=0))
		    {
#endif
			 verbose = TRUE;
#ifdef DEBUG
		    } else if (strcmp("p_main",*argv) == 0) {  
			 print_main = atoi(*++argv);
			 --argc; 
			 argc_next = 1;
		    }
#endif /* DEBUG */
		    break;
/*
 * long option keys
 */
	    case '-':
		if (strcmp("-help", *argv) == 0) {
                    printf("\n%s", argv[0]);
                    printf("\n%s", HELP);
                    printf("\n%s", USAGE);
                    exit(0);
                } else if (strcmp("-print", *argv) == 0) {
		    verbose = TRUE;
                } else if (strcmp("-version", *argv) == 0) {
                    printf("%s\n", RCSID);
                    exit(0);
                } else {
		    gpiv_error("%s: unknown option: %s", argv[0], *argv);
		}
		argc_next = 1;
		break;

	       default:
		    fprintf (stderr,USAGE);
#ifdef DEBUG
		    printf ("\n%s",USAGE_DEBUG);
#endif
		    exit(1);
		    break;
	       }
	  }
     }

    /*
     * Check if filename or stdin /stdout is used
     */
    if (argc == 1) {
        use_stdin_stdout = FALSE;
        strcpy (fname, argv[argc - 1]);
    } else if (argc == 0) {
        use_stdin_stdout = TRUE;
        verbose = FALSE;
    } else {
#ifdef DEBUG
	  printf ("\n%s", USAGE_DEBUG);
#endif
	  gpiv_error("%s: %s", argv[0], USAGE);
     }


}



static gchar *
make_fname(char *fname_in, 
           char *fname_header, 
           char *fname_parameter, 
           char *fname_out
           )
/*  ---------------------------------------------------------------------------
 * function to generate filenames
 */
{
    gchar *err_msg = NULL;
    gchar *fname_base = NULL;

    if (fname_in == NULL ) {
        err_msg = "make_fname: \"fname_in == NULL\"";
        return (err_msg);
    }
 
    /*
     * Stripping filename
     */
    fname_base = g_strdup(fname_in);
    strtok(fname_base, ".");

    /*
     * filenames for output PIV data
     */
    gpiv_io_make_fname (fname_base, GPIV_EXT_HEADER, fname_header);
    if (verbose) printf ("# Image header file: %s\n", fname_header);
     
    gpiv_io_make_fname (fname_base, GPIV_EXT_PAR, fname_parameter);
    if (verbose) printf ("# Parameter file: %s\n", fname_parameter);
     
    gpiv_io_make_fname (fname_base, GPIV_EXT_SC, fname_out);
    if (verbose) printf ("# Output file: %s\n", fname_out);
     
    g_free (fname_base);
    return (err_msg);
}



int
main (int argc, 
      char *argv[]
      )
/* ----------------------------------------------------------------------------
 *  Start of the main program
 */
{
    char * err_msg = NULL;
    FILE  *fp_par_dat, *fp;
    char fname_in[GPIV_MAX_CHARS], fname_out[GPIV_MAX_CHARS], 
        fname_header[GPIV_MAX_CHARS], fname_parameter[GPIV_MAX_CHARS];

    gboolean var_scale = 0;

    GpivPivData *piv_data = NULL;
    GpivImagePar *image_par = g_new (GpivImagePar, 1);
    GpivPostPar *post_par = g_new (GpivPostPar, 1);
    

    gpiv_img_parameters_set(image_par, FALSE);
    gpiv_post_parameters_set(post_par, FALSE);
     
    command_args(argc, argv, fname_in, image_par, post_par);
    if (verbose) {
        printf("# %s\n# Command line options:\n", RCSID);
        gpiv_img_print_parameters (NULL, image_par);
        gpiv_post_print_parameters (NULL, post_par);
    }

    gpiv_scan_parameter(GPIV_POSTPAR_KEY, PARFILE, post_par, verbose);
    if ((err_msg =
         gpiv_scan_resourcefiles(GPIV_POSTPAR_KEY, post_par, verbose))
        != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);
    
    if (use_stdin_stdout == FALSE) {
/*
 * Generating proper filenames
 */
        if ((err_msg = 
             make_fname (fname_in, fname_header, fname_parameter, fname_out)) 
            != NULL) {
            gpiv_error("%s: Failure calling make_fname", argv[0]);
        } 

/*
 * Prints command line parameters to par-file 
 */
        if ((fp_par_dat = fopen (fname_parameter, "a")) == NULL) {
            gpiv_error("\n%s: failure opening %s for input",
                       argv[0], fname_parameter);
        }
        fprintf(fp_par_dat, "\n\n# %s\n# Command line options:\n", argv[0]); 
        gpiv_img_print_parameters(fp_par_dat, image_par);

/*
 * Reading parametes from image header and PARFILE (and writing to data 
 * par-file)
 */
        gpiv_scan_parameter ("", fname_header, image_par, verbose);
        gpiv_scan_parameter (GPIV_IMGPAR_KEY, PARFILE, image_par, 
                            verbose);
        if ((err_msg =
             gpiv_scan_resourcefiles(GPIV_IMGPAR_KEY, image_par, 
                                     verbose))
            != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);
        gpiv_img_print_parameters (fp_par_dat, image_par);
        gpiv_post_print_parameters (fp_par_dat, post_par);

	fclose(fp_par_dat);


    } else {
        gpiv_scan_parameter (GPIV_IMGPAR_KEY, PARFILE, image_par, 
                            verbose);
        if ((err_msg =
             gpiv_scan_resourcefiles (GPIV_IMGPAR_KEY, image_par, 
                                     verbose))
            != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);
    }


/*
 * Reading input data
 */
    if (use_stdin_stdout == TRUE) {
        fp = stdin;
    } else {
        if ((fp = fopen (fname_in, "rb")) == NULL) {
            gpiv_error ("%s: failure opening %s for reading", 
                        argv[0], fname_in);
        }
    }

    if ((piv_data = gpiv_read_pivdata (fp)) == NULL) {
        gpiv_error ("%s: %s", argv[0], err_msg);
    }
    if (use_stdin_stdout == FALSE) fclose (fp);


/*
 * Here the function calls of the post-processing; scaling of PIV data
 */
    if (verbose == TRUE) printf("\n");

    if (post_par->scale_type == GPIV_SCALE) {
        if ((err_msg = gpiv_post_scale (piv_data, image_par)) != NULL) {
            gpiv_error("%s: Failure calling gpiv_post_scale", argv[0]);
        }
        var_scale = TRUE;
    } else if (post_par->scale_type == GPIV_SCALE_INV) {
        if ((err_msg = gpiv_post_inverse_scale (piv_data, image_par)) != NULL) {
            gpiv_error("%s: Failure calling gpiv_post_inverse_scale", argv[0]);
        }
        var_scale = TRUE;
    } else {
        gpiv_error("scale: used unexisting image_par.s_scale__set");
    }

/*
 * Adding comment to the data
 * And writing data to output
 */
    g_free (piv_data->comment);
    piv_data->comment = g_strdup_printf ("# Software: %s\n", RCSID);
    piv_data->comment = gpiv_add_datetime_to_comment (piv_data->comment);

    if (use_stdin_stdout == TRUE) {
        fp = stdout;
    } else {
        if ((fp = fopen (fname_out, "wb")) == NULL) { 
            gpiv_error ("%s: Failure opening %s for output", 
                        argv[0], fname_out); 
        }
    }

    if ((err_msg = gpiv_write_pivdata (fp, piv_data, TRUE)) != NULL) {
        gpiv_error ("%s: %s", argv[0], err_msg);
    }
    if (use_stdin_stdout == FALSE) fclose (fp);



    if (verbose == TRUE) printf("\n");
    exit (0);
}





   




