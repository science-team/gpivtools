/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*---------------------------------------------------------------------------

  vorstra - calculates differential quantities (like vorticity and
  strain) from PIV data


   Copyright (C) 2002, 2003, 2004 Gerber van der Graaf

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------------

  Some formulea:
     vorty_z = dV/dx - dU/dy 
     sh_strain = dU/dy + dV/dx 
      n_strain = dU/dx + dV/dy 


-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>
#include <gpiv.h>

/* #define PARFILE "vorstra.par" */	/* Parameter file name */
#define PARFILE "gpivrc"	/* Parameter file name */

#define GNUPLOT_DISPLAY_COLOR "DarkBlue"
#define GNUPLOT_DISPLAY_SIZE 250

#define USAGE "\
Usage: vorstra | vorty | nstrain | sstrain \n\
               [-d int] [-g] [--no_g] [-h | --help] [-o] [-s] [-n] \n\
               [-p | --print] [-v | --version] [filename] < stdin > stdout \n\
\n\
keys: \n\
-d N:                  differential type; central (0), least squares \n\
                       (1), richardson (2), circulation method (3)  \n\
-g:                    graphical visualization with gnuplot (needs -f) \n\
--no_g:                suppresses graphical visualization \n\
-h | --help:           this on-line help \n\
-n:                    normal strain analyses \n\
-o:                    vorticity analyses \n\
-p | --print:          print parameters to stdout  \n\
-s:                    shear strain analyses \n\
-v | --version;        version number\n\
filename:              input PIV data file,. Substitutes stdin and stdout \n\
"

#ifdef DEBUG
#define USAGE_DEBUG "\
Developers version also contains: \n\
               [-p_main][-p_differential] \n\
\n\
keys: \n\
-p_'function' N: prints data to be generated in the function; the \n\
                 higher N, the more detailed the output. \n\
                 For N = 10, err_vec will exit at the end of the function"
#endif


#define HELP  "\
vorstra calculates the differential quantities vorticity, shear strain and \n\
normal strain from PIV data."

#define RCSID "$Id: vorstra.c,v 2.16 2008-10-09 14:45:01 gerber Exp $"


gboolean use_stdin_stdout = FALSE;
gboolean verbose = FALSE;

int gnuplot = 0;
gboolean gnuplot__set = FALSE;

#ifdef DEBUG
/*
 * Parameters for development version
 */
int print_main = 0, print_differential = 0;
#endif



static void 
command_args(int argc, 
             char *argv[], 
             char fname[GPIV_MAX_CHARS],
             GpivPostPar * piv_post_par
             )
/* ----------------------------------------------------------------------------
 * Command line argument handling
 */
{
    char c;
    int argc_next;


    while (--argc > 0 && (*++argv)[0] == '-') {
	argc_next = 0;

/*
 * argc_next is set to 1 if the next cmd line argument has to be
 * searched for; in case that the command line argument concerns more
 * than one char or cmd line argument needs a parameter
 */
	while (argc_next == 0 && (c = *++argv[0])) {
	    switch (c) {
/*
 * Use Revision Control System (RCS) for version
 */
	    case 'v':
		printf("\n%s\n", RCSID);
		exit(0);

/*
 * differential operator type
 */
	    case 'd':
		piv_post_par->diff_type__set = TRUE;
		piv_post_par->diff_type = atoi(*++argv);
		argc_next = 1;
		--argc;
		break;

/*
 * graphic output with gnuplot
 */
	    case 'g':
	      gnuplot = 1;
	      gnuplot__set = TRUE;
	      break;
	    case 'h':
		printf("\n%s", argv[0]);
		printf("\n%s", HELP);
		printf("\n%s\n", USAGE);
		exit(0);

/*
 * normal strain analysis
 */
	    case 'n':
                piv_post_par->operator_vorstra__set = TRUE;
                piv_post_par->operator_vorstra = GPIV_N_STRAIN;
		break;

/*
 * vorticity analysis
 */
	    case 'o':
		piv_post_par->operator_vorstra__set = TRUE;
		piv_post_par->operator_vorstra = GPIV_VORTICITY;
		break;
	    case 'p':
#ifdef DEBUG
		if (strcmp(*argv, "p_main") != 0) {
#endif
		    verbose = TRUE;
#ifdef DEBUG
		} else if (strcmp("p_main", *argv) == 0) {
		    print_main = atoi(*++argv);
		    --argc;
		    argc_next = 1;
		}
#endif
		break;
/*
 * shear strain analysis
 */
	    case 's':
		piv_post_par->operator_vorstra__set = TRUE;
		piv_post_par->operator_vorstra = GPIV_S_STRAIN;
		break;

/*
 * long option keys
 */
	    case '-':
		if (strcmp("-help", *argv) == 0) {
                    printf("\n%s", argv[0]);
                    printf("\n%s", HELP);
                    printf("\n%s", USAGE);
                    exit(0);
                } else if (strcmp("-print", *argv) == 0) {
		    verbose = TRUE;
                } else if (strcmp("-version", *argv) == 0) {
                    printf("%s\n", RCSID);
                    exit(0);

/*
 * do not plot with gnuplot
 */
		} else if (strcmp("-no_g", *argv) == 0) {
                    gnuplot = 0;
                    gnuplot__set = TRUE;
                    argc_next = 1;

                } else {
		    gpiv_error("%s: unknown option: %s", argv[0], *argv);
		}
		argc_next = 1;
		break;

	    default:
		gpiv_error("\n%s error : unknown option: %s\n",
			argv[0], *argv);
		break;
	    }
	}
    }

    /*
     * Check if filename or stdin /stdout is used
     */
    if (argc == 1) {
        use_stdin_stdout = FALSE;
        strcpy(fname, argv[argc - 1]);
    } else if (argc == 0) {
        use_stdin_stdout = TRUE;
        verbose = FALSE;
    } else {
	gpiv_error("\n%s error: unknown argument: %s\n", argv[0], *argv);
    }

}



static gchar *
make_fname(char *fname_in, 
           char *fname_parameter, 
           char *fname_out, 
           GpivPostPar *piv_post_par
           )
/* ----------------------------------------------------------------------------
 * generate filenames
 */
{
    gchar *err_msg = NULL;
    gchar *fname_base = NULL;

    if (fname_in == NULL ) {
        err_msg = "make_fname: \"fname_in == NULL\"";
        return (err_msg);
    }

/*
 * Stripping filename
 */
    fname_base = g_strdup(fname_in);
    strtok(fname_base, ".");

/*
 * filenames for output
 */
    gpiv_io_make_fname(fname_base, GPIV_EXT_PAR, fname_parameter);
    if (verbose)
	printf("# Parameter file: %s\n", fname_parameter);

    if (piv_post_par->operator_vorstra == GPIV_VORTICITY) {
        gpiv_io_make_fname (fname_base, GPIV_EXT_VOR, fname_out);
    } else if (piv_post_par->operator_vorstra == GPIV_S_STRAIN) {
        gpiv_io_make_fname (fname_base, GPIV_EXT_SSTR, fname_out);
    } else if (piv_post_par->operator_vorstra == GPIV_N_STRAIN) {
        gpiv_io_make_fname (fname_base, GPIV_EXT_NSTR, fname_out);
    } else {
        gpiv_error("%s: non valid operation", RCSID);
    }
    if (verbose)
	printf("# output data file: %s\n", fname_out);

    g_free (fname_base);
    return (err_msg);
}



int 
main(int argc, 
     char *argv[]
     )
/*-----------------------------------------------------------------------------
*/
{
    char *err_msg = NULL, *c = NULL;
    FILE *fp_par_dat, *fp;
    gchar fname_in[GPIV_MAX_CHARS],
        fname_out[GPIV_MAX_CHARS],
	fname_parameter[GPIV_MAX_CHARS];

    GpivPostPar *piv_post_par = g_new (GpivPostPar, 1);;
    GpivPivData *in_data = NULL;
    GpivScalarData *out_data = NULL;


    verbose = FALSE;
    use_stdin_stdout = FALSE;
    piv_post_par->operator_vorstra__set = FALSE;
    piv_post_par->diff_type__set = FALSE;

/*
 * Define GpivOperation type from program name, which is a symbolic link to 
 * gpiv_vorstra
 */
    if ((c = strstr(argv[0], "vorstra")) != NULL) {

    } else if ((c = strstr(argv[0], "vorty")) != NULL) {
	piv_post_par->operator_vorstra = GPIV_VORTICITY;
	piv_post_par->operator_vorstra__set = TRUE;
 
   } else if ((c = strstr(argv[0], "nstrain")) != NULL) {
	piv_post_par->operator_vorstra = GPIV_N_STRAIN;
	piv_post_par->operator_vorstra__set = TRUE;

    } else if ((c = strstr(argv[0], "sstrain")) != NULL) {
	piv_post_par->operator_vorstra = GPIV_S_STRAIN;
	piv_post_par->operator_vorstra__set = TRUE;

    } else {
        gpiv_error("vorstra: unvalid program name or symlink");
    }


    command_args (argc, argv, fname_in, piv_post_par);
    if (verbose) {
      printf("# %s\n# Command line options:\n", RCSID);
      gpiv_post_print_parameters (NULL, piv_post_par);
    }


    if (use_stdin_stdout == FALSE) {
	make_fname(fname_in, fname_parameter, fname_out, piv_post_par);

/*
 * Prints command line parameters to par-file
 */
	if ((fp_par_dat = fopen(fname_parameter, "a")) == NULL)
	     gpiv_error("\n%s: failure opening %s for input",
			argv[0], fname_parameter);
      fprintf(fp_par_dat, "# %s\n# Command line options:\n", argv[0]);
      gpiv_post_print_parameters (fp_par_dat, piv_post_par);

/*
 * Reading parametes from PARFILE (and writing to data par-file)
 */
        gpiv_scan_parameter (GPIV_POSTPAR_KEY, PARFILE, piv_post_par, verbose);
        if ((err_msg =
             gpiv_scan_resourcefiles (GPIV_POSTPAR_KEY, piv_post_par, verbose))
            != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);
        gpiv_post_print_parameters (fp_par_dat, piv_post_par);
	fclose (fp_par_dat);


    } else {
        gpiv_scan_parameter (GPIV_POSTPAR_KEY, PARFILE, piv_post_par, verbose);
        if ((err_msg =
             gpiv_scan_resourcefiles (GPIV_POSTPAR_KEY, piv_post_par, verbose))
            != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);
    }

    gpiv_post_check_parameters_read (piv_post_par, NULL);


/*
 * Check parameters on correct values and adjust belonging variables
 */
    if (piv_post_par->diff_type__set) {
	 if (piv_post_par->diff_type != 0 
             && piv_post_par->diff_type != 1 
             && piv_post_par->diff_type != 2
	     && piv_post_par->diff_type != 3)
	      gpiv_error("%s error: no value differential differentiator type",
			 argv[0]);
    }

    if (use_stdin_stdout == TRUE && gnuplot == 1) {
        gpiv_error ("%s: filename has to be used in combination with 'gnuplot'\n",
                    argv[0]);
    }

    if ((piv_post_par->operator_vorstra == GPIV_S_STRAIN 
	 || piv_post_par->operator_vorstra == GPIV_N_STRAIN)
	&& piv_post_par->diff_type == GPIV_CIRCULATION) {
        gpiv_error ("\n%s error: strain can not be calculated by circulation method",
                    argv[0]);
    }


/*
 * Reading input PIV data
 */
    if (use_stdin_stdout == TRUE) {
        fp = stdin;
    } else {
        if ((fp = fopen (fname_in, "rb")) == NULL) { 
            gpiv_error ("%s: Failure opening %s for input", argv[0], fname_in); 
        }
    }

    if ((in_data = gpiv_read_pivdata (fp)) == NULL) {
        gpiv_error ("%s: %s", argv[0], err_msg);
    }
    if (use_stdin_stdout == FALSE) fclose (fp);


/*
 * here the function calls of post-processing; calculating vorticity, strain
 */

    if ((out_data = gpiv_post_vorstra (in_data, piv_post_par)) == NULL) {
        gpiv_error ("%s: %s", argv[0], err_msg);
    }

/*
 * Adding comment to the data
 * And writing to output
 */
    g_free (out_data->comment);
    out_data->comment = g_strdup_printf ("# Software: %s\n", RCSID);
    out_data->comment = gpiv_add_datetime_to_comment (out_data->comment);
    if (piv_post_par->operator_vorstra == GPIV_VORTICITY) {
        out_data->comment = g_strconcat (out_data->comment, 
                                         "\n# Data type: vorticity [1/s]\n", 
                                         NULL);
    } else  if (piv_post_par->operator_vorstra == GPIV_N_STRAIN) {
        out_data->comment = g_strconcat (out_data->comment, 
                                         "\n# Data type: normal strain [1/s]\n", 
                                         NULL);
    } else  if (piv_post_par->operator_vorstra == GPIV_S_STRAIN) {
        out_data->comment = g_strconcat (out_data->comment, 
                                         "\n# Data type: shear strain [1/s]\n", 
                                         NULL);
    }


    if (use_stdin_stdout == TRUE) {
        fp = stdout;
    } else {
        if ((fp = fopen (fname_out, "wb")) == NULL) { 
            gpiv_error ("%s: Failure opening %s for output", argv[0], fname_out);
        }
    }

    if (gnuplot == 1) {
        if ((err_msg = gpiv_write_sc_griddata (fp, out_data, TRUE)) != NULL) {
            gpiv_error ("%s: %s", argv[0], err_msg);
        }
    } else {
        if ((err_msg = gpiv_write_scdata (fp, out_data, TRUE)) != NULL) {
            gpiv_error ("%s: %s", argv[0], err_msg);
        }
    }
    if (use_stdin_stdout == FALSE) fclose (fp);


/*
 * Freeing allocated memory of matrices
 */
    gpiv_free_pivdata(in_data);


/* 
 * Graphical output with gnuplot
 */
    if (gnuplot) {
        gchar title[GPIV_MAX_CHARS];

        snprintf (title, GPIV_MAX_CHARS, "vorticity of %s", fname_in);
	gpiv_scalar_gnuplot (fname_out, title, GNUPLOT_DISPLAY_COLOR, 
			    GNUPLOT_DISPLAY_SIZE);
    }
    
    exit (0);
}



