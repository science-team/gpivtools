/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*---------------------------------------------------------------------------

   aint - Calculates mean image intensity at each interrogation area

   Copyright (C) 2006 Gerber van der Graaf

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

------------------------------------------------------------------------*/

#include <stdio.h> 
#include <stdlib.h> 
#include <glib.h>
#include <gpiv.h>

#define PARFILE "gpivrc"     /* Parameter file name */
#define USAGE "\
Usage: gpiv_aint [ -h | --help] [-p | --print] [-s | --second]\n\
              [-V | --verbose] [-v | --version] filename \n\
\n\
keys: \n\
-h | --help:           this on-line help \n\
-p | --print:          print parameters to stdout \n\
-s | --second:         obtain averages from second image frame \n\
-V | --verbose:        prints data of interrogation area's and resulting mean \n\
-v | --version:        version number \n\
filename:              image file name \n\
"

#define HELP  "\
gpiv_aint calculates mean image intensity at each interrogation area"

#define RCSID "$Id: aint.c,v 2.5 2008-09-25 13:08:34 gerber Exp $"
/*
 * Extension of filename means: "Interrogation Area Intensity"
 */
#define AINT_EXT ".ain"

/*
 * Global variables
 */
gboolean second_image = FALSE;
gboolean print_par = FALSE, verbose = FALSE;

#ifdef DEBUG
/*
 * Variables for development version 
 */
int print_main=0;

#endif 


static gboolean
assign_img2intarr (gint ipoint_x,
                   gint ipoint_y,
                   guint16 **img_1,
                   guint16 **img_2,
                   gint int_size_f,
                   gint int_size_i,
                   gfloat **int_area_1,
                   gfloat **int_area_2,
                   gint pre_shift_row,
                   gint pre_shift_col,
                   gint nrows,
                   gint ncolumns,
                   gint int_size_0
                   );

static gboolean
assign_img2intarr_central (gint ipoint_x,
                           gint ipoint_y,
                           guint16 **img_1,
                           guint16 **img_2,
                           gint int_size_f,
                           gint int_size_i,
                           gfloat **int_area_1,
                           gfloat **int_area_2,
                           gint pre_shift_row,
                           gint pre_shift_col,
                           gint nrows,
                           gint ncolumns,
                           gint int_size_0
                           );

static gfloat
int_mean (gfloat **int_area,
          gint int_size
          );

static void 
command_args (int argc, 
              char *argv[], 
              char fname[GPIV_MAX_CHARS]
              );

static char *
make_fname (char *fname, 
            char *fname_par, 
            char *fname_piv, 
            char *fname_out
            );

static GpivScalarData *
post_ia_intensity (GpivImage *image,  
                   GpivPivData *piv_data, 
                   GpivPivPar *piv_par
                   );

static GpivScalarData *
post_ia_intensity (GpivImage *image,  
                   GpivPivData *piv_data, 
                   GpivPivPar *piv_par
                   )
/* ----------------------------------------------------------------------------
 * Calculates mean image intensity for each I.A. 
 */
{
    GpivScalarData *sc_data = NULL;

    gint i = 0, j = 0;
    gfloat **int_area_1;
    gfloat **int_area_2;
    GpivImage *loc_image = NULL;

    gboolean flag_accept = FALSE;
    gint pre_shift_col_act = 0, pre_shift_row_act = 0;


    assert (image->frame1[0] != NULL);
    assert (image->frame2[0] != NULL);

    assert (piv_data->point_x != NULL);
    assert (piv_data->point_y != NULL);
    assert (piv_data->dx != NULL);
    assert (piv_data->dy != NULL);
    assert (piv_data->snr != NULL);
    assert (piv_data->peak_no != NULL);

    int_area_1 = gpiv_matrix (piv_par->int_size_f, piv_par->int_size_f);
    int_area_2 = gpiv_matrix (piv_par->int_size_f, piv_par->int_size_f);


    sc_data = gpiv_alloc_scdata (piv_data->nx, piv_data->ny);

/*
 * Deform entire image
 */
    if (piv_par->int_scheme == GPIV_IMG_DEFORM) {
        loc_image = gpiv_cp_img (image);
        gpiv_imgproc_deform (loc_image, piv_data);
    } else {
        loc_image->frame1 = image->frame1;
        loc_image->frame2 = image->frame2;
    }

/*
 * For each I.A. obtain image data
 * Source: gpiv_piv_interr_reg
 */
    if (piv_par->int_scheme == GPIV_ZERO_OFF_CENTRAL 
        || piv_par->int_scheme == GPIV_ZERO_OFF_FORWARD
        ) {

#ifdef DEBUG
        g_message("post_ia_intensity:: assign_img2intarr_central");
#endif /* DEBUG */

        flag_accept = FALSE;
        pre_shift_col_act = piv_data->dx[i][j] + piv_par->pre_shift_col;
        pre_shift_row_act = piv_data->dy[i][j] + piv_par->pre_shift_row;
        for (i = 0; i < piv_data->ny; i++) {
            for (j = 0; j < piv_data->nx; j++) {
                flag_accept = 
                    assign_img2intarr_central ((int) piv_data->point_x[i][j], 
                                               (int) piv_data->point_y[i][j],
                                               loc_image->frame1, 
                                               loc_image->frame2,
                                               piv_par->int_size_f, 
                                               piv_par->int_size_f,
                                               int_area_1, 
                                               int_area_2,
                                               pre_shift_row_act,
                                               pre_shift_col_act,
                                               image->header->nrows,
                                               image->header->ncolumns,
                                               piv_par->int_size_f);
/*
 * Calculate average value and apply to out_data
 */
                sc_data->point_x[i][j] = piv_data->point_x[i][j];
                sc_data->point_y[i][j] = piv_data->point_y[i][j];
                if (flag_accept) {
                    sc_data->flag[i][j] = 1;
                    if (second_image) {
                        sc_data->scalar[i][j] = int_mean(int_area_2, 
                                                          piv_par->int_size_f);
                    } else {
                        sc_data->scalar[i][j] = int_mean(int_area_1, 
                                                          piv_par->int_size_f);
                        if (verbose) printf("x=%d y=%d mean = %f \n\n",
                                            (int)sc_data->point_x[i][j],
                                            (int)sc_data->point_x[i][j],
                                            sc_data->scalar[i][j]);
                    }
                } else {
                    sc_data->flag[i][j] = 0;
                    sc_data->scalar[i][j] = 0.0;
                }
            }
        }
    } else {

#ifdef DEBUG
        g_message("post_ia_intensity:: assign_img2intarr");
#endif /* DEBUG */
        flag_accept = FALSE;
        pre_shift_col_act = piv_par->pre_shift_col;
        pre_shift_row_act = piv_par->pre_shift_row;
        for (i = 0; i < piv_data->ny; i++) {
            for (j = 0; j < piv_data->nx; j++) {
                flag_accept = 
                     assign_img2intarr((int) piv_data->point_x[i][j], 
                                       (int) piv_data->point_y[i][j],
                                       loc_image->frame1, 
                                       loc_image->frame2,
                                       piv_par->int_size_f, 
                                       piv_par->int_size_f,
                                       int_area_1, 
                                       int_area_2,
                                       pre_shift_row_act,
                                       pre_shift_col_act,
                                       image->header->nrows,
                                       image->header->ncolumns,
                                       piv_par->int_size_f);
/*
 * Calculate average value and apply to out_data
 */
                sc_data->point_x[i][j] = piv_data->point_x[i][j];
                sc_data->point_y[i][j] = piv_data->point_y[i][j];
                if (flag_accept) {
                    sc_data->flag[i][j] = 1;
                    if (second_image) {
                        sc_data->scalar[i][j] = int_mean(int_area_2, 
                                                          piv_par->int_size_f);
                    } else {
                        sc_data->scalar[i][j] = int_mean(int_area_1, 
                                                          piv_par->int_size_f);
                        if (verbose) printf("x=%d y=%d mean = %f \n\n",
                                            (int)sc_data->point_x[i][j],
                                            (int)sc_data->point_x[i][j],
                                            sc_data->scalar[i][j]);
                    }
                } else {
                    sc_data->flag[i][j] = 0;
                    sc_data->scalar[i][j] = 0.0;
                }
            }
        }
    }

/*
 * Free allocated memory
 */
    gpiv_free_matrix(int_area_1);
    gpiv_free_matrix(int_area_2);
    if (piv_par->int_scheme == GPIV_IMG_DEFORM) {
        gpiv_free_img (loc_image);
    }

    return sc_data;
}


static gboolean
assign_img2intarr(gint ipoint_x,
                  gint ipoint_y,
                  guint16 **img_1,
                  guint16 **img_2,
                  gint int_size_f,
                  gint int_size_i,
                  gfloat **int_area_1,
                  gfloat **int_area_2,
                  gint pre_shift_row,
                  gint pre_shift_col,
                  gint nrows,
                  gint ncolumns,
                  gint int_size_0
                  )
/*-----------------------------------------------------------------------------
 * Assigns image data to the interrogation area arrays straightforward
 */
{
    gint m, n;
    gint arg_int1_r = ipoint_y  - int_size_f / 2 + 1;
    gint arg_int1_c = ipoint_x  - int_size_f / 2 + 1;
    gint arg_int2_r = ipoint_y  - int_size_i / 2 + 1;
    gint arg_int2_c = ipoint_x  - int_size_i / 2 + 1;

    gboolean flag_valid;

#ifdef DEBUG
    g_message("assign_img2intarr:: 0 arg_int1_r = %d arg_int1_c = %d",
              arg_int1_r, arg_int1_c);
    g_message("assign_img2intarr:: 0 arg_int2_r = %d arg_int2_c = %d",
              arg_int2_r, arg_int2_c);
#endif /* DEBUG */
/*
 * Check if Interrogation Areas are within the image boundaries.
 * Principally arg_int1_r,c don't have to be tested as 
 * int_size_i >= int_size_f, but has been kept to maintain generallity with the
 * other assign_img2intarr* functions
 */
    if ((arg_int1_r) >= 0
         && (arg_int1_r + int_size_f - 1) < nrows
         && (arg_int1_c) >= 0
         && (arg_int1_c + int_size_f - 1) < ncolumns

         && (arg_int2_r) >= 0
         && (arg_int2_r + int_size_i - 1) < nrows
         && (arg_int2_c) >= 0
         && (arg_int2_c + int_size_i - 1) < ncolumns) {
        flag_valid = TRUE;
    } else {
        flag_valid = FALSE;
    }


#ifdef DEBUG
     g_message("assign_img2intarr:: 1 flag_valid=%d", flag_valid);
#endif /* DEBUG */
    if (flag_valid == TRUE) {
/*
 * reset int_area_1, int_area_2 values
 */
        memset(int_area_1[0], 0.0, (sizeof(gfloat)) * int_size_0 * int_size_0);
        memset(int_area_2[0], 0.0, (sizeof(gfloat)) * int_size_0 * int_size_0);

        if (verbose) printf("assign_img2intarr: x = %d y = %d",
                            ipoint_x, ipoint_y);
        for (m = 0; m < int_size_f; m++) {
           if (verbose) printf("\n");
             for (n = 0; n < int_size_f; n++) {
                int_area_1[m][n] =
                    (float) img_1[m + arg_int1_r][n + arg_int1_c];
                if (verbose) printf("%d ", (int) int_area_1[m][n]);
             }
        }
        if (verbose) printf("\n\n");

        for (m = 0; m < int_size_i; m++) {
            for (n = 0; n < int_size_i; n++) {
                int_area_2[m][n] =
                    (float) img_2[m + arg_int2_r][n + arg_int2_c];
            }
        }
    }

#ifdef DEBUG
    g_message("assign_img2intarr:: END");
#endif /* DEBUG */
    return flag_valid;
}



static gboolean
assign_img2intarr_central(gint ipoint_x,
                          gint ipoint_y,
                          guint16 **img_1,
                          guint16 **img_2,
                          gint int_size_f,
                          gint int_size_i,
                          gfloat **int_area_1,
                          gfloat **int_area_2,
                          gint pre_shift_row,
                          gint pre_shift_col,
                          gint nrows,
                          gint ncolumns,
                          gint int_size_0
                          )
/*-----------------------------------------------------------------------------
 * Assigns image data to the interrogation area arrays for central differential
 * scheme
 */
{
    gint m, n;
    gint idum = gpiv_max((int_size_i - int_size_f) / 2, 0);
    gint arg_int1_r = ipoint_y - int_size_f / 2 + 1 - pre_shift_row / 2 -
        pre_shift_row % 2;
    gint arg_int1_c = ipoint_x - int_size_f / 2 + 1 - pre_shift_col / 2 -
        pre_shift_col % 2;
    gint arg_int2_r = ipoint_y - int_size_i / 2 + 1 + pre_shift_row / 2;
    gint arg_int2_c = ipoint_x - int_size_i / 2 + 1 + pre_shift_col / 2;
    gboolean flag_valid;

#ifdef DEBUG
    g_message("assign_img2intarr_central:: 0 arg_int1_r = %d arg_int1_c = %d",
              arg_int1_r, arg_int1_c);
    g_message("assign_img2intarr_central:: 0 arg_int2_r = %d arg_int2_c = %d",
              arg_int2_r, arg_int2_c);
#endif /* DEBUG */
/*
 * Check if Interrogation Areas are within the image boundaries.
 */
     if ((arg_int1_r) >= 0
         && (arg_int1_r + int_size_f - 1) < nrows
         && (arg_int1_c) >= 0
         && (arg_int1_c + int_size_f - 1) < ncolumns

         && (arg_int2_r) >= 0
         && (arg_int2_r + int_size_i - 1) < nrows
         && (arg_int2_c) >= 0
         && (arg_int2_c + int_size_i - 1) < ncolumns) {
         flag_valid = TRUE;
     } else {
         flag_valid = FALSE;
     }


#ifdef DEBUG
     g_message("assign_img2intarr_central:: 1 flag_valid=%d", flag_valid);
#endif /* DEBUG */
    if (flag_valid == TRUE) {
/*
 * reset int_area_1, int_area_2 values
 */
        memset(int_area_1[0], 0.0, (sizeof(gfloat)) * int_size_0 * int_size_0);
        memset(int_area_2[0], 0.0, (sizeof(gfloat)) * int_size_0 * int_size_0);

        if (verbose) printf("assign_img2intarr_central: x = %d y = %d",
                            ipoint_x, ipoint_y);
        for (m = 0; m < int_size_f; m++) {
           if (verbose) printf("\n");
            for (n = 0; n < int_size_f; n++) {
                int_area_1[m + idum][n + idum] =
                    (float) img_1[m + arg_int1_r][n + arg_int1_c];
                    if (verbose) printf(" %d ", (int) int_area_1[m][n]);
            }
        }
        if (verbose) printf("\n\n");

        for (m = 0; m < int_size_i; m++) {
            for (n = 0; n < int_size_i; n++) {
                int_area_2[m][n] =
                    (float) img_2[m + arg_int2_r][n + arg_int2_c];
            }
        }

    }

#ifdef DEBUG
    g_message("assign_img2intarr_central:: END");
#endif /* DEBUG */
    return flag_valid;
}



static gfloat
int_mean(gfloat **int_area,
         gint int_size
         )
/* ----------------------------------------------------------------------------
 * calculates mean value from interrogation area
 */
{
    int m = 0, n = 0;
    gfloat int_area_sum = 0;
    gfloat mean = 0.0;

    for (m = 0; m < int_size; m++) {
        for (n = 0; n < int_size; n++) {
            int_area_sum += int_area[m][n];
        }
    }
    mean = int_area_sum / (int_size * int_size);

    return mean;
}



static void 
command_args (int argc, 
              char *argv[], 
              char fname[GPIV_MAX_CHARS]
              )
/* ----------------------------------------------------------------------------
 * Command line argument handling 
 */
{
    char c;
    int argc_next;


    while (--argc > 0 && (*++argv)[0] == '-') {
        argc_next=0;
/*
 * argc_next is set to 1 if the next cmd line argument has to be searched for; 
 * in case that the command line argument concerns more than one char or cmd 
 * line argument needs a parameter
 */
        while (argc_next == 0 && (c = *++argv[0])) {
            switch (c) {

            case 'v':
/*
 * Use Revision Control System (RCS) for version
 */
                printf ("%s\n", RCSID); 
                exit (0);
                break;

            case 'h':
                printf ("%s\n", argv[0]); 
                printf ("%s\n", HELP);
                printf ("%s\n", USAGE);
                exit (0);
                break;

            case 'p':
                print_par = TRUE;
                break;
                
            case 's':
                second_image = TRUE;
                break;

            case 'V':
                verbose = TRUE;
                break;
                
/*
 * long option keys
 */
	    case '-':
		if (strcmp ("-help", *argv) == 0) {
                    printf ("\n%s", argv[0]);
                    printf ("\n%s", HELP);
                    printf ("\n%s", USAGE);
                    exit (0);
                } else if (strcmp ("-print", *argv) == 0) {
		    print_par = TRUE;
                } else if (strcmp ("-second", *argv) == 0) {
		    second_image = TRUE;
                } else if (strcmp ("-verbose", *argv) == 0) {
		    verbose = TRUE;
                } else if (strcmp ("-version", *argv) == 0) {
                    printf ("%s\n", RCSID);
                    exit (0);
                } else {
		    gpiv_error ("%s: unknown option: %s", argv[0], *argv);
		}
		argc_next = 1;
		break;

            default:
                fprintf (stderr, USAGE);
                exit (1);
                break;
            }
        }
    }


    if (argc == 1) {
        strcpy (fname, *argv); 
    } else {
        gpiv_error ("%s: %s", argv[0], USAGE);
    }

}



static gchar *
make_fname (char *fname, 
            char *fname_par, 
            char *fname_piv, 
            char *fname_out
            )
/*  ---------------------------------------------------------------------------
 * function to generate filenames
 */
{
    gchar *err_msg = NULL;
    gchar *fname_base = NULL;

    if (fname == NULL ) {
        err_msg = "make_fname: \"fname == NULL\"";
        return (err_msg);
    }
    fname_base = g_strdup (fname);
    strtok (fname_base, ".");

    gpiv_io_make_fname (fname_base, GPIV_EXT_PAR, fname_par);
    if (print_par)     printf("# Parameter file: %s\n",fname_par);
     
    gpiv_io_make_fname (fname_base, GPIV_EXT_PIV, fname_piv);
    if (print_par)     printf("# PIV data file: %s\n",fname_piv);

    gpiv_io_make_fname (fname_base, AINT_EXT, fname_out);
    if (print_par)     printf("# Output file: %s\n",fname_out);
     
    g_free (fname_base);
    return (err_msg);
}



int
main (int argc, 
      char *argv[]
      )
/* ----------------------------------------------------------------------------
 *  Start of the main program
 */
{
    gchar * err_msg = NULL;
    FILE  *fp, *fp_par;
    gchar fname[GPIV_MAX_CHARS], fname_out[GPIV_MAX_CHARS], 
        fname_par[GPIV_MAX_CHARS], fname_piv[GPIV_MAX_CHARS];

    GpivPivPar *piv_par = g_new0 (GpivPivPar, 1);
    GpivPostPar *post_par = g_new0 (GpivPostPar, 1);
    
    GpivImage *image = NULL;
    GpivPivData *piv_in_data = NULL;
    GpivScalarData *sc_out_data = NULL;
    


    gpiv_piv_parameters_set (piv_par, FALSE);
    gpiv_post_parameters_set (post_par, FALSE);
     

    command_args (argc, argv, fname);


/*
 * Generating proper filenames
 */
    if ((err_msg = 
         make_fname (fname, fname_par, fname_piv, fname_out)) 
        != NULL) {
        gpiv_error ("%s: Failure calling make_fname", argv[0]);
    }
	  

/*
 * Prints command line parameters to par-file 
 */
    if ((fp_par = fopen (fname_par, "a")) == NULL) {
        gpiv_error ("\n%s: failure opening %s for input",
                    argv[0], fname_par);
    }
    fprintf (fp_par, "\n\n# %s\n# Command line options:\n", RCSID); 

/*
 * Reading parametes from PARFILE and (system) resources (and writing to data 
 * par-file)
 */
    
    gpiv_scan_parameter (GPIV_PIVPAR_KEY, PARFILE, piv_par, print_par);
    if ((err_msg =
         gpiv_scan_resourcefiles (GPIV_PIVPAR_KEY, piv_par, print_par))
        != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);
    gpiv_piv_print_parameters (fp_par, piv_par);
    
    gpiv_scan_parameter (GPIV_POSTPAR_KEY, PARFILE, post_par, print_par);
    if ((err_msg =
         gpiv_scan_resourcefiles (GPIV_POSTPAR_KEY, post_par, print_par))
        != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);
    gpiv_post_print_parameters (fp_par, post_par);


    if ((err_msg = 
         gpiv_post_check_parameters_read (post_par, NULL))
        != NULL) gpiv_error ("%s: %s", argv[0], err_msg);

/*
 * Read input image
 */
        if ((image = gpiv_fread_image (fname)) == NULL) {
            gpiv_error ("%s: gpiv_fread_image_ni", argv[0]);
        }


/*
 * Read PIV data for input
 */
        if ((fp = fopen (fname_piv, "rb")) == NULL) {
            guint nx, ny;

        fprintf (fp_par, "# WARNING: no PIV data found; particle displacements set to 0");
        if (print_par) gpiv_warning ("Particle displacements set to 0");

        if ((err_msg = 
             gpiv_piv_count_pivdata_fromimage (image->header, piv_par, &nx, &ny))
            != NULL) gpiv_error ("%s: %s", argv[0], err_msg);

        if ((piv_in_data = 
             gpiv_piv_gridgen (nx, ny, image->header, piv_par))
            == NULL) gpiv_error ("%: %s", argv[0], err_msg);

        if ((err_msg = 
             gpiv_0_pivdata (piv_in_data))
            != NULL) gpiv_error (": %s", argv[0], err_msg);

        } else {
            if ((piv_in_data =  gpiv_read_pivdata (fp)) 
                == NULL)  gpiv_error ("%s: %s", argv[0], err_msg);
            fclose(fp);
        }

        fclose(fp_par);

    
/*
 * Here the function calls of the post-processing; intensity of
 * Interrogation Area's
 */
    if (print_par == TRUE) printf ("\n");

    if ((sc_out_data = 
         post_ia_intensity (image, piv_in_data, piv_par)) 
        == NULL) gpiv_error ("%s: Failure %s", argv[0], err_msg);

/*
 *  And writing to output
 */
    if ((fp = fopen (fname_out, "wb")) == NULL) {
        gpiv_error ("%s: failing opening %s", argv[0], fname_out);
    }
    if ((err_msg =
         gpiv_write_scdata (fp, sc_out_data, TRUE))
        != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);
    fclose (fp);

/*
 * Freeing allocated memory
 */
    gpiv_free_pivdata (piv_in_data);
    gpiv_free_img (image);
    if (print_par == TRUE) printf ("\n");
    exit (0);
}
