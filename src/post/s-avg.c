/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*---------------------------------------------------------------------------
Descripion:
  s-avg - reports spatial global mean velocity and rms error of a PIV data 
         stream. Eventually subtracts mean value from piv the data.

   Copyright (C) 2002, 2003, 2004 Gerber van der Graaf

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

------------------------------------------------------------------------*/

#include <stdio.h> 
#include <stdlib.h> 
#include <math.h>
#include <glib.h>
#include <gpiv.h>


/* #define PARFILE "savg.par"     */ /* Parameter file name */
#define PARFILE "gpivrc"     /* Parameter file name */
#define USAGE "\
Usage: scale [-h | --help] [-p | --print] [-s|-no_s] \n\
             [-v | --version] [-z dx dy] [filename] < stdin > stdout \n\
\n\
keys: \n\
-h | --help:           this on-line help \n\
-p | --print:          print parameters to stdout \n\
-s:                    subtracts nothing (0), mean of dx and dy (2) from \n\
                       displacements or velocity estimators \n\
-no_s:                 suppresses subtracting mean from input data \n\
-v | --version:        prints version \n\
-z dx dy:              zero offset of velocities/displacements \n\
filename:              input PIV file. Substitutes stdin and stdout \n\
"


#ifdef DEBUG
#define USAGE_DEBUG "\
Developers version also contains:  \n\
               [-p_main] \n\
keys: \n\
-p_'function' N; prints data to be generated in the function; the \n\
                   higher N, the more detailed the output. \n\
                   For N = 10, err_vec will exit at the end of the function"
#endif


#define HELP  "\
s-avg reports spatial global mean velocity and rms error of a PIV data stream"

#define RCSID "$Id: s-avg.c,v 2.14 2008-09-25 13:08:35 gerber Exp $"

/*
 * Global variables 
*/
gboolean use_stdin_stdout = FALSE;
gboolean verbose = FALSE;

#ifdef DEBUG
/*
 * Variables for development version
*/
int print_main=0;
#endif 



static void 
command_args(int argc, char *argv[], 
             char fname[GPIV_MAX_CHARS],
             GpivPostPar * piv_post_par
             )
/* ----------------------------------------------------------------------------
 * Command line argument handling
 */
{
    char c;
    int argc_next;


    while (--argc > 0 && (*++argv)[0] == '-') {
        argc_next=0;

/*
 * argc_next is set to 1 if the next cmd line argument has to be
 * searched for; in case that the command line argument concerns more
 * than one char or cmd line argument needs a parameter
*/
        while (argc_next==0 && (c = *++argv[0])) {
            switch (c) {

            case 'v':
/*
 * Use Revision Control System (RCS) for version
 */
                printf("%s\n", RCSID); 
                exit(0);

            case 'h':
          printf("%s\n", argv[0]); 
          printf("%s\n",HELP);
          printf("%s\n",USAGE);
#ifdef DEBUG
          printf ("\n%s\n",USAGE_DEBUG);
#endif
          exit(0);
/*
 * Spatial scaling
 */
            case 's':
                piv_post_par->subtract = atoi(*++argv);
                piv_post_par->subtract__set = TRUE;
                argc_next = 1;
                break;
                
            case 'p':
#ifdef DEBUG
                if ((strcmp(*argv,"p_main" ) !=0)) {
#endif
                    verbose = TRUE;
#ifdef DEBUG
                } else if (strcmp("p_main",*argv) == 0) {  
                    print_main=atoi(*++argv);
                    --argc; 
	    argc_next=1;
                }	  
#endif /* DEBUG */
                break;
                
            case 'z':
                piv_post_par->z_off_dx = atof(*++argv);
                piv_post_par->z_off_dy = atof(*++argv);
                fprintf(stderr, "\n000:: z_off_dx=%f", piv_post_par->z_off_dx);
                fprintf(stderr, "  z_off_dy=%f\n", piv_post_par->z_off_dy);
                piv_post_par->z_off_dx__set = TRUE;
                piv_post_par->z_off_dy__set = TRUE;
                argc_next = 1;
                --argc;
                --argc;
                argc_next = 1;
                break;

/*
 * negotion of settings
 */            case 'n': 
		if (strcmp("no_s", *argv) == 0) {    /* --- do not subtract mean -*/
                    piv_post_par->subtract = 0;
                    piv_post_par->subtract__set = TRUE;
                }
                break;


/*
 * long option keys
 */
	    case '-':
		if (strcmp("-help", *argv) == 0) {
                    printf("\n%s", argv[0]);
                    printf("\n%s", HELP);
                    printf("\n%s", USAGE);
                    exit(0);
                } else if (strcmp("-print", *argv) == 0) {
		    verbose = TRUE;
                } else if (strcmp("-version", *argv) == 0) {
                    printf("%s\n", RCSID);
                    exit(0);
                } else {
		    gpiv_error("%s: unknown option: %s", argv[0], *argv);
		}
		argc_next = 1;
		break;

            default:
                fprintf (stderr,USAGE);
#ifdef DEBUG
                printf ("\n%s",USAGE_DEBUG);
#endif
                exit(1);
                break;
            }
        }
    }
    
    /*
     * Check if filename or stdin /stdout is used
     */
    if (argc == 1) {
        use_stdin_stdout = FALSE;
        strcpy(fname, argv[argc - 1]);
    } else if (argc == 0) {
        use_stdin_stdout = TRUE;
        verbose = FALSE;
    } else {
#ifdef DEBUG
        printf ("\n%s", USAGE_DEBUG);
#endif
        gpiv_error("%s: %s", argv[0], USAGE);
    }
}



static gchar *
make_fname (char *fname_in, 
           char *fname_parameter, 
           char *fname_out
           )
/* ----------------------------------------------------------------------------
 * function to generate filenames
 */
{
    gchar *err_msg = NULL;
    gchar *fname_base = NULL;

    if (fname_in == NULL ) {
        err_msg = "make_fname: \"fname_in == NULL\"";
        return (err_msg);
    }
 
    /*
     * Stripping filename
     */
    fname_base = g_strdup (fname_in);
    strtok (fname_base, ".");

    /*
     * filenames for output PIV data
     */
    gpiv_io_make_fname (fname_base, GPIV_EXT_PAR, fname_parameter);
    if (verbose) printf ("# Data parameter file: %s\n", fname_parameter);
    
    gpiv_io_make_fname (fname_base, GPIV_EXT_SA, fname_out);
    if (verbose) printf ("# Output file: %s\n", fname_out);

    return (err_msg);    
}



int 
main (int argc, 
      char *argv[]
      )
/*-----------------------------------------------------------------------------
*/
{
    gchar *err_msg = NULL;
    FILE  *fp_par_dat, *fp;
    gchar fname_in[GPIV_MAX_CHARS], 
        fname_out[GPIV_MAX_CHARS], 
        fname_parameter[GPIV_MAX_CHARS];

    GpivPivData *piv_data = NULL;
    GpivPostPar *piv_post_par = g_new (GpivPostPar, 1);


    gpiv_post_parameters_set (piv_post_par, FALSE);
    command_args (argc, argv, fname_in, piv_post_par);
    if (verbose) {
        printf ("# %s\n# Command line options:\n", RCSID);
        gpiv_post_print_parameters (NULL, piv_post_par);
    }
    

/*
 * Reading parametes from PARFILE and resources
 * Check parameters on correct values and adjust belonging variables
 */
    gpiv_scan_parameter (GPIV_POSTPAR_KEY, PARFILE, piv_post_par, verbose);
    if ((err_msg =
         gpiv_scan_resourcefiles (GPIV_POSTPAR_KEY, piv_post_par, verbose))
        != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);
    gpiv_post_check_parameters_read (piv_post_par, NULL);

    if (use_stdin_stdout == FALSE) {
        if ((err_msg = 
             make_fname (fname_in, fname_parameter, fname_out))
            != NULL) {
            gpiv_error ("%s: %s\n", argv[0], err_msg);
        }
/*
 * Prints parameters to par-file 
 */
        if ((fp_par_dat = fopen (fname_parameter, "a")) == NULL) {
            gpiv_error ("%s: failure opening %s for input\n", argv[0],
                        fname_parameter);
        }
        fprintf (fp_par_dat, "# %s\n# Post parameters:\n", argv[0]);
        gpiv_post_print_parameters (fp_par_dat, piv_post_par);
	fclose (fp_par_dat);
    }
    
/* 
 * Reading input file of piv data 
 */
#ifdef DEBUG
    if (print_main >= 1) fprintf (stderr,"\ncalling (f)gpiv_read_pivdata\n");
#endif

    if (use_stdin_stdout == TRUE) {
        fp = stdin;
    } else {
        if ((fp = fopen (fname_in, "rb")) == NULL) {
            gpiv_error ("%s: failure opening %s for reading", 
                        argv[0], fname_in);
        }
    }

    if ((piv_data = gpiv_read_pivdata (fp)) == NULL) {
        gpiv_error ("%s: %s", argv[0], err_msg);
    }
    if (use_stdin_stdout == FALSE) fclose (fp);

/*
 * Here the function call of the program body
 */
    if (verbose == TRUE) printf ("\n");

#ifdef DEBUG
    g_message ("\ncalling gpiv_post_savg\n");
#endif


    if ((err_msg = gpiv_post_savg (piv_data, piv_post_par)) != NULL) {
        gpiv_error ("%s: ", argv[0], err_msg);
    }

/*
 * Adding comment to the data
 * And writing data to output 
 */
    g_free (piv_data->comment);
    piv_data->comment = g_strdup_printf ("# Software: %s\n", RCSID);
    piv_data->comment = gpiv_add_datetime_to_comment (piv_data->comment);

    if (piv_post_par->subtract == TRUE) {
        if (use_stdin_stdout == TRUE) {
            fp = stdout;
        } else {
            if ((fp = fopen (fname_out, "wb")) == NULL) {
                gpiv_error ("%s: failure opening %s for writing", 
                            argv[0], fname_out);
            }
        }

        if ((err_msg = gpiv_write_pivdata (fp, piv_data, FALSE)) != NULL) {
            gpiv_error ("%s: %s", argv[0], err_msg);
        }
        if (use_stdin_stdout == FALSE) fclose (fp);


    } else {
        if (use_stdin_stdout == TRUE) {
            printf ("# Software: %s\n", RCSID);
            printf ("# %s\n", gpiv_add_datetime_to_comment (NULL));
            printf ("# ndata = %d\n", piv_data->count);
            printf ("# mean_dx = %f sdev_dx = %f min_dx = %f max_dx = %f\n", 
                    piv_data->mean_dx, 
                    piv_data->sdev_dx, 
                    piv_data->min_dx,  
                    piv_data->max_dx);
            printf ("# mean_dy = %f sdev_dy = %f min_dy = %f max_dy = %f\n", 
                    piv_data->mean_dy, 
                    piv_data->sdev_dy, 
                    piv_data->min_dy,  
                    piv_data->max_dy);

        } else {
            if ((fp = fopen (fname_out,"wb")) == NULL) { 
                gpiv_error ("\n%s: Failure opening %s for output\n",
                            argv[0], fname_out); 
            }

            fprintf (fp, "# Software: %s\n", RCSID);
            fprintf (fp, "# %s\n", gpiv_add_datetime_to_comment (NULL));
            fprintf (fp, "# ndata = %d\n", piv_data->count);
            fprintf (fp, "# mean_dx = %f sdev_dx = %f min_dx = %f max_dx = %f\n", 
                     piv_data->mean_dx, 
                     piv_data->sdev_dx, 
                     piv_data->min_dx,  
                     piv_data->max_dx);
            fprintf (fp, "# mean_dy = %f sdev_dy = %f min_dy = %f max_dy = %f\n", 
                     piv_data->mean_dy, 
                     piv_data->sdev_dy, 
                     piv_data->min_dy,  
                     piv_data->max_dy);

        }
    }
    
/*
 * Freeing allocated memory of matrices 
 */
    gpiv_free_pivdata (piv_data);    
    if (verbose == TRUE) printf ("\n");
    exit (0);
}




