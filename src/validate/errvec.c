/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/* -----------------------------------------------------------------------------

   errvec - searches the erroneous vectors in PIV data and
   substitutes with new values, if possible

   Copyright (C) 2002, 2003, 2004 Gerber van der Graaf

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  


------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gpiv.h>

/*#define  PARFILE "errvec.par" */	/* Parameter file name */
#define PARFILE "gpivrc"	/* Parameter file name */
#define USAGE "\
Usage: errvec [-a] [-b] [-c] [-g] [-h | --help] [-i | image_type]  [|--no_b]\n\
              [-n N] [-p | --print] [-r int] [-s int] [-t float] [-u | --surround] \n\
              [-v | --version] [filename] < stdin > stdout \n\
\n\
keys: \n\
-a:                    Automatic threshold; calculates threshhold from \n\
                       residu histogram \n\
-b:                    calculates residu statistics to print histogram of \n\
                       N bins \n\
-g:                    graphical visualization of residu statistcs with \n\
                       gnuplot (needs -f)\n\
-h | --help:           this on-line help \n\
-i | --image_type:     image type (tif, gif, bmp, pgm, r, gpi). Default: png. \n\
--no_b:                suppresses printing median residu histogram \n\
-n N:                  defines number of histogram of bins N for -b or -a \n\
-p | --print:          print parameters to stdout  \n\
-r N:                  residu type calculated from: snr (0), median  (1) \n\
                       or normalized median (2) \n\
-s N:                  substitution of erroneous vector by: nothing (0), \n\
                       local mean from the surroundings (1), \n\
                       the median of the surroundings (2), next highest \n\
                       correlation peak (3) (needs -f) \n\
-t F:                  threshhold of residus to be accepted \n\
-u | --surround N:     odd number representing the surrounding grid \n\
                       points to be used for local mean and median values. \n\
-v | --version:        version number \n\
filename:              input PIV file. Substitutes stdin and stdout \n\
"


#define HELP  "\
errvec searches the erroneous vectors in a PIV data set and eventually \n\
substitutes them with new values."

#define RCSID "$Id: errvec.c,v 2.16 2008-10-09 14:45:01 gerber Exp $"

/*
 * Global variables for the parameters
 */
static gboolean verbose = FALSE;
static gboolean use_stdin_stdout = FALSE;
static gchar *itype =  "png";
static gboolean itype__set = FALSE;

static guint nbins = 0;
/*
 *  Why does gpiv_count_pivdata work if struct is defined here, 
 *  but not in main?
 */
/* GpivPivData in_data, out_data; */
/* GpivBinData *klass = NULL; */
/* GpivLinRegData *linreg = g_new0 (GpivLinRegData, 1); */

#define GNUPLOT_DISPLAY_COLOR "DarkSlateGray"
#define GNUPLOT_DISPLAY_SIZE 250
#define NEW_INT

gboolean gnuplot = 0, gnuplot__set = FALSE;
gboolean res_stats = FALSE, res_stats__set = FALSE;
gboolean auto_thresh = FALSE, auto_thresh__set = FALSE;


/*
 * Function prototypes
 */

int 
command_args(int argc, 
             char *argv[], 
             char fname[GPIV_MAX_CHARS],
             GpivValidPar *valid_par
             )
/* ----------------------------------------------------------------------------
 * Command line argument handling
 */
{
    char c;
    int argc_next;


    while (--argc > 0 && (*++argv)[0] == '-') {
	argc_next = 0;
/*
 * argc_next is set to 1 if the next cmd line argument has to be searched for; 
 * in case that the command line argument concerns more than one char or cmd 
 * line argument needs a parameter
 */
	while (argc_next == 0 && (c = *++argv[0])) {
	    switch (c) {
	    case 'v':
/*
 * Use Revision Control System (RCS) for version
 */
		printf("%s\n", RCSID);
		exit(0);
		break;

	    case 'h':
		printf("%s\n", argv[0]);
		printf("%s\n", HELP);
		printf("%s\n", USAGE);
#ifdef DEBUG
		printf("\n%s\n", USAGE_DEBUG);
#endif
		exit(0);
		break;
/*
 * Automatic threshold value
 */
	    case 'a':
                auto_thresh = TRUE;
                auto_thresh__set = TRUE;
                valid_par->residu_max__set = TRUE;
		break;

	    case 'b':		/*  calculates median values */
		/*  to print out to stdout in histogram of N */
                res_stats = TRUE;
                res_stats__set = TRUE;
                break;

/*
 * Visualize residu statistics with gnuplot
 */
	    case 'g':
	        gnuplot = 1;
		gnuplot__set = TRUE;
/*
 * also set to calculate median values
 */
		res_stats = TRUE;
		res_stats__set = TRUE;
		break;

            case 'i':
                itype = g_strdup(*++argv);
                argc_next = 1;
                --argc;
                itype__set = TRUE;
                break;

	    case 'n':
                nbins = atoi(*++argv);
                argc_next = 1;
                --argc;
		break;

	    case 'p':
	        verbose = TRUE;
		break;
/*
 *  residu type: snr or median
 */
	    case 'r':
		valid_par->residu_type = atoi(*++argv);
		--argc;
		valid_par->residu_type__set = TRUE;
		argc_next = 1;
		break;
/*
 * substitution by mean, median or next highest cov. peak
 */
	    case 's':
		valid_par->subst_type = atoi(*++argv);
		valid_par->subst_type__set = TRUE;
		--argc;
		argc_next = 1;
		break;
/*
 * maximum residu value allowed for 
 */
	    case 't':
                valid_par->residu_max = atof(*++argv);
                valid_par->residu_max__set = TRUE;
                --argc;
                argc_next = 1;
		break;


	    case 'u':
                valid_par->neighbors = atoi(*++argv);
                valid_par->neighbors__set = TRUE;
                argc_next = 1;
                --argc;
		break;

/*
 * long option keys
 */
	    case '-':
		if (strcmp("-help", *argv) == 0) {
                    printf("\n%s", argv[0]);
                    printf("\n%s", HELP);
                    printf("\n%s", USAGE);
                    exit(0);
                } else if (strcmp("-print", *argv) == 0) {
		    verbose = TRUE;
                } else if (strcmp("-version", *argv) == 0) {
                    printf("%s\n", RCSID);
                    exit(0);
                } else if (strcmp("-no_b", *argv) == 0) {
		    res_stats = FALSE;
		    res_stats__set = TRUE;
                } else if (strcmp("-surround", *argv) == 0) {
                    valid_par->neighbors = atoi(*++argv);
                    valid_par->neighbors__set = TRUE;
                    argc_next = 1;
                    --argc;
                } else if (strcmp("-image_type", *argv) == 0) {
                    itype = g_strdup(*++argv);
                    argc_next = 1;
		    --argc;
                    itype__set = TRUE;

                } else {
		    gpiv_error("%s: unknown option: %s", argv[0], *argv);
		}
		argc_next = 1;
		break;

	    default:
#ifdef DEBUG
		printf("\n%s\n", USAGE_DEBUG);
#endif
		gpiv_error(USAGE);
		break;
	    }
	}
    }

    if (argc == 1) {
        use_stdin_stdout = FALSE;
        strcpy(fname, argv[argc - 1]);
    } else if (argc == 0) {
        use_stdin_stdout = TRUE;
        verbose = FALSE;
    } else {
#ifdef DEBUG
	printf("\n%s", USAGE_DEBUG);
#endif
        gpiv_error("\n%s: unknown argument: %s: %s", argv[0], *argv, USAGE);
    }

    return 0;
}



static gchar *
make_fname (char *fname_in, 
            char *fname_img,
            char *fname_parameter,
            char *fname_out
            )
/* ----------------------------------------------------------------------------
 * generates filenames
 */
{
    gchar *err_msg = NULL;
    gchar *fname_base = NULL;
    if (use_stdin_stdout == TRUE) {
	gpiv_error("%s: Filename has to be set", RCSID);
    }
    if (fname_in == NULL ) {
        err_msg = "make_fname: \"fname_in == NULL\"";
        return (err_msg);
    }

    fname_base = g_strdup(fname_in);
    strtok(fname_base, ".");

    g_snprintf(fname_img, GPIV_MAX_CHARS, "%s.%s", fname_base, itype);
    if (verbose)
        printf("# Image file: %s\n", fname_img);
    
    gpiv_io_make_fname(fname_base, GPIV_EXT_PAR, fname_parameter);
    if (verbose)
        printf("# Data parameter file: %s\n", fname_parameter);

    if (res_stats == TRUE) {
        gpiv_io_make_fname(fname_base, GPIV_EXT_ERR_STAT, fname_out);
    } else {
        gpiv_io_make_fname(fname_base, GPIV_EXT_ERR_PIV, fname_out);
    }
    if (verbose)
	printf("# Output file: %s\n", fname_out);

    g_free (fname_base);
    return (err_msg);
}



int 
main (int argc, 
      char *argv[]
      )
/* ----------------------------------------------------------------------------
 * main routine to calculate erroneous vectors in a PIV data set
 */
{
    gchar *err_msg = NULL;

    FILE *fp_par_dat;
    gchar fname_in_piv[GPIV_MAX_CHARS], fname_out[GPIV_MAX_CHARS],
	fname_parameter[GPIV_MAX_CHARS], fname_img[GPIV_MAX_CHARS];

    FILE *fp;
    GpivImage *image = NULL;

    GpivPivData *in_data = NULL, *out_data = NULL;
    GpivPivPar *piv_par = g_new0 (GpivPivPar, 1);
    GpivValidPar *valid_par = g_new0 (GpivValidPar, 1); 

    GpivBinData *klass = NULL;
    GpivLinRegData *linreg = g_new0 (GpivLinRegData, 1);


    g_message (":: 0");

    gpiv_valid_parameters_set (valid_par, FALSE);
    gpiv_piv_parameters_set (piv_par, FALSE);
    nbins = GPIV_NBINS_DEFAULT;

/*     if ((c = strstr(argv[0], "medtst")) != NULL) { */
    if (strcmp (argv[0], "medtst") == 0) {
	res_stats = FALSE;
	res_stats__set = TRUE;
	valid_par->residu_max = 0;
	valid_par->residu_max__set = TRUE;
    }

    command_args (argc, argv, fname_in_piv, valid_par);
    if (verbose) {
      printf ("# %s\n# Command line options:\n", RCSID);
      gpiv_valid_print_parameters (NULL, valid_par);
    }

    if (use_stdin_stdout == FALSE) {
/*
 * Generating proper filenames
 */
    g_message (":: 1");
        if ((err_msg = 
             make_fname (fname_in_piv, fname_img, fname_parameter, fname_out))
            != NULL) {
	    gpiv_error ("%s: Failure calling make_fname", argv[0]);
	}


/*
 * Prints command line parameters to stdout and data par-file
 */
	if ((fp_par_dat = fopen (fname_parameter, "a")) == NULL) {
	    gpiv_error ("%s: failure opening %s for input",
		    argv[0], fname_parameter);
	}
	fprintf (fp_par_dat, "\n\n # %s\n# Command line options:\n", argv[0]);
	gpiv_valid_print_parameters (fp_par_dat, valid_par);
    g_message (":: 2");


/*
 * Reading parametes from PARFILE (writing to data par-file will be done 
 * after eventually calculating the threshold from the PIV dataset)
 */
        gpiv_scan_parameter (GPIV_VALIDPAR_KEY, PARFILE, valid_par, 
                            verbose);
        if ((err_msg =
             gpiv_scan_resourcefiles (GPIV_VALIDPAR_KEY, valid_par, 
                                     verbose))
            != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);


        if (res_stats == TRUE
            || auto_thresh == TRUE) {
/*
 * We also need image evaluation parameters for piv now
 */
            gpiv_scan_parameter (GPIV_PIVPAR_KEY, PARFILE, piv_par, 
                                verbose);
            if ((err_msg =
                 gpiv_scan_resourcefiles (GPIV_PIVPAR_KEY, piv_par, verbose))
                != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);
            gpiv_piv_print_parameters (fp_par_dat, piv_par);
        }
	fclose (fp_par_dat);


    } else {
/*
 * use stdin, stdout
 */
        gpiv_scan_parameter (GPIV_VALIDPAR_KEY, PARFILE, valid_par, 
                            verbose);
        if ((err_msg =
             gpiv_scan_resourcefiles (GPIV_VALIDPAR_KEY, valid_par, 
                                     verbose))
            != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);

        if (res_stats == TRUE
            || auto_thresh == TRUE) {
            gpiv_scan_parameter (GPIV_PIVPAR_KEY, PARFILE, piv_par, 
                                verbose);
        if ((err_msg =
             gpiv_scan_resourcefiles (GPIV_PIVPAR_KEY, piv_par, 
                                     verbose))
            != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);
        }
   }


    gpiv_valid_check_parameters_read (valid_par, NULL);

    if (res_stats == TRUE
        || auto_thresh == TRUE) {
        gpiv_piv_check_parameters_read (piv_par, NULL);
    }
/*
 * Check parameters on correct values and adjust belonging variables
 */
    if (use_stdin_stdout == TRUE 
        && valid_par->subst_type == GPIV_VALID_SUBSTYPE__COR_PEAK) {
	gpiv_error ("%s: -f \"filename\" has to be used in combination with "
		   "'peak'", argv[0]);
    }

    if (use_stdin_stdout == TRUE && gnuplot == TRUE) {
	gpiv_error ("%s: -f \"filename\" has to be used in combination with "
		   "'gnuplot'", argv[0]);
    }


/*
 * Reading input data
 */
    if (use_stdin_stdout == FALSE) {
        if (valid_par->subst_type == GPIV_VALID_SUBSTYPE__COR_PEAK) {

            if ((image = gpiv_fread_image (fname_img))
                == NULL) gpiv_error ("%s: %s", argv[0], err_msg);
        }

        if ((fp = fopen (fname_in_piv, "rb")) == NULL) {
            gpiv_error ("%s: failing fopen %s", argv[0], fname_in_piv);
        }

        if ((in_data = gpiv_read_pivdata (fp)) == NULL)  {
            gpiv_error ("%s: %s", argv[0], err_msg);
        }
        fclose (fp);

    } else {
        if ((in_data = gpiv_read_pivdata (NULL)) == NULL) {
            gpiv_error ("%s: %s", argv[0], err_msg);
        }
    }


/*
 * estimation of critical residu by means of fitting a straight line
 * through inverse cumulative histogram data. 
 * Fit depends on number and accuracy of bins. As a first approximation, 
 * a relative good fit will be found for 10 data (residus) 
 * per bin, defined by GPIV_NBINS_DEFAULT. 
 * BUGFIX: To be improved by minimizing linreg.sumsqr (klass.nbins).
 *
 * Calculates threshold value if automatic threshold has been set
 */
    if (auto_thresh == TRUE
        || res_stats == TRUE) {
        if ((err_msg = gpiv_valid_residu (in_data, valid_par, TRUE)) != NULL) {
            gpiv_error ("%s: %s", argv[0], err_msg);
        }
        out_data = gpiv_cp_pivdata (in_data);

        if ((klass = gpiv_valid_residu_stats (out_data, nbins, linreg)) 
            == NULL) {
            err_msg = "GPIV_VALID_RESIDU: error from residu_stats";
            g_warning ("%s", err_msg);
            exit (1);
        }

        if (auto_thresh == TRUE) {
            valid_par->residu_max = 
                gpiv_valid_threshold (piv_par, valid_par, linreg);
        }


    } else {
/*
 * Validating PIV data on outliers and substituting
 * or calculating residu histogram
 */
    if ((err_msg = 
         gpiv_valid_errvec (in_data, image, piv_par, valid_par, FALSE))
        != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);
    }
    out_data = gpiv_cp_pivdata (in_data);
/*
 * Adding comment and timestamp to out_data
 * Writing PIV out_data (and valid parameters) to output
 */
    if (res_stats == FALSE) {
        out_data->comment = g_strdup_printf ("# Software: %s\n", RCSID);
        out_data->comment = gpiv_add_datetime_to_comment (out_data->comment);

	if (use_stdin_stdout == FALSE) {
            if ((fp_par_dat = fopen (fname_parameter, "a")) == NULL) {
                gpiv_error ("%s: failure opening %s for input",
                           argv[0], fname_parameter);
            }
            gpiv_valid_print_parameters (fp_par_dat, valid_par);
            fclose (fp_par_dat);


            if ((fp = fopen (fname_out, "wb")) == NULL) {
                gpiv_error ("%s: failing fopen %s", argv[0], fname_out);
            }
            if ((err_msg =
                 gpiv_write_pivdata (fp, out_data, TRUE))
                != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);
            fclose (fp);
	} else {
            if ((err_msg =
                 gpiv_write_pivdata (NULL, out_data, TRUE))
                != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);
	}

/*
 * print median residus as a (CUMULATIVE!!) histogram
 */
    } else {
        gfloat residu_max = 0.0;
        gchar *lcom = NULL;

        if (piv_par->int_size_f__set == FALSE) {
            gpiv_error ("\n%s error: int_size_f not set", GPIV_PIVPAR_KEY);
        }

        klass->comment = g_strdup_printf ("# Software: %s\n", RCSID);
        klass->comment = gpiv_add_datetime_to_comment (out_data->comment);
        klass->comment = g_strconcat (klass->comment, 
                                      " straight line fitting results:", 
                                      NULL);

        lcom = g_strdup_printf(" c0 = %f c1 = %f (= <residu>)", 
                               linreg->c0, linreg->c1);
        klass->comment = g_strconcat (klass->comment, lcom, NULL);
        g_free (lcom);

        lcom = g_strdup_printf(" cov00 = %f cov01 = %f", 
                               linreg->cov00, linreg->cov01);
        klass->comment = g_strconcat (klass->comment, lcom, NULL);
        g_free (lcom);

        lcom = g_strdup_printf(" cov11 = %f sumsq = %f", 
                               linreg->cov11, linreg->sumsq);
        klass->comment = g_strconcat (klass->comment, lcom, NULL);
        g_free (lcom);

        lcom = g_strdup_printf(" residu_max = %f", 
                               residu_max = 
                               gpiv_valid_threshold (piv_par, valid_par, 
                                                     linreg));
        klass->comment = g_strconcat (klass->comment, lcom, NULL);
        g_free (lcom);


	if (use_stdin_stdout == TRUE) {
	    gpiv_print_cumhisto_eqdatbin (NULL, klass, FALSE);
	} else {
            if ((fp = fopen (fname_out, "wb")) == NULL) {
                gpiv_error ("%s: failure fopen %s", argv[0], fname_out);
            }
	    gpiv_print_cumhisto_eqdatbin (fp, klass, FALSE);
	}

/*
 * Plots histogram with gnuplot
 */
	if (gnuplot) {
            gchar title[GPIV_MAX_CHARS];

	    snprintf (title, GPIV_MAX_CHARS, "residu statistics of %s", fname_in_piv);
	    gpiv_cumhisto_eqdatbin_gnuplot (fname_out, title, 
                                            GNUPLOT_DISPLAY_COLOR, 
                                            GNUPLOT_DISPLAY_SIZE,
                                            linreg);
	}
    }


/*
 * Freeing allocated memory
 */
    gpiv_free_pivdata (in_data);

    if (use_stdin_stdout == FALSE
        && valid_par->subst_type == GPIV_VALID_SUBSTYPE__COR_PEAK) {
        gpiv_free_img (image);
    }

    if (res_stats == TRUE
        || auto_thresh == TRUE) {
        gpiv_free_bindata (klass);
    }

    if (itype__set) g_free (itype);
    if (verbose)
	printf ("\n");

    exit (0);
}
