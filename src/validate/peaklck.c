/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/* --------------------------------------------------------------------
                                                                        
   peaklck - test on peak-locking effects in PIV analyses.              
                                                                        
   Copyright (C) 2002, 2003, 2004 Gerber van der Graaf

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  


---------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <glib.h>
#include <gpiv.h>

/* #define PARFILE  "peaklck.par" */	/* Parameter file name */
#define PARFILE  "gpivrc"	/* Parameter file name */

#define HELP "Optains histograms from PIV displacements: \n\
Peaklck tests PIV data on the so-called peak-locking effect  \n\
by printing/displaying statistics of the particle displacements at \n\
sub-pixel level. Uhisto and vhisto output histograms of horizontal \n\
and vertical displacements"

#define USAGE "\
Usage: peaklck | uhisto  | vhisto \n\
               [-n N] [-g] [-h | --help] [-n] [-p | --print] \n\
               [-v | --version] [filename] < stdin > stdout \n\
keys: \n\
-n N:                  override number of bins \n\
-g:                    graphical visualization of with gnuplot \n\
-h | --help:           this on-line help \n\
-p | --print:          print parameters and other info to stdout \n\
-v | --version:        shows version number and exits successfully \n\
filename:              input PIV file. Substitutes stdin and stdout \n\
"

#define RCSID "$Id: peaklck.c,v 2.14 2008-09-25 13:08:35 gerber Exp $"


/*
 * General global parameters
 */
static gboolean print_par = FALSE;
static gboolean use_stdin_stdout = FALSE;
static gint operator = 10;

#define GNUPLOT_DISPLAY_COLOR "DarkRed"
#define GNUPLOT_DISPLAY_SIZE 250

int gnuplot = 0, gnuplot__set = FALSE;



void command_args (int argc, char *argv[], 
                  char fname[GPIV_MAX_CHARS],
                   GpivValidPar *piv_valid_par,
                   gint *nbins,
                   gboolean *nbins__set
                  )
/*-----------------------------------------------------------------------------
 * Command line argument handling
 */
{
    char c;
    int argc_next;


    while (--argc > 0 && (*++argv)[0] == '-') {
	argc_next = 0;
	while ((argc_next == 0) && (c = *++argv[0])) {
	    switch (c) {
	    case 'n':           /* default: nbins=10 */
		*nbins = atoi (*++argv);
		*nbins__set = TRUE;
		argc_next = 1;
		--argc;
		break;

	    case 'g':	        /* graphic output with gnuplot */
	        gnuplot = 1;
	        gnuplot__set = TRUE;
		break;

	    case 'h':
		printf ("%s\n", argv[0]);
		printf ("%s\n", HELP);
		printf ("%s\n", USAGE);
		exit (1);

	    case 'p':
		print_par = TRUE;
		break;

	    case 'v':
		printf ("%s\n", RCSID);
		exit (1);
		break;
/*
 * long option keys
 */
	    case '-':
		if (strcmp ("-help", *argv) == 0) {
                    printf ("\n%s", argv[0]);
                    printf ("\n%s", HELP);
                    printf ("\n%s", USAGE);
                    exit (0);

                } else if (strcmp ("-print", *argv) == 0) {
		    print_par = TRUE;

                } else if (strcmp ("-version", *argv) == 0) {
                    printf ("%s\n", RCSID);
                    exit (0);

                } else {
		    gpiv_error ("%s: unknown option: %s", argv[0], *argv);
		}

		argc_next = 1;
		break;

	    default:
		gpiv_error ("%s: unknown option: %s", argv[0], *argv);
		break;
	    }
	}
    }
    /*
     * Check if filename or stdin /stdout is used
     */
    if (argc == 1) {
        use_stdin_stdout = FALSE;
        strcpy (fname, argv[argc - 1]);
    } else if (argc == 0) {
        use_stdin_stdout = TRUE;
        print_par = FALSE;
    } else {
 	gpiv_error ("%s: unknown argument: %s", argv[0], *argv);
    }

}



static gchar *
make_fname  (char *fname_in, 
            char *fname_out,
            char *fname_parameter
            )
/*-----------------------------------------------------------------------------
 * generates filenames
 */
{
    gchar *err_msg = NULL;
    gchar *fname_base = NULL;

    if (fname_in == NULL ) {
        err_msg = "make_fname: \"fname_in == NULL\"";
        return (err_msg);
    }
 
    /*
     * Stripping filename
     */
    fname_base = g_strdup (fname_in);
    strtok (fname_base, ".");

    /*
     * filename for output
     */
    if (operator == GPIV_U || operator == GPIV_V) {
        gpiv_io_make_fname (fname_base, GPIV_EXT_UVHISTO, fname_out);
    } else {
        gpiv_io_make_fname (fname_base, GPIV_EXT_PLK, fname_out);
    }
    if (print_par) printf ("# Output file: %s\n", fname_out);

    gpiv_io_make_fname (fname_base, GPIV_EXT_PAR, fname_parameter);
    if (print_par) printf ("# Parameter file: %s\n", fname_parameter);

    g_free (fname_base);
    return (err_msg);
}



int main (int argc, char *argv[])
/*-----------------------------------------------------------------------------
 * main routine to tests PIV data on peak-locking effects
 */
{
    gchar *err_msg = NULL, *c = NULL;
    FILE *fp, *fp_par_dat;
    gchar fname_in[GPIV_MAX_CHARS],
	fname_parameter[GPIV_MAX_CHARS], 
        fname_out[GPIV_MAX_CHARS];

    gint nbins = 0;
    gboolean nbins__set = FALSE;

    GpivPivData *in_data = NULL;
    GpivBinData *klass = NULL;

/* BUGFIX: made as pointers! */
    GpivValidPar *valid_par = g_new0 (GpivValidPar, 1);


    if ((c = strstr (argv[0], "peaklck")) != NULL) {
        /*         operator = PEAKLCK; */
    } else if ((c = strstr (argv[0], "uhisto")) != NULL) {
        operator = GPIV_U;
    } else if ((c = strstr (argv[0], "vhisto")) != NULL) {
        operator = GPIV_V;
    } else {
        gpiv_error ("%s: unvalid program name or symlink", argv[0]);
    }

/*
 * Setting parameters
 */
    gpiv_valid_parameters_set (valid_par, FALSE);

    command_args (argc, argv, fname_in, valid_par, &nbins, &nbins__set);
    if (print_par) {
        printf ("# %s\n# Command line options:\n", argv[0]);
        gpiv_valid_print_parameters (NULL, valid_par);
    }

    if (nbins__set == FALSE) nbins = GPIV_NBINS_DEFAULT;

/*
 * Reading parametes from PARFILE and resources
 */
    gpiv_scan_parameter (GPIV_VALIDPAR_KEY, PARFILE, valid_par, 
                         print_par);
    if ((err_msg =
         gpiv_scan_resourcefiles (GPIV_VALIDPAR_KEY, valid_par, 
                                  print_par))
        != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);


    if (use_stdin_stdout == FALSE) {

/*
 * Generating filenames
 */
        if ((err_msg = 
             make_fname (fname_in, fname_out, fname_parameter))
            != NULL) {
            gpiv_error ("%s: %s\n", argv[0], err_msg);
        }

/*
 * Prints command line parameters to stdout and par-file
 */
	if ((fp_par_dat = fopen (fname_parameter, "a")) == NULL) {
	    gpiv_error ("%s: failure fopen %s", 
                        argv[0], fname_parameter);
	}
	fprintf (fp_par_dat, "\n\n# %s\n# Command line options:\n", RCSID);
	gpiv_valid_print_parameters (fp_par_dat, valid_par);
	fclose (fp_par_dat);

/*
 * Writing parametes to data par-file
 */
        gpiv_valid_print_parameters (NULL, valid_par);
    }

    gpiv_valid_check_parameters_read (valid_par, NULL);

/*
 * Reading input data
 */
    if (print_par) g_message ("nbins = %d", nbins);

    if (use_stdin_stdout == FALSE) {
        if ((fp = fopen (fname_in, "rb")) == NULL) {
            gpiv_error ("%s: failure fopen %s", argv[0], fname_in);
        }
        if ((in_data = gpiv_read_pivdata (fp)) == NULL)  {
            gpiv_error ("%s: %s", argv[0], err_msg);
        }
        fclose (fp);

    } else {
        if ((in_data = gpiv_read_pivdata (NULL)) == NULL)  {
            gpiv_error ("%s: %s", argv[0], err_msg);
        }
    }

/*
 * Calling program function and saving results
 */
    if (operator == GPIV_U || operator == GPIV_V) {
        klass = gpiv_post_uvhisto (in_data, nbins, operator);	  
    } else {
        if ((klass = gpiv_valid_peaklck (in_data, nbins)) == NULL) {
            gpiv_error ("%s: gpiv_valid_peaklck__ni failed", argv[0]);
        }
    }

    if (use_stdin_stdout == FALSE) {
        if ((fp = fopen (fname_out, "wb")) == NULL) {
            gpiv_error ("%s: failure fopen %s", argv[0], fname_out);
        }
        gpiv_print_histo (fp, klass, FALSE);
        fclose (fp);
    } else {
        gpiv_print_histo (NULL, klass, FALSE);
    }

/*
 * Graphical output with gnuplot
 */
    if (gnuplot && use_stdin_stdout == FALSE) {
        gchar title[GPIV_MAX_CHARS];

        if (operator == GPIV_U ) {
            snprintf (title, GPIV_MAX_CHARS, "Histogram U-velocities of %s", fname_out);
        } else if (operator == GPIV_V ) {
            snprintf (title, GPIV_MAX_CHARS, "Histogram V-velocities of %s", fname_out);
        } else {
            snprintf (title, GPIV_MAX_CHARS, "Peak locking statistics of %s", fname_out);
        }
        if ((err_msg = 
             gpiv_histo_gnuplot (fname_out, title, GNUPLOT_DISPLAY_COLOR, 
                                 GNUPLOT_DISPLAY_SIZE))
            != NULL) gpiv_error ("%s: %s", argv[0], err_msg);   
    }


/*
 * Freeing allocated memory of matrices
 */
    gpiv_free_pivdata (in_data);
    gpiv_free_bindata (klass);

    exit (0);

}
