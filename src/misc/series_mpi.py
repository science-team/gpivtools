#!/usr/bin/env mpipython


#   gpiv_series - Processes a set of numbered input data

#   Copyright (C) 2008 Gerber van der Graaf

#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2, or (at your option)
#   any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software Foundation,
#   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  


#--------------------------------------------------------------------

#
# This version is MPI enabled for parallel processing
#
import os, re
from Scientific import MPI
communicator = MPI.world.duplicate()

#
#----------- Command line arguments parser
#
from optparse import OptionParser

usage = "%prog [options] \"process\""
parser = OptionParser(usage)
parser.add_option("-a", "--arg_n",
		  action="store_true", dest="arg_n", default=False,
                  help="if the process needs the current number in its \
		  argument list instead of prepending/appending it to the \
		  filebase name, the number will be put before (-f) \
		  \"filename\" in the \"process\" string.")
parser.add_option("-b", "--basename", type='string', dest="basename",
                  help="File basename for reading", metavar="FILE")
parser.add_option("-e", "--ext", type='string', dest="ext", metavar="EXT",
                  help="add an extension after the file basename + number (without leading \".\")")
parser.add_option("-f", "--first", type='int', dest="first_nr", default=0,
                  help="first numbered file (default: 0)", metavar="N")
parser.add_option("-l", "--last", type='int', dest="last_nr", default=0,
                  help="last numbered file(default: 0)", metavar="N")
parser.add_option("-i", "--incr", type='int', dest="incr_nr", default=1,
                  help="increment file number (default: 1)", metavar="N")
parser.add_option("-p", "--print",
		  action="store_true", dest="pri", default=False,
                  help="prints process parameters/variables to stdout")
parser.add_option("--pad", type='int', dest="pad0", default=0,
                  help="padding number with zero's (default: 0)", metavar="N")
parser.add_option("-n", "--none",
		  action="store_true", dest="none", default=False,
		  help="suppresses real execution")
parser.add_option("-x", "--prefix", action="store_true", dest="prefix", default=False,
                  help="prefix numbering to file basename")

(options, args) = parser.parse_args()
if len(args) != 1:
        parser.error("incorrect number of arguments")
else:
	process = args[0]


#
#----------- Function definitions
#
def pri_date(msg = "Time stamp at start of series processing:"):
	"""Prints time stamp.

	Keyword arguments:
	msg -- message to be printed before time stamp
	"""
	if options.pri == True:
		print msg
		os.system('date')
	elif options.none:
		print msg
		os.system('date')



def count_digits(nr):
	"""Counts number of digits from a number
	
	Keyword arguments:
	nr -- number to be questioned
	"""
	count=0
	while nr/10 !=0:
		nr=nr/10
		count=count+1
	return count


def pad0(nr):
	"""Created a string for zero padding
	
	Keyword arguments:
	nr -- number of zeros to be padded
	"""
	pd0=""
	for i in range(0, nr):
		pd0 = str(pd0)+"0"

	return pd0


def compose_name_nr(nr):
	"""Creates proper name from basename and number.

	Keyword arguments:
	nr -- number of filename to be processed
	"""
	if options.pad0 > 0:
		ndig=count_digits(nr)
		null_str=pad0(options.pad0 - ndig)
		nr_str=null_str+str(nr)
	else:
		nr_str=str(nr)
		
	if options.prefix:
		if options.arg_n:
			name=str(options.basename)
		else:
			name=nr_str+str(options.basename)
	else:
		if options.arg_n:
			name=str(options.basename)
		else:
			name=str(options.basename)+nr_str
			
	if str(options.ext) != "None":
		name=str(name)+str(".")+str(options.ext)	

	return(name)


def compose_cmd(name, nr):
	"""Creates proper command.

	Keyword arguments:
	name -- complete filename
	"""
	command=str(process)
	if options.arg_n:
		# Eventually, substitutes "-f" with: "nr -f"
		command_tmp=re.sub("-f", str(nr)+" -f", command)
		if command_tmp != command:
			command = str(command_tmp)+" "+str(name)
		else:
			command = str(command_tmp)+" "+str(nr)+" "+str(name)
	else:
		command=str(command)+" "+str(name)

	return command


def proc_series_par():
	"""Processes a series on identic numbered files in parallel
	environment using mpipython.

	"""
	#
	# Total number of data and number of data per node
	#
	Nt = (options.last_nr+1 - options.first_nr) / options.incr_nr
	Nn = Nt / communicator.size

	#
	# Bail out if number of nodes is larger than number of data
	#
	if communicator.size > Nt:
		print 'nprocs (',communicator.size,\
		      ') larger then number of data (',Nt,")"
		exit()
		

       	for i in range(options.first_nr, options.last_nr+1, options.incr_nr):
		for j in range(0, communicator.size, 1):
			if communicator.rank == j:
				if i >= options.first_nr + j*Nn*options.incr_nr:
					if i < options.first_nr + (j+1)*Nn*options.incr_nr:
						name_nr = compose_name_nr(i)
						command = compose_cmd(name_nr, i)
						if options.pri == True: print "rank=",communicator.rank,command
						elif options.none == True: print "rank=",communicator.rank,command
						if options.none == False: os.system(command)

				#
				# The fraction of data left, if Nn is not an integer,
				# is processed at the highest node
				#
				if j == communicator.size - 1:
					if i >=  options.first_nr + (j+1)*Nn*options.incr_nr:
						name_nr = compose_name_nr(i)
						command = compose_cmd(name_nr, i)
						if options.pri == True: print "rank=",communicator.rank,command
						elif options.none == True: print "rank=",communicator.rank,command
						if options.none == False: os.system(command)


#
#----------- Calling functions
#
#if communicator.rank == 0: 
#	if options.pri == True: pri_date()
#	elif options.none == True: pri_date()

proc_series_par()

#communicator.rank == 0:
#	if options.pri == True: pri_date(msg = "Time stamp at end of series processing:")	    
#	elif options.none == True: pri_date(msg = "Time stamp at end of series processing:")

#
#----------- That's all folks
#
