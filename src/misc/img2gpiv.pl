#!/usr/bin/perl -w

#   img2gpiv - Converts an image frame "image_name", containing a PIV image
#              pair, into a raw data frame "image_name.r" in order to 
#              analyse with gpiv's 'rr' program. Additional header info is
#              saved in "image_name.h".

#   Copyright (C) 2002 Gerber van der Graaf

#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2, or (at your option)
#   any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software Foundation,
#   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#------------------------------------------------------------------

$VERSION = q$Id: img2gpiv.pl,v 1.12 2008-04-28 12:09:30 gerber Exp $;
$HELP = "Converts images into raw data (extension .r) format with belonging
ASCII header or hdf (extension .hdf) format. \n";
$USAGE = "gpiv_img2gpiv [-a|--auto] [-c|--cols ncolumns] [--combine] [-d|--depth] 
[-h|--help] [-hdf] [-i|--inverse] [--suf_a] 
[--suf_b] [--suf_num N] [-n] [--noclean] [-p] [-r|--rows nrows] [-t] [-v|--version] 
file_base_name

keys:
-x|cross:           double image for cross-correlation (default: single image
                    for auto-correlation)
-c|cols ncolumns:   the number of columns in the frame (default 1280)
--combine           combinates two separate image frames to a single one for 
                    cross-correlation. Also look at --suf_a/b and --suf_num
-d|depth depth:     color depth (default 8)
-h|help:            on-line help
-hdf:               converts to (or from) hdf format (extension .hdf)
-i:                 inverse conversion; from raw 8-bit data to image format
--suf_a             with --combine: suffix of first image (default: _a)
--suf_b             with --combine: suffix of second image (default: _b)
--suf_num N         with --combine: subsequent numbered images of which the 
                    first one with number N and the second N+1. The converted 
                    image will be named to file_base_name + N
-n:                 no execute, only print processes
-noclean:           do not remove existing image
-r|rows rows:       the number of rows in the frame of a SINGLE image 
                    (default 1024)
-p:                 prints process to stdout
-t:                 image type or format (default bmp)
-v|version:         version";


#----------------- Default parameters ----------
$opt_x = 0;
$ncols = 1280;
$opt_combine = 0;
##$opt_d = 0;
##$opt_f = 0;
$opt_h = 0;
$opt_hdf = 0;
$opt_i = 0;
$opt_n = 0;
$opt_noclean = 0;
$nrows = 1024;
$opt_p = 0;
##$opt_t = ".bmp";
$opt_v = 0;
$suf_set = 0;
$suf_num = -9999;
$suf_num_next = -9999;

#------------------ Command line arguments handling ----------
use Getopt::Long;
##$result = 
GetOptions("h|help",  "i|inverse", "n|none", "p|print",
'hdf',
"noclean",
"t|type=s" => \$format,
"v|version",
'c|cols=i' => \$ncols,
"combine",
'suf_a=s' => \$suf_a,
'suf_b=s' => \$suf_b,
'suf_num=i' => \$suf_num,
'd|depth=i' => \$depth,
'r|rows=i' => \$nrows,
"x|cross"
);


if ($opt_h) {
  print ("$HELP\n");
  print ("$USAGE\n");
  exit;
}

if ($opt_v) {
  print ("$VERSION\n");
  exit;
}

$file_base_name = shift (@ARGV);


if ($#ARGV != -1) {
  printf ("\nUsage: $USAGE\n");
  exit;
}


#------------------ Default variables ----------
if (!$nrows) {
  $nrows = 1024;
}

if (!$ncols) {
  $ncols = 1280;
}

if (!$depth) {
  $depth = 8;
}

if (!$format) {
  $format = "bmp";
}

if (!$suf_a) {
  $suf_a = "_a";
} else {
  $suf_set = 1;
}

if (!$suf_b) {
  $suf_b = "_b";
} else {
  $suf_set = 1;
}

if ($suf_num != -9999) {
  $suf_num_next = $suf_num + 1;
  if ($opt_p || $opt_n) {
      print ("suf_num=$suf_num suf_num_next=$suf_num_next\n");
  }
}

if ($suf_set == 1 && $suf_num != -9999) {
  print ("error: a suffix is defined as well as a numbered filenames is used.");
  exit;
}

if ($opt_p || $opt_n) {
    print ("c=$ncols r=$nrows i=$opt_i t=$format f=$file_base_name\n");
}

if (!$opt_i) {
    if ($suf_num == -9999) {
        $file_name_header = $file_base_name.".h";
    } else {
        $file_name_header = $file_base_name.$suf_num.".h";
    }

    if ($opt_combine) {$opt_x = 1;}
#------------------ Convert the entire frame to raw data
    if ($opt_combine) {
        if ($opt_noclean) {
	    if ($suf_num == -9999) {
            @args=("convert $file_base_name$suf_a.$format -depth $depth gray:$file_base_name$suf_a.r && \
convert $file_base_name$suf_b.$format -depth $depth gray:$file_base_name$suf_b.r");
	    } else {
            @args=("convert $file_base_name$suf_num.$format -depth $depth gray:$file_base_name$suf_num.r && \
convert $file_base_name$suf_num_next.$format -depth $depth gray:$file_base_name$suf_num_next.r");
	    }
        } else {
	    if ($suf_num == -9999) {
            @args=("convert $file_base_name$suf_a.$format -depth $depth gray:$file_base_name$suf_a.r && rm $file_base_name$suf_a.$format && \
convert $file_base_name$suf_b.$format -depth $depth gray:$file_base_name$suf_b.r && rm $file_base_name$suf_b.$format");
	    } else {
            @args=("convert $file_base_name$suf_num.$format -depth $depth gray:$file_base_name$suf_num.r && rm $file_base_name$suf_num.$format && \
convert $file_base_name$suf_num_next.$format -depth $depth gray:$file_base_name$suf_num_next.r && rm $file_base_name$suf_num_next.$format");
	    }
	}
    } else {
        if ($opt_noclean) {
            @args=("convert $file_base_name.$format -depth $depth gray:$file_base_name.r");
        } else {
            @args=("convert $file_base_name.$format -depth $depth gray:$file_base_name.r && rm $file_base_name.$format");
        }
    }
    if ($opt_p || $opt_n) {printf ("@args \n");}
    if (!$opt_n) {system (@args) == 0 || die "system @args failed: $?";}

    if ($opt_combine) {
        if ($suf_num == -9999) {
            @args=("cat $file_base_name$suf_b.r >> $file_base_name$suf_a.r && mv $file_base_name$suf_a.r $file_base_name.r && rm $file_base_name$suf_b.r");
            if ($opt_p || $opt_n) {printf ("@args \n");}
            if (!$opt_n) {system (@args) == 0 || die "system @args failed: $?";}
        } else {
            @args=("cat $file_base_name$suf_num_next.r >> $file_base_name$suf_num.r && rm $file_base_name$suf_num_next.r");
            if ($opt_p || $opt_n) {printf ("@args \n");}
            if (!$opt_n) {system (@args) == 0 || die "system @args failed: $?";}
	}
    }

#------------------ Add header info
    if (!$opt_n) {
        open (OUTH,">$file_name_header") || die 'can\'t open $file_name_header';
        printf(OUTH "Ncolumns: $ncols\n");
        printf(OUTH "Nrows: %d\n", $nrows);
        printf(OUTH "X_corr: %d\n", $opt_x);
        printf(OUTH "Nbits: %d\n", $depth);
        close (OUTH) || die 'can\'t close $file_name_header';
    } else {
        printf("Ncolumns: $ncols\n");
        printf("Nrows: %d\n", $nrows);
        printf("X_corr: %d\n", $opt_x);
        printf("Nbits: %d\n", $depth);
    }

    if ($opt_hdf) {
        if ($opt_p || $opt_n) {
            @args=("gpiv_piv2hdf -i -p -e $file_base_name");
        } else {
            @args=("gpiv_piv2hdf -i -e $file_base_name");
        }
        if ($opt_p || $opt_n) {printf ("@args \n");}
        if (!$opt_n) {system (@args) == 0 || die "system @args failed: $?";}

        @args=("rm $file_base_name.r $file_base_name.h");
        if ($opt_p || $opt_n) {printf ("@args \n");}
        if (!$opt_n) {system (@args) == 0 || die "system @args failed: $?";}
    }


} else {                      # $opt_i; inverse conversion
    if ($opt_hdf) {
        if ($opt_p || $opt_n) {
            @args=("hdf2piv -p -e $file_base_name");
        } else {
            @args=("hdf2piv -e $file_base_name");
        }
        if ($opt_p || $opt_n) {printf ("@args \n");}
        if (!$opt_n) {system (@args) == 0 || die "system @args failed: $?";}
        if ($opt_x) {
            if ($opt_noclean) {
                $CMD = sprintf("convert -size %dx%d -depth $depth gray:$file_base_name.r $file_base_name.$format", 
                $ncols, $nrows * 2);
            } else {
                $CMD = sprintf("convert -size %dx%d -depth $depth gray:$file_base_name.r $file_base_name.$format && rm $file_base_name.r $file_base_name.hdf", 
                $ncols, $nrows * 2);
            }
        } else {
            if ($opt_noclean) {
                $CMD = sprintf("convert -size %dx%d -depth $depth gray:$file_base_name.r $file_base_name.$format", 
                $ncols, $nrows);
            } else {
                $CMD = sprintf("convert -size %dx%d -depth $depth gray:$file_base_name.r $file_base_name.$format && rm $file_base_name.r $file_base_name.hdf", 
                $ncols, $nrows);
            }
        }
    } else {
        if ($opt_x) {
            if ($opt_noclean) {
                $CMD = sprintf("convert -size %dx%d -depth $depth gray:$file_base_name.r $file_base_name.$format", 
                $ncols, $nrows * 2);
            } else {
                $CMD = sprintf("convert -size %dx%d -depth $depth gray:$file_base_name.r $file_base_name.$format && rm $file_base_name.r", 
                $ncols, $nrows * 2);
            }
        } else {
            if ($opt_noclean) {
                $CMD = sprintf("convert -size %dx%d -depth $depth gray:$file_base_name.r $file_base_name.$format", 
                $ncols, $nrows);
            } else {
                $CMD = sprintf("convert -size %dx%d -depth $depth gray:$file_base_name.r $file_base_name.$format && rm $file_base_name.r", 
                $ncols, $nrows);
            }
        }
    }
    @args=($CMD);
    if ($opt_p || $opt_n) {printf ("@args \n");}
    if (!$opt_n) {system (@args) == 0 || die "system @args failed: $?";}
}


#
# ----------------- That's all folks.
#
