/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*-----------------------------------------------------------------------------

   suta - subtracts time-avaraged velocities (local mean velocities) from
          the PIV estimators

   Copyright (C) 2002, 2003, 2004 Gerber van der Graaf

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

-----------------------------------------------------------------------------*/

#include <stdio.h> 
#include <stdlib.h> 
#include <assert.h> 
#include <math.h>
#include <gpiv.h>

/* #define PARFILE "scale.par"    */  /* Parameter file name */
#define PARFILE "gpivrc"     /* Parameter file name */
#define MAX_PIVSETS 1000     /* Maximum number of PIV data sets */
#define MIN_SAMPLES 20       /* Minimum number of samples used for estimation */
#define USAGE "\
Usage: suta [-h | --help] [-p | --print] [-v | --version] \n\
filename_ta filename \n\
\n\
keys: \n\
-h | --help:           this on-line help \n\
-p | --print:          print parameters to stdout \n\
-v | --version:        version number \n\
filename               File name of PIV data set (without .piv) \n\
filename_ta            File name of time-averaged (local means) PIV data set \n\
                       (full name) \n\
"

#define HELP  "\
suta - subtracts time-avaraged velocities (local mean) from the PIV estimators"

#define RCSID "$Id: suta.c,v 1.6 2008-09-25 13:08:34 gerber Exp $"

gboolean print_par = FALSE;

void 
command_args(int argc, 
             char *argv[], 
             char fname_in[GPIV_MAX_CHARS],
             char fname_mean[GPIV_MAX_CHARS]
             )
/* ----------------------------------------------------------------------------
 * Command line argument handling
 */
{
    char c = '\0';
    int argc_next;

    while (--argc > 0 && (*++argv)[0] == '-') {

/*
 * argc_next is set to 1 if the next cmd line argument has to be searched for; 
 * in case that the command line argument concerns more than one char or cmd 
 * line argument needs a parameter 
 */

        argc_next = 0;
	while (argc_next == 0 && (c = *++argv[0]))
            
            switch (c) {
            case 'v':
                printf("%s\n", RCSID); 
                exit(0);
                break;
            case 'h':
                printf("%s\n", argv[0]); 
                printf("%s\n",HELP);
                printf("%s\n",USAGE);
                exit(0);
                break;
            case 'p':
                print_par = TRUE;
                break;

/*
 * long option keys
 */
	    case '-':
		if (strcmp("-help", *argv) == 0) {
                    printf("\n%s", argv[0]);
                    printf("\n%s", HELP);
                    printf("\n%s", USAGE);
                    exit(0);
                } else if (strcmp("-print", *argv) == 0) {
		    print_par = TRUE;
                } else if (strcmp("-version", *argv) == 0) {
                    printf("%s\n", RCSID);
                    exit(0);
                } else {
		    gpiv_error("%s: unknown option: %s", argv[0], *argv);
		}
		argc_next = 1;
		break;
                
            default:
                gpiv_error(USAGE);
                break;
            }
    }

    if(argc != 2) { 
        gpiv_error("%s: %s", argv[0], USAGE);
    }

    strcpy(fname_mean, argv[0]);
    strcpy(fname_in, argv[1]);
}




void 
make_fname(char *fname, 
           char *fname_in,
           char *fname_out
           )
/* ----------------------------------------------------------------------------
 * define input and output filenames
 */
{     
    gpiv_io_make_fname(fname, GPIV_EXT_PIV, fname_in);
    if (print_par) printf("# Input data file: %s\n",fname_in);

/* 
 * Extension of filename with time-averaged reduced piv data
 */
#define EXT_SUB_TAVG_PIV  ".suta.piv" 
    gpiv_io_make_fname(fname, EXT_SUB_TAVG_PIV, fname_out);
    if (print_par) printf("# Output data file: %s\n",fname_out);

}



int 
main(int argc, 
     char *argv[]
     )
/* ----------------------------------------------------------------------------
 */
{
    FILE *fp = NULL;
    gchar *err_msg = NULL;
    gchar fname[GPIV_MAX_CHARS],
        fname_in[GPIV_MAX_CHARS],
        fname_out[GPIV_MAX_CHARS],
        fname_mean[GPIV_MAX_CHARS];
    guint i, j;
    
    GpivPivData *in_data = NULL, *mean_data = NULL, *out_data = NULL;



    command_args(argc, argv, fname, fname_mean);
    make_fname(fname, fname_in, fname_out);
    if (print_par) {
        g_message("fname_mean = %s", fname_mean);
    }



    if ((fp = fopen (fname_in, "r")) == NULL) {
        gpiv_error ("%s: failing opening %s", argv[0], fname_in);
    }

    if ((in_data = gpiv_read_pivdata (fp)) == NULL) {
        gpiv_error ("%s: failing gpiv_read_pivdata", argv[0], err_msg);
    }
    fclose (fp);



    if ((fp = fopen (fname_mean, "r")) == NULL) {
        gpiv_error ("%s: failing opening %s", argv[0], fname_mean);
    }

    if ((mean_data = gpiv_read_pivdata (fp)) == NULL) {
        gpiv_error ("%s: gpiv_read_pivdata", argv[0], err_msg);
    }
    fclose (fp);


    out_data = gpiv_alloc_pivdata (in_data->nx, in_data->ny);

    for (i = 0; i < in_data->ny; i++) {
        for (j = 0; j < in_data->nx; j++) {
            out_data->point_x[i][j] = in_data->point_x[i][j];
            out_data->point_y[i][j] = in_data->point_y[i][j];
            out_data->peak_no[i][j] = in_data->peak_no[i][j];
            if (mean_data->peak_no[i][j] > 0) {
                out_data->snr[i][j] = in_data->snr[i][j];
                out_data->dx[i][j] = in_data->dx[i][j] - mean_data->dx[i][j];
                out_data->dy[i][j] = in_data->dy[i][j] - mean_data->dy[i][j];
            } else {
                out_data->snr[i][j] = GPIV_SNR_DISABLE;
                out_data->dx[i][j] = 0.0;
                out_data->dy[i][j] = 0.0;
            }
        }
    }
    

    if ((fp = fopen (fname_out, "w")) == NULL) {
        gpiv_error ("%s: failing opening %s", argv[0], fname_out);
    }

    if ((err_msg = gpiv_write_pivdata(fp, out_data, TRUE)) != NULL) {
        gpiv_error ("%s: %s", argv[0], err_msg);
    }
    fclose (fp);
    


    gpiv_free_pivdata (in_data);
    gpiv_free_pivdata (mean_data);
    exit (0);
}



