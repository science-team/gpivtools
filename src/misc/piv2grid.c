/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*---------------------------------------------------------------------------

   piv2h5 - converts PIV data to hdf5 format data

   Copyright (C) 2002, 2003, 2004 Gerber van der Graaf

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

------------------------------------------------------------------------*/

#include <stdio.h> 
#include <stdlib.h> 
#include <gpiv.h> 


/* #define PARFILE "scale.par"    */  /* Parameter file name */
#define PARFILE "gpivrc"     /* Parameter file name */
#define USAGE "\
Usage: piv2grid [-h | --help] [-p | --print] [-v | --version] [-t type] \n\
                filename \n\
\n\
keys: \n\
-h | --help:           this on-line help \n\
-p | --print:          print parameters to stdout \n\
-v | --version:        version number \n\
-t:                    select which data to be stored on grid; dx, dy or snr \n\
                       (default: snr)\n\
"

#define HELP  "\
piv2grid - converts PIV data to grid data for generating contour plots \n\
plotmtv \n\
"

#define RCSID "$Id: piv2grid.c,v 1.9 2008-09-25 13:08:34 gerber Exp $"

enum DataType {
  DT_DX = 0,
  DT_DY = 1,
  DT_SNR = 2
};

gboolean print_par = FALSE;



void 
command_args(int argc, 
             char *argv[], 
             char fname[GPIV_MAX_CHARS],
             enum DataType *datatype
             )
/*-----------------------------------------------------------------------------
 * Command line argument handling
 */
{
    char c = '\0';
    int argc_next;

    while (--argc > 0 && (*++argv)[0] == '-') {

/*
 * argc_next is set to 1 if the next cmd line argument has to be searched for; 
 * in case that the command line argument concerns more than one char or cmd 
 * line argument needs a parameter 
 */
	argc_next = 0;
	while (argc_next == 0 && (c = *++argv[0])) {

            switch (c) {
            case 'v':
                printf("%s\n", RCSID); 
                exit(0);
                break;
            case 'h':
                printf("%s\n", argv[0]); 
                printf("%s\n",HELP);
                printf("%s\n",USAGE);
                exit(0);
                break;
            case 'p':
                print_par = TRUE;
                break;
            case 't':
                if (strcmp("dx", *argv) == 0) {
                    *datatype = DT_DX;
                } else 

                if (strcmp("dy", *argv) == 0) {
                    *datatype = DT_DY;
                } else

                if (strcmp("snr", *argv) == 0) {
                    *datatype = DT_SNR;
                } else {
                    gpiv_error("piv2grid: wrong specification of data type");
                }
                break;

/*
 * long option keys
 */
	    case '-':
		if (strcmp("-help", *argv) == 0) {
                    printf("\n%s", argv[0]);
                    printf("\n%s", HELP);
                    printf("\n%s", USAGE);
                    exit(0);
                } else if (strcmp("-print", *argv) == 0) {
		    print_par = TRUE;
                } else if (strcmp("-version", *argv) == 0) {
                    printf("%s\n", RCSID);
                    exit(0);
                } else {
		    gpiv_error("%s: unknown option: %s", argv[0], *argv);
		}
		argc_next = 1;
		break;

            default:
                fprintf (stderr,USAGE);
                exit(1);
                break;
            }
        }
        
    }
     if(argc != 1) { 
	  gpiv_error("%s: %s", argv[0], USAGE);
     }
     strcpy(fname, argv[0]); 


}


void 
make_fname(char *fname, 
           char *fname_piv, 
           char *fname_out
           )
/*-----------------------------------------------------------------------------
 * generate filenames
 */
{
     gpiv_io_make_fname(fname, GPIV_EXT_PIV, fname_piv);
     if (print_par) printf("# Input data file: %s\n",fname_piv);
     
     gpiv_io_make_fname(fname, ".grid", fname_out);
     if (print_par) printf("# Output file: %s\n",fname_out);
     
}



   



int 
main(int argc, 
     char *argv[]
     )
/* ----------------------------------------------------------------------------
 * main routine to convert PIV data to grid data for generating contour plots
 */
{
    FILE *fp;
    gchar *err_msg = NULL;
    gchar fname[GPIV_MAX_CHARS], 
        fname_out[GPIV_MAX_CHARS], 
        fname_piv[GPIV_MAX_CHARS],
        fname_parameter[GPIV_MAX_CHARS];
    guint i, j;

    GpivPivData *piv_data = NULL;
    GpivScalarData *sc_data = NULL;
    enum DataType datatype = DT_SNR;


    command_args (argc, argv, fname, &datatype);
    make_fname (fname, fname_piv, fname_out);
    gpiv_io_make_fname (fname, GPIV_EXT_PAR, fname_parameter);
    if (print_par) printf ("datatype = %d\n", datatype);
/*
 * Reading PIV data
 */
    if ((fp = fopen (fname_piv, "r")) == NULL) {
        gpiv_error ("%s: failing opening %s", argv[0], fname_piv);
    }

    if ((piv_data = gpiv_read_pivdata(fp)) != NULL) {
        gpiv_error ("%s: %s", argv[0], err_msg);
    }

/*
 * copying PIV data to SCALAR data
 */
    if ((sc_data = gpiv_alloc_scdata (piv_data->nx, piv_data->ny)) == NULL) {
        gpiv_error ("%s: failing gpiv_alloc_scdata", argv[0]);
    }

    for (i = 0; i < sc_data->ny; i++) {
        for (j = 0; j < sc_data->nx; j++) {
            sc_data->point_x[i][j] = piv_data->point_x[i][j];
            sc_data->point_y[i][j] = piv_data->point_y[i][j];
            if (piv_data->peak_no[i][j] != -1) {
                if (datatype == DT_DX) sc_data->scalar[i][j] = piv_data->dx[i][j];
                if (datatype == DT_DY) sc_data->scalar[i][j] = piv_data->dy[i][j];
                if (datatype == DT_SNR) sc_data->scalar[i][j] = piv_data->snr[i][j];
            } else {
                sc_data->scalar[i][j] = -1.0;
            }
            sc_data->flag[i][j] = piv_data->peak_no[i][j];
        }
    }



/*
 * Saving SCALAR data
 */
    if ((fp = fopen (fname_out, "w")) == NULL) {
        gpiv_error ("%s: failing opening %s", argv[0], fname_out);
    }

    if ((err_msg = gpiv_write_sc_mtvgriddata (fp, sc_data, TRUE))
        != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);



    gpiv_free_pivdata (piv_data);
    exit (0);
}






