#!/usr/bin/perl -w

#     process-chain - A complete process chain on a single image in tif/bmp 
#     format using gpiv tools resulting into estimated PIV velocities
#     or its derivatives.
#

#   Copyright (C) 2002 Gerber van der Graaf

#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2, or (at your option)
#   any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software Foundation,
#   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#--------------------------------------------------------------------

$VERSION = q$Id: process-chain.pl,v 1.7 2007-01-29 14:26:54 gerber Exp $;
$HELP = "Processes a pipe of Gpiv-tool commands";
$USAGE = "gpiv_process-chain [-h|-help] [-n|-none] [-af] [-c|-clean] [-fik]
[-p|-print] [proc_*] [-v|-version] filename

keys:
-af string:         append string to file_basename
-c:                 removes raw image data and header
-fik:               use fi-keyline for filtering gpiv parms from README
-h:                 this on-line help
-n:                 suppresses real execution
-p:                 prints process commands to stdout
-pf string          prepend string to file-base name
-proc_*             defines processes to be included in the chain (recimg, 
                    valid, scale, manipiv, flipx, flipy, revert, rot90, rot180
                    and vorty or nstrain or sstrain). Proc_vorty, proc_nstrain
                    and proc_sstrain not in combination with .gpi input file.
-v:                 prints version
filename            input image filename. Format may be: .r, .gpi (hdf), .IMG 
                    (davis), png, gif, tif or bmp
";

#----------------- Command line arguments handling ----------
##$opt_a = 0;
## $opt_c = 0;
$opt_fik = 0;
$opt_h = 0;
$opt_n = 0;
$opt_p = 0;
$opt_v = 0;

$opt_proc_recimg = 0;
$opt_proc_valid = 0;
$opt_proc_scale = 0;
$opt_proc_manipiv = 0;
$opt_proc_flipx = 0;
$opt_proc_flipy = 0;
$opt_proc_revert = 0;
$opt_proc_rot90 = 0;
$opt_proc_rot180 = 0;
$opt_proc_vorty = 0;
$opt_proc_nstrain = 0;
$opt_proc_sstrain = 0;

#$post_spatial_scale = 0.0; # [mm/px]
#$post_time_scale = .0;        # [s]
#$post_zero_offx = 0.0;       # [mm]
#$post_zero_offy = 0.0;          # [mm]

use Getopt::Long;
##$result = 
GetOptions('h|help', 'n|none', 'p|print', 'v|version',
'c|clean',
'af=s'  => \$append_fname,
't=s'  => \$img_format,
'pf=s'  => \$prepend_fname,
'fik',
'proc_recimg',
'proc_valid',
'proc_scale',
'proc_manipiv',
'proc_flipx',
'proc_flipy',
'proc_revert',
'proc_rot90',
'proc_rot180',
'proc_vorty',
'proc_nstrain',
'proc_sstrain'
);


if ($opt_h) {
  print ("$HELP\n");
  print ("$USAGE\n");
  exit;
}
if ($opt_v) {
  print ("$VERSION\n");
  exit;
}

#printf ("argc = $#ARGV\n");
if ($#ARGV == 0) {
# ------------- splitting $filename up
    $use_stdin_stdout = 0;
    $filename = shift (@ARGV);
} elsif ($#ARGV == -1) {
#BUGFIX: TODO:    $use_stdin_stdout = 1;
#    if ($opt_p || $opt_n) {print ("using STDIN/STDOUT\n");}
    printf ("\nUsage: $USAGE\n");
    exit;

} else {
    printf ("\nUsage: $USAGE\n");
    exit;
}
#    exit;


sub FileExt {
#--------------- extracts file extension from a full filename
 my ($file_base_name, $file_extension);
 @file_fields = split(/\./,$_[0]);
 $file_extension = pop @file_fields;
 $file_base_name = join(".", @file_fields);
 return  $file_extension;
}


sub FileBase {
#--------------- extracts file basename from a full filename
 my ($file_base_name, $file_extension);
 @file_fields = split(/\./,$_[0]);
 pop @file_fields;
 $file_base_name = join(".", @file_fields);
 return $file_base_name;
}



#---------------------------------------------------- Intializing variables
$file_basename = FileBase($filename);
$img_format = FileExt($filename);
if ($opt_p || $opt_n) {
       print("file_basename = $file_basename img_format = $img_format\n");
}

if ($opt_proc_vorty && $opt_proc_nstrain 
   || $opt_proc_vorty && $opt_proc_sstrain 
   || $opt_proc_sstrain && $opt_proc_nstrain) {
    print ("Only one of the processes proc_vorty, proc_sstrain or proc_nstrain may be enabled\n");
    exit;
}

if (($img_format && $img_format =~ "gpi") && $opt_proc_vorty
   || ($img_format && $img_format =~ "gpi") && $opt_proc_sstrain 
   || ($img_format && $img_format =~ "gpi") && $opt_proc_nstrain) {
    print ("proc_vorty, proc_sstrain or proc_nstrain may no be enabled in combination with -t hdf\n");
    exit;
}

# ----------------- Default pars and Initial variables
if ($use_stdin_stdout == 0) {
    if ($append_fname) {
        $out_basefile = $file_basename.$append_fname;
    }

    if ($prepend_fname) {
        @words = split(/\//, $file_basename);      #splitting line $_ up in words
    #printf("nwords = $#words\n");
        for ($i=0; $i <= $#words; $i = $i+1) {
    #printf("words[$i] = $words[$i]\n");
        }
        $words[$#words] = $prepend_fname.$words[$#words];
    #printf("words[$#words] = $words[$#words]\n");
        $out_basefile = join("/", @words);
    #$prepend_fname.$file_basename; 
    #printf("out_basefile = $out_basefile\n");
    }

    if ($append_fname || $prepend_fname) {
            $parfile = $out_basefile.".par";
        if ($opt_proc_vorty) {
            $out_file = $out_basefile.".vor";
        } elsif ($opt_proc_nstrain) {
            $out_file = $out_basefile.".nstr";
        } elsif ($opt_proc_sstrain) {
            $out_file = $out_basefile.".sstr";
        } else {
            $out_file = $out_basefile.".piv";
        }
    } else {
            $parfile = $file_basename.".par";
        if ($opt_proc_vorty) {
            $out_file = $file_basename.".vor";
        } elsif ($opt_proc_nstrain) {
        $out_file = $file_basename.".nstr";
        } elsif ($opt_proc_sstrain) {
            $out_file = $file_basename.".sstr";
        } else {
            $out_file = $file_basename.".piv";
        }
    }





# ----------------- gpivtools image conversion program names
    $fi_keyline = "gpiv_fi-keyline";

# ----------------- Filter parameters from README
    if ($opt_fik) {
        @words = split(/\//, $file_basename);      #splitting line $_ up in words
        $keyword = $words[$#words - 1];
        $keyword = $keyword.">>";
        @args=("$fi_keyline -if README -of gpivrc \"$keyword\"");
        if ($opt_p || $opt_n) {printf ("@args \n");}
        if (!$opt_n) {system (@args) == 0 || die "system @args failed: $?";}
    }
}
# ----------------- Creating chain and execute
# $out_file
$CMD = "";
if ($opt_proc_recimg) {
    $CMD = "gpiv_recimg ";
    $CMD = $CMD."| gpiv_rr ";
} else {
    if ($use_stdin_stdout == 1) {
        $CMD = "gpiv_rr < <STDIN> "; #STDIN
    } else {
        $CMD = "gpiv_rr < $filename ";
    }
}

if ($opt_proc_valid) {$CMD = $CMD."| gpiv_errvec ";}
if ($opt_proc_scale) {$CMD = $CMD."| gpiv_scale ";}
if ($opt_proc_manipiv) {$CMD = $CMD."| gpiv_manipiv ";}
if ($opt_proc_flipx) {$CMD = $CMD."| gpivflipx ";}
if ($opt_proc_flipy) {$CMD = $CMD."| gpiv_flipy ";}
if ($opt_proc_revert) {$CMD = $CMD."| gpiv_revert ";}
if ($opt_proc_rot90) {$CMD = $CMD."| gpiv_rot90 ";}
if ($opt_proc_rot180) {$CMD = $CMD."| gpiv_rot180 ";}
if ($opt_proc_vorty) {$CMD = $CMD."| gpiv_vorty ";}
if ($opt_proc_nstrain) {$CMD = $CMD."| gpiv_nstrain ";}
if ($opt_proc_sstrain) {$CMD = $CMD."| gpiv_sstrain ";}

if ($use_stdin_stdout == 1) {
    $CMD = $CMD." > <STDOUT>"; #STDOUT
} else {
    $CMD = $CMD." > $out_file";
}
#@args=("$eval  < $file_basename.png  | $valid | $post1 | $post2 > $out_file");
@args=($CMD);

if ($opt_p || $opt_n) {printf ("@args \n");}
if (!$opt_n) {system (@args) == 0 || die "system @args failed: $?";}


# ----------------- Save the pararameters
#  @args=("cp gpivrc $parfile");
#  if ($opt_p || $opt_n) {printf ("@args \n");}
#  if (!$opt_n) {system (@args) == 0 || die "system @args failed: $?";}

if ($img_format && $img_format =~ "gpi") {
        @args=("piv2hdf -i $file_basename.piv");
    if ($opt_p || $opt_n) {printf ("@args \n");}
    if (!$opt_n) {system (@args) == 0 || die "system @args failed: $?";}
}


#------------------------------------------------------------------------------
#
# that's all folks!
#
