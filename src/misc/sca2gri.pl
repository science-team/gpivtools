#!/usr/bin/perl -w

#   sca2grid - Converts scalar data from gpiv to grid data for contour 
#              plotting purposes with gnuplot, plotmtv, ..
#
#   Copyright (C) 2002 Gerber van der Graaf

#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2, or (at your option)
#   any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software Foundation,
#   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#------------------------------------------------------------------

$VERSION = q$Id: sca2gri.pl,v 1.5 2006/03/04 12:37:08 gerber Exp $;
$PRG = "sca2gri";
$HELP = "Converts scalar data from gpiv to grid data for contour plotting 
purposes with gnuplot and plotmtv";
$USAGE = "Usage: gpiv_sca2gri [-f|filename fn] [-h] [-p] [-v] < input > output

keys:
-f fn:   complete filename
-h:      on-line help
-p:      prints process parameters/variables to stdout
-v:      prints version and exits sucessfully
";


#----------------- Command arguments handling ----------
$opt_h = 0;
$opt_v = 0;

#use lib "/home/gerber/lib";
#use FileBaseExt;
#use Getopt::Std;
#getopts('f:hpv');
use Getopt::Long;
GetOptions('h|help', 'p|print', 'v|version',
'f|filename=s' => \$full_filename
);



#$full_filename = $opt_f;
#if ($opt_f) {
#  $filename_logic = 1;
#}

if ($opt_h) {
  print ("$HELP\n");
  print ("$USAGE\n");
  exit;
}

if ($opt_v) {
  print ("$VERSION\n");
  exit;
}


if ($#ARGV != -1) {
  print ("Usage: $USAGE\n");
  exit;
}
#--------------- initializing variables and functions
$x_old = 0;


sub FileExt {
#--------------- extracts file extension from a full filename
 my ($file_base_name, $file_extension);
 @file_fields = split(/\./,$_[0]);
 $file_extension = pop @file_fields;
 $file_base_name = join(".", @file_fields);
 return  $file_extension;
}


sub FileBase {
#--------------- extracts file basename from a full filename
 my ($file_base_name, $file_extension);
 @file_fields = split(/\./,$_[0]);
 pop @file_fields;
 $file_base_name = join(".", @file_fields);
 return $file_base_name;
}



#---------------------------------------------------- Intializing variables
$file_base_name = FileBase($full_filename);
$file_extension = FileExt($full_filename);
if ($opt_p) {
  printf("$PRG: file_base_name=%s\n", $file_base_name);
  printf("$PRG: file_extension=%s\n", $file_extension);
}

$file_name_sca = $full_filename;
$file_name_gri = $file_base_name.".gri.".$file_extension;
$infile = $file_name_sca;
$outfile = $file_name_gri;
if ($opt_p == 1) {  
  printf ("Scalar data file: %s\n", $file_name_sca);
  printf ("Grid data file: %s\n", $file_name_gri);
}
#exit;

open (STDIN,"$infile") || die 'can\'t open $infile';
open (STDOUT,">$outfile") || die 'can\'t open $outfile';


#----------------------------------------- Starts reading the lines with data
while (<STDIN>) {
  chomp;
  if ($_ ne "" &&  (!/^(\#|;)/)) { #skip blank lines or ; sign at first col
    s/,/./g;              #substitutes eventually komma's by dot 

    s/^ *//;              #removes eventually leading spaces
      @words = split(/\s+/,$_);      #splitting line $_ up in words
                                     #same as @words = split


    $x_pos = $words[0];
#	$y_pos[$k] = $words[1];
#	$dx[$k] = $words[2];
#	$dy[$k] = $words[3];
#	$snr[$k] = $words[4];
#	$peak_nr[$k] = $words[5];
    if ($x_pos != $x_old) {
      $x_old = $x_pos;
      printf(STDOUT "\n");
    }
    printf(STDOUT "\n%s",$_);
  }
}

close (STDIN) || die 'can\'t close $infile';
close (STDOUT) || die 'can\'t close $outfile';












