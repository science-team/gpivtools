#!/usr/bin/perl -w
#
#   fi-keyline - Substracts parameters from an arbitrary file and writes them 
#                to gpivrc. Initially used for substarcting parameters from a 
#                README doc, containing description of experiments.
#
#   Copyright (C) 2002 Gerber van der Graaf

#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2, or (at your option)
#   any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software Foundation,
#   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#------------------------------------------------------------------

$VERSION = q$Id: fi-keyline.pl,v 1.4 2006/03/04 12:37:08 gerber Exp $;
$HELP = "Filters each line that starts with a keyword";
$USAGE = "gpiv_fi-keyline [-h|-help] [-p|-print] [-v|-version]
[-if|input_file] [-of|output_file] keyword

keys:
-h: on-line help
-p: prints process parameters/variables to stdout
-v: prints version
-if: input file
-of: output file
";

#[-n|-none]
#-n: suppresses real execution
#-p: prints process parameters/variables to stdout


#----------------- Command line arguments handling ----------
$opt_h = 0;
# $opt_n = 0;
$opt_p = 0;
$opt_v = 0;

use Getopt::Long;
GetOptions('h|help', 
# 'n|none', 
'p|print', 
'v|version',
'if|input_file=s' => \$infile,
'of|output_file=s' => \$outfile,
);

if ($opt_h) {
    print ("$HELP\n");
    print ("$USAGE\n");
    exit;
}
if ($opt_v) {
    print ("$VERSION\n");
    exit;
}

if ($#ARGV != 0) {
    printf ("\nUsage: $USAGE\n");
    exit;
}

$keyword = shift (@ARGV);

#------------------------------------- 
if ($infile) {
    if ($opt_p) {printf("input file = $infile\n");}
    open (STDIN, "$infile") || die 'can\'t open $infile';
}

if ($outfile) {
    if ($opt_p) {printf("output file = $outfile\n");}
    open (STDOUT, ">$outfile") || die 'can\'t open $outfile';
}





while (<STDIN>) {
    chomp;
    if ($_ ne "" && (!/^(\#|;)/)) {  #skip blank lines, # or ; signs at first col

        s/^ *//;                    #removes eventually leading space
        @words = split(/ /, $_);      #splitting line $_ up in words
                                   #same as @words = split
        if ($words[0] =~ $keyword) {
            shift @words;
            if ($outfile) {
                printf(STDOUT "@words\n");
            } else {
                printf("@words\n");
            }
        }

    }
}


if ($infile) {close (STDIN) || die 'can\'t close $infile';}
if ($outfile) {close (STDOUT) || die 'can\'t close $infile';}

#
# Thats all folks
#





