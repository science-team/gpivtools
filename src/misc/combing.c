/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */


/*-----------------------------------------------------------------------------

   combing - combines two images for cross-correlation

   Copyright (C) 2002, 2003, 2004 Gerber van der Graaf

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

-----------------------------------------------------------------------------*/

#include <gpiv.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h> /* get_login() */
#include <time.h>


#define GPIV_EXT_PNG_IMAGE ".png"
#define GPIV_URL "gpiv.sourceforge.net"

/* #define VERSION "0.1" */
#define DEFAULT_INNAME "../test/pngtest.png"

#define PARFILE "gpivrc"     /* Parameter file name */
#define USAGE "\
Usage: gpiv_combing [-h | --help] [-p | --print] [-v | --version] \n\
[-a | --suf_a] [-b | --suf_b] [-d | --dt] [-u | --suf_num] [s | --suf_skip] \n\
[-t | type]  [-w | --warning] file_basename \n\
\n\
keys: \n\
-h | --help:           this on-line help \n\
-p | --print:          print parameters to stdout \n\
-v | --version:        version number \n\
-V | --verbose:        program behaves verbose during operation \n\
-a | --suf_a S:        use a suffix of the first image (default: _a) \n\
-b | --suf_b S:        use a suffix of the second image (default: _b) \n\
-d | --dt S:           separation time delta t between subsequent \n\
                       recordings [in milliseconds] \n\
-u | --suf_num N:      use numbered images, instead of suf_a/suf_b, of which  \n\
                       the first one with number N and the second N+1. \n\
                       The converted image will be named to file_base_name + N \n\
-s | --skip S:         skip S numbers; the first image with number N will \n\
                       combine with the second image N+S+1 (default: S = 0) \n\
-t | type:             input image type (tif, gif, bmp, pgm, r, gpi). Default: png. \n \
                       Output image will be in png format. \n\
-w | --warning:        Warns if an input image already contains two frames. \n\
file_basename:         filename without extension or suffix \n\
"

#define HELP  "\
gpiv_combing - combines two images for cross-correlation"

#define RCSID "$Id: combing.c,v 1.5 2008-09-25 13:08:34 gerber Exp $"


/* static const char *fname = "/home/gerber/src/GBTOOLS/gbtools-0.1.0/test/png/img1"; */
/* static const gchar *program_name = "gpiv_combing"; */
static gboolean verbose = FALSE;
static gboolean print_par = FALSE;

static gint num = 0;
static gint skip = 0;
static gchar *suf_a =  "_a";
static gchar *suf_b =  "_b";
static gchar *itype =  "png";
static gboolean warning = FALSE;


static gboolean num__set = FALSE;
static gboolean skip__set = FALSE;
static gboolean suf_a__set = FALSE;
static gboolean suf_b__set = FALSE;
static gboolean itype__set = FALSE;
static gboolean warning__set = FALSE;



static void 
command_args (int argc, 
              char *argv[], 
              char fname[GPIV_MAX_CHARS],
              GpivImagePar *image_par
              )
/* ----------------------------------------------------------------------------
 * Command line argument handling
 */
{
    char c = '\0';
    gint argc_next;

    while (--argc > 0 && (*++argv)[0] == '-') {

/*
 * argc_next is set to 1 if the next cmd line argument has to be searched for; 
 * in case that the command line argument concerns more than one char or cmd 
 * line argument needs a parameter 
 */

        argc_next = 0;
	while (argc_next == 0 && (c = *++argv[0]))
            
            switch (c) {
            case 'v':
                printf("%s\n", RCSID); 
                exit(0);
                break;

            case 'V':
                verbose = TRUE;
                break;

            case 'h':
                printf("%s\n", argv[0]); 
                printf("%s\n",HELP);
                printf("%s\n",USAGE);
                exit(0);
                break;

            case 'p':
                print_par = TRUE;
                break;

            case 'a':
                suf_a = g_strdup(*++argv);
                argc_next = 1;
                --argc;
                suf_a__set = TRUE;
                break;

            case 'b':
                suf_b = g_strdup(*++argv);
                argc_next = 1;
                --argc;
                suf_b__set = TRUE;
                break;

/*
 * Time scaling parameter
 */
            case 'd':                 
                image_par->t_scale = atof(*++argv);
                image_par->t_scale__set = TRUE;
                argc_next = 1;	
                --argc;
                break;

            case 's':
                skip = atoi(*++argv);
                argc_next = 1;	
                --argc;
                skip__set = TRUE;
                break;

            case 'u':
                num = atoi(*++argv);
                argc_next = 1;	
                --argc;
                num__set = TRUE;
                break;

            case 't':
                itype = g_strdup(*++argv);
                argc_next = 1;
                --argc;
                itype__set = TRUE;
                break;

            case 'w':
                warning = TRUE;
                warning__set = TRUE;
                break;

/*
 * long option keys
 */
	    case '-':
		if (strcmp("-help", *argv) == 0) {
                    printf("\n%s", argv[0]);
                    printf("\n%s", HELP);
                    printf("\n%s", USAGE);
                    exit(0);
                } else if (strcmp("-print", *argv) == 0) {
		    print_par = TRUE;
                } else if (strcmp("-version", *argv) == 0) {
                    printf("%s\n", RCSID);
                    exit(0);
                } else if (strcmp("-verbose", *argv) == 0) {
		    verbose = TRUE;
                } else if (strcmp("-dt", *argv) == 0) {
		    image_par->t_scale = atof(*++argv);
		    image_par->t_scale__set = TRUE;
		    argc_next = 1;	
		    --argc;
                } else if (strcmp("-suf_a", *argv) == 0) {
                    suf_a = g_strdup(*++argv);
                    argc_next = 1;
                    --argc;
                    suf_a__set = TRUE;
                } else if (strcmp("-suf_b", *argv) == 0) {
                    suf_b = g_strdup(*++argv);
                    argc_next = 1;
                    --argc;
                    suf_b__set = TRUE;
               } else if (strcmp("-skip", *argv) == 0) {
                    skip = atoi(*++argv);
		    argc_next = 1;	
		    --argc;
                    skip__set = TRUE;
                } else if (strcmp("-num", *argv) == 0) {
                    num = atoi(*++argv);
		    argc_next = 1;	
		    --argc;
                    num__set = TRUE;
                } else if (strcmp("-type", *argv) == 0) {
                    itype = g_strdup(*++argv);
                    argc_next = 1;
		    --argc;
                    itype__set = TRUE;
                } else if (strcmp("-warning", *argv) == 0) {
                    warning = TRUE;
                    warning__set = TRUE;

                } else {
		    gpiv_error ("%s: unknown option: %s", argv[0], *argv);
		}
		argc_next = 1;
		break;

            default:
                gpiv_error (USAGE);
                break;
            }
    }


    if (argc == 1) {
        strcpy(fname, argv[argc - 1]);
    } else {
        gpiv_error ("\n%s", USAGE);
    }

    if (num__set && (suf_a__set || suf_b__set)) {
        gpiv_error ("\nA suffix has been defined as well as numbered filenames.");
    }


    if (skip__set == TRUE
        && image_par->t_scale__set == TRUE) {
        image_par->t_scale *= (skip + 1);
    }

    if (verbose) {
        if (suf_a__set) g_message("comand_args: suf_a = %s", suf_a);
        if (suf_b__set) g_message("comand_args: suf_b = %s", suf_b);
        if (num__set) g_message("comand_args: num = %d", num);
        if (skip__set) g_message("comand_args: skip = %d", skip);
        if (image_par->t_scale__set) g_message("comand_args: t_scale = %f", 
                                                image_par->t_scale);
        g_message("comand_args: fname = %s", fname);
    }

}



static int 
make_fname (char *fname,
            char *fname_in1, 
            char *fname_in2, 
            char *fname_out
            )
/*-----------------------------------------------------------------------------
 * generates filenames
 */
{
    gchar *dirname = g_strdup (g_path_get_dirname(fname));
    gchar *fname_base = g_strdup (g_path_get_basename(fname));
    strtok (fname_base, ".");

/*
 * Input image #1, #2 filename
 */
    if (fname != NULL ) {
        if (num__set) {
            if (fname_in1 != NULL ) {
                g_snprintf(fname_in1, GPIV_MAX_CHARS, "%s%d.%s", 
                           fname_base, num, itype);
                if (verbose) printf("\n#input image #1 is: %s", fname_in1);
            }
            
            if (fname_in2 != NULL ) {
                g_snprintf(fname_in2, GPIV_MAX_CHARS, "%s%d.%s", 
                           fname_base, num + 1 + skip, itype);
                if (verbose) printf("\n#input image #2 is: %s", fname_in2);
            }
        } else {
            if (fname_in1 != NULL ) {
                g_snprintf(fname_in1, GPIV_MAX_CHARS, "%s%s.%s", 
                           fname_base, suf_a, itype);
                if (verbose) printf("\n#input image #1 is: %s", fname_in1);
            }
            
            if (fname_in2 != NULL ) {
                g_snprintf(fname_in2, GPIV_MAX_CHARS, "%s%s.%s", 
                           fname_base, suf_b, itype);
                if (verbose) printf("\n#input image #2 is: %s", fname_in2);
            }
        }
/*
 * Output image filename
 */
        if (fname_out != NULL ) {
/* BUGFIX: removed "comb_" in output filename */
            if (num__set) {
                g_snprintf(fname_out, GPIV_MAX_CHARS, "%s%s%s%d%s", 
                           dirname, G_DIR_SEPARATOR_S, fname_base, num, 
                           GPIV_EXT_PNG_IMAGE);
                if (verbose) printf("\n#output image file is: %s", fname_out);
            } else {
                g_snprintf(fname_out, GPIV_MAX_CHARS, "%s%s%s%s", 
                           dirname, G_DIR_SEPARATOR_S, fname_base, 
                           GPIV_EXT_PNG_IMAGE);
                if (verbose) printf("\n#output image file is: %s", 
                                      fname_out);
            }
        }
        
        if (verbose) printf("\n");
    }
    return 0;
}



int
main (int argc, char *argv[])
/*-----------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    FILE *fp;
    gchar fname_base[GPIV_MAX_CHARS], 
        fname_in_a[GPIV_MAX_CHARS], 
        fname_in_b[GPIV_MAX_CHARS], 
        fname_out[GPIV_MAX_CHARS];
    
    GpivImage *image_a = NULL;         /* Image containing first frame */
    GpivImage *image_b = NULL;         /* Image containing second frame */
    GpivImage *image_c = g_new0 (GpivImage, 1);         /* Combination of image_a and image_b */

    GpivImagePar *image_par = g_new0 (GpivImagePar, 1);
    GpivImagePar *image_par_c = NULL;
    gint return_val = 0;

/*
 * Construct file names from output PNG files for writing or
 * read / write to stdin and stdout
 */
    command_args (argc, argv, fname_base, image_par);

    if ((return_val = 
         make_fname (fname_base, fname_in_a, fname_in_b, fname_out))
        != 0) {
        gpiv_error ("%s: Failure make_fname", argv[0]);
    }
    
/*
 * reads input images and checks if they are of equal dimensions
 */
    if ((image_a = gpiv_fread_image (fname_in_a)) == NULL) {
        gpiv_error ("%s: %s\n", argv[0], err_msg);
    }
    
    if ((image_b = gpiv_fread_image (fname_in_b)) == NULL) {
        gpiv_error ("%s: %s\n", argv[0], err_msg);
    }

    if (warning
        && image_a->header->x_corr == TRUE) {
        gpiv_error ("%s: the first input image contains two frames", argv[0]);
    }

    if (warning
        && image_b->header->x_corr == TRUE) {
        gpiv_error ("%s: the second input image contains two frames", argv[0]);
    } 

    if (image_a->header->nrows != image_b->header->nrows) {
        gpiv_error ("%s: input images differ in nrows;  %d <=> %d", argv[0],
                    image_a->header->nrows, image_b->header->nrows);
    }

    if (image_a->header->ncolumns != image_b->header->ncolumns) {
        gpiv_error ("%s: input images differ in ncolumns;  %d <=> %d", argv[0],
                    image_a->header->ncolumns, image_b->header->ncolumns);
    }

    if (image_a->header->depth != image_b->header->depth) {
        gpiv_error ("%s: input images differ in depth;  %d <=> %d", argv[0],
                    image_b->header->depth);
    }

/*
 * Header info from image_par and image_a->header will be used for the new image
 * Adding some extra info to image_c header
 */
    image_par_c = gpiv_img_cp_parameters (image_a->header);
    image_par_c->t_scale = image_par->t_scale;
    image_par_c->t_scale__set = TRUE;
    image_par_c->x_corr = TRUE;
    image_par_c->x_corr__set = TRUE;

    if (image_par_c->software__set == FALSE) {
        g_snprintf (image_par_c->software, GPIV_MAX_CHARS, "%s", argv[0]);
        image_par_c->software__set = TRUE;
    }

    if (image_par_c->author__set == FALSE) {
        g_snprintf (image_par_c->author, GPIV_MAX_CHARS,"%s", g_get_real_name());
        image_par_c->author__set = TRUE;
    }

    if (image_par_c->creation_date__set == FALSE) {
        char time_string[GPIV_MAX_CHARS];
        struct tm time_struct;
        time_t itime;
        time (&itime);
        time_struct = *gmtime (&itime);
        strftime (time_string, GPIV_MAX_CHARS, "%a %b %d %Y %T", &time_struct);
        if (verbose)
            g_message ("Setting new time = %s", time_string);
        snprintf (image_par_c->creation_date, GPIV_MAX_CHARS, "%s", time_string);
        image_par_c->creation_date__set = TRUE;
    }

/* 
 * Reading the rest of (optional) image parameters. 
 */
    gpiv_scan_parameter (GPIV_IMGPAR_KEY, PARFILE, image_par_c, print_par);
    gpiv_scan_resourcefiles (GPIV_IMGPAR_KEY, image_par_c, print_par);

/*
 * Creating image_c from image_a, image_b and image_par_c
 */
    image_c->header = image_par_c;    
    image_c->frame1 = image_a->frame1;    
    image_c->frame2 = image_b->frame1;    
    if (print_par) gpiv_img_print_header (stdout, image_c->header);

/*
 * Writing image
 */
    if ((fp = fopen(fname_out, "wb")) == NULL) {
        return (1);
    }
    if ((err_msg = gpiv_write_png_image (fp, image_c, FALSE)) != NULL) {
        gpiv_error ("%s: %s\n", argv[0], err_msg);
    }
    fclose (fp);
        


    if (image_a != NULL) gpiv_free_img(image_a);
    if (image_b != NULL) gpiv_free_img(image_b);
    if (suf_a__set) g_free (suf_a);
    if (suf_b__set) g_free (suf_b);
    if (itype__set) g_free (itype);
    exit (0);
}
