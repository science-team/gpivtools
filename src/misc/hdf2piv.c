/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*-----------------------------------------------------------------------------

   hdf2piv - converts hdf5 PIV data ASCII data

   Copyright (C) 2002, 2003, 2004 Gerber van der Graaf

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

-----------------------------------------------------------------------------*/

#include <stdio.h> 
#include <stdlib.h> 
#include <assert.h> 
#include <gpiv.h>

/* #define PARFILE "scale.par"    */  /* Parameter file name */
#define PARFILE "gpivrc"     /* Parameter file name */
#define USAGE "\
Usage: hdf2piv [-e] [-h | --help] [-p | --print] [-v | --version] \n\
[-V | --verbose] filename \n\
\n\
keys: \n\
-e:                    exclude data \n\
-h | --help:           this on-line help \n\
-p | --print:          print parameters to stdout \n\
-v | --version:        version number \n\
-V | --verbose:        version number \n\
filename:              input file, hdf formatted (including .hdf extension) \n\
"

#define HELP  "\
hdf2piv - converts hdf5 PIV-data to ASCII data"

#define RCSID "$Id: hdf2piv.c,v 1.13 2008-09-25 13:08:34 gerber Exp $"

gboolean print_par = FALSE, verbose = FALSE, exclude_data = FALSE;


void 
command_args(gint argc, 
             gchar *argv[], 
             gchar fname[GPIV_MAX_CHARS]
             )
/* ----------------------------------------------------------------------------
 * Command line argument handling
 */
{
    gchar c = '\0';
    gint argc_next;

    while (--argc > 0 && (*++argv)[0] == '-') {

/*
 * argc_next is set to 1 if the next cmd line argument has to be searched for; 
 * in case that the command line argument concerns more than one char or cmd 
 * line argument needs a parameter 
 */

        argc_next = 0;
	while (argc_next == 0 && (c = *++argv[0]))
            
            switch (c) {
            case 'v':
                printf("%s\n", RCSID); 
                exit(0);
                break;
            case 'V':
                verbose = TRUE;
                break;
            case 'e':
                exclude_data = TRUE;
                break;
            case 'h':
                printf("%s\n", argv[0]); 
                printf("%s\n",HELP);
                printf("%s\n",USAGE);
                exit(0);
                break;
            case 'p':
                print_par = TRUE;
                break;

/*
 * long option keys
 */
	    case '-':
		if (strcmp("-help", *argv) == 0) {
                    printf("\n%s", argv[0]);
                    printf("\n%s", HELP);
                    printf("\n%s", USAGE);
                    exit(0);
                } else if (strcmp("-print", *argv) == 0) {
		    print_par = TRUE;
                } else if (strcmp("-version", *argv) == 0) {
                    printf("%s\n", RCSID);
                    exit(0);
                } else if (strcmp("-verbose", *argv) == 0) {
                    verbose = TRUE;
                } else {
		    gpiv_error("%s: unknown option: %s", argv[0], *argv);
		}
		argc_next = 1;
		break;

            default:
                gpiv_error(USAGE);
                break;
            }
    }

    if (argc == 1) {
        strcpy (fname, argv[argc - 1]);
    } else {
        gpiv_error ("%s: %s", argv[0], USAGE);
    }

}



gchar *
make_fname(gchar *fname_in, 
           gchar *fname_out_img,
           gchar *fname_out_piv,
           gchar *fname_out_vor, 
           gchar *fname_out_nstrain, 
           gchar *fname_out_sstrain,
           gchar *fname_out_par
           )
/* ----------------------------------------------------------------------------
 * define filenames
 */
{     
    gchar *err_msg = NULL;
    gchar *fname_base = NULL;

    if (fname_in == NULL ) {
        err_msg = "make_fname: \"fname_in == NULL\"";
        return (err_msg);
    }
 
    /*
     * Stripping filename
     */
    fname_base = g_strdup (fname_in);
    strtok (fname_base, ".");

    /*
     * filenames for output PIV data
     */
     gpiv_io_make_fname (fname_base, GPIV_EXT_RAW_IMAGE, fname_out_img);
     if (verbose) printf("# Output data file: %s\n", fname_out_img);
     
     if (exclude_data == FALSE) {
         gpiv_io_make_fname(fname_base, GPIV_EXT_PIV, fname_out_piv);
         if (verbose) printf("# Output data file: %s\n",fname_out_piv);
         
         gpiv_io_make_fname(fname_base, GPIV_EXT_VOR, fname_out_vor);
         if (verbose) printf("# Input data file: %s\n",fname_out_vor);
         
         gpiv_io_make_fname(fname_base, GPIV_EXT_NSTR, fname_out_nstrain);
         if (verbose) printf("# Input data file: %s\n",
                                    fname_out_nstrain);
         
         gpiv_io_make_fname(fname_base, GPIV_EXT_SSTR, fname_out_sstrain);
         if (verbose) printf("# Input data file: %s\n",
                                    fname_out_sstrain);

         gpiv_io_make_fname(fname_base, GPIV_EXT_PAR, fname_out_par);
         if (verbose) printf("# Input parameter file: %s\n",
                                    fname_out_par);
     }
    g_free (fname_base);
    return (err_msg);
}



int 
main(int argc, 
     char *argv[]
     )
/* ----------------------------------------------------------------------------
 */
{
    FILE *fp_out = NULL;
    gchar *err_msg = NULL;
    gchar fname_in[GPIV_MAX_CHARS], 
        fname_out_img[GPIV_MAX_CHARS], 
        fname_out_piv[GPIV_MAX_CHARS], 
        fname_out_vor[GPIV_MAX_CHARS],
        fname_out_nstrain[GPIV_MAX_CHARS],
        fname_out_sstrain[GPIV_MAX_CHARS],
        fname_out_par[GPIV_MAX_CHARS];

    GpivImage *image = NULL;
    GpivPivData *piv_data = NULL;
    GpivScalarData *sc_data;

    GpivPivPar *piv_par = NULL;
    GpivValidPar *valid_par = NULL;
    GpivPostPar *post_par = NULL;

/*
 * Initializing parameters
 */
    command_args (argc, argv, fname_in);
    if ((err_msg = 
         make_fname (fname_in, fname_out_img, fname_out_piv, fname_out_vor, 
                     fname_out_nstrain, fname_out_sstrain, 
                     fname_out_par))
        != NULL) {
        gpiv_error ("%s: %s\n", argv[0], err_msg);
    }
	  
/* 
 * reading input from hdf5
 *
 * image data
 */
    if ((image = gpiv_fread_hdf5_image (fname_in)) == NULL) {
        gpiv_error ("%s: %s", argv[0], err_msg);
    }

    if ((fp_out = fopen (fname_out_img, "wb")) == NULL) {
        gpiv_error("%s error: failure opening %s for output",
                   argv[0], fname_out_img);
    }
    gpiv_write_png_image (fp_out, image, TRUE);
    fclose(fp_out);

/*
 * piv data
 */ 
    if (exclude_data == FALSE) {
        if ((piv_data = gpiv_fread_hdf5_pivdata (fname_in, "PIV" )) == NULL) {
            gpiv_error ("%s: failing gpiv_fread_hdf5_pivdata", argv[0]);
        }

        if ((fp_out = fopen (fname_out_piv, "w")) == NULL) {
            gpiv_error("%s error: failure opening %s for output",
                       argv[0], fname_out_piv);
        }

        if ((err_msg = gpiv_write_pivdata (fp_out, piv_data, TRUE)) 
            != NULL) {
            gpiv_error ("%s: %s", argv[0], err_msg);
        }
        fclose(fp_out);

/*
 * scalar data: vorticity
 */
        if ((sc_data = gpiv_fread_hdf5_scdata (fname_in, "VORTICITY" )) 
            == NULL) {
            gpiv_error ("%s: failing gpiv_fread_hdf5_scdata", argv[0]);
        }

        if ((fp_out = fopen (fname_out_vor, "w")) == NULL) {
            gpiv_error("%s error: failure opening %s for output",
                       argv[0], fname_out_vor);
        }
        
        if ((err_msg = gpiv_write_scdata (fp_out, sc_data, TRUE)) 
            != NULL) {
            gpiv_error ("%s: %s", argv[0], err_msg);
        }
        fclose(fp_out);
         
/*
 * scalar data: normal strain
 */
        if ((sc_data = gpiv_fread_hdf5_scdata (fname_in, "NORMAL STRAIN" )) 
            == NULL) {
            gpiv_error ("%s: failing gpiv_fread_hdf5_scdata", argv[0]);
        }

        if ((fp_out = fopen (fname_out_nstrain, "w")) == NULL) {
            gpiv_error("%s error: failure opening %s for output",
                       argv[0], fname_out_vor);
        }
        
        if ((err_msg = gpiv_write_scdata (fp_out, sc_data, TRUE)) 
            != NULL) {
            gpiv_error ("%s: %s", argv[0], err_msg);
        }
        fclose(fp_out);

/*
 * scalar data: shear strain
 */
        if ((sc_data = gpiv_fread_hdf5_scdata (fname_in, "SHEAR STRAIN" )) 
            == NULL) {
            gpiv_error ("%s: failing gpiv_fread_hdf5_scdata", argv[0]);
        }

        if ((fp_out = fopen (fname_out_sstrain, "w")) == NULL) {
            gpiv_error("%s error: failure opening %s for output",
                       argv[0], fname_out_vor);
        }
        
        if ((err_msg = gpiv_write_scdata (fp_out, sc_data, TRUE)) 
            != NULL) {
            gpiv_error ("%s: %s", argv[0], err_msg);
        }
        fclose(fp_out);

/* 
 * parameters
 */
         if ((piv_par =
              gpiv_piv_fread_hdf5_parameters (fname_in))
             == NULL)  {
             gpiv_error ("%s: failing gpiv_piv_fread_hdf5_parameters", argv[0]);
         }

         if ((valid_par =
              gpiv_valid_fread_hdf5_parameters (fname_in))
             == NULL)  {
             gpiv_error ("%s: failing gpiv_valid_fread_hdf5_parameters", argv[0]);
         }

         if ((post_par =
              gpiv_post_fread_hdf5_parameters(fname_in))
             == NULL) {
             gpiv_error ("%s: failing gpiv_post_fread_hdf5_parameters", argv[0]);
         }


         if ((fp_out = fopen (fname_out_par, "w")) == NULL) {
             gpiv_error("%s error: failure opening %s for output",
                        argv[0], fname_out_par);
         }
         gpiv_img_print_parameters (fp_out, image->header);
         gpiv_piv_print_parameters (fp_out, piv_par);
         gpiv_valid_print_parameters (fp_out, valid_par);
         gpiv_post_print_parameters (fp_out, post_par);
         fclose (fp_out);
 
         if (print_par) {
             gpiv_img_print_parameters (NULL, image->header);
             gpiv_piv_print_parameters (NULL, piv_par);
             gpiv_valid_print_parameters (NULL, valid_par);
             gpiv_post_print_parameters (NULL, post_par);
         }
     }


     exit (0);
}






