/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*---------------------------------------------------------------------------

   piv2gnuplt - converts PIV data gnuplot data

   Copyright (C) 2002, 2003, 2004 Gerber van der Graaf

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

------------------------------------------------------------------------*/

#include <stdio.h> 
#include <stdlib.h> 
#include <gpiv.h>

/* #define PARFILE "scale.par"    */  /* Parameter file name */
#define PARFILE "gpivrc"     /* Parameter file name */
#define USAGE "\
Usage: piv2gnuplt [-al L] [-au UNIT] [-h | --help] [-p | --print] [-s SCALE] \n\
                  [-v] filename \n\
\n\
keys: \n\
-al L:                 draws an annotation vector with lenght L \n\
-au UNIT:              define unit of annotation vector (defaul m/s) \n\
-h | --help:           this on-line help \n\
-p | --print:          print parameters to stdout \n\
-s SCALE:              scale factor for graphic output with gnuplot \n\
-v | --version:        version number \n\
-z                     full zoom (for scaled data) \n\
filename:              inpit PIV data file. \n\
"

#define HELP  "\
piv2gnuplt - converts PIV data gnuplot data"

#define RCSID "$Id: piv2gnuplt.c,v 1.12 2008-09-25 13:08:34 gerber Exp $"
#define GNUPLOT_DISPLAY_SIZE 500
#define GNUPLOT_DISPLAY_COLOR "LightBlue"

gboolean print_par = FALSE, zoom = FALSE;
gboolean scale__set = FALSE, annot__set = FALSE;
float scale = 1.0, annot_length = 1.0;
char annot_unit[GPIV_MAX_CHARS] = "m/s";

/*
TODO:
Annotation; length, unit
Full Zoom
*/

void 
command_args(int argc, 
             char *argv[], 
             char fname[GPIV_MAX_CHARS]
             )
/*-----------------------------------------------------------------------------
 * Command line argument handling
 */
{
    char c = '\0';
    int argc_next = 0;


    while (--argc > 0 && (*++argv)[0] == '-') {
        argc_next = 0;
/*
 * argc_next is set to 1 if the next cmd line argument has to be searched for; 
 * in case that the command line argument concerns more than one char or cmd 
 * line argument needs a parameter 
 */

	while (argc_next == 0 && (c = *++argv[0]))
            switch (c) {
            case 'a':
		if (strcmp(*argv, "al") != 0) {
                    annot_length = atof(*++argv);
                } else if (strcmp(*argv, "au") != 0) {
		    strcpy(annot_unit, *++argv);
/* 		    annot__set = TRUE; */
		    argc_next = 1;
		    --argc;
                } else {
                    gpiv_error("%s: %s", argv[0], USAGE);
                }
                annot__set = TRUE;
                break;
            case 'v':
                printf("%s\n", RCSID); 
                exit(0);
                break;
             case 'h':
                printf("%s\n", argv[0]); 
                printf("%s\n",HELP);
                printf("%s\n",USAGE);
                exit(0);
                break;
            case 'p':
                print_par = TRUE;
                break;
            case 's':	       
                scale = atof(*++argv);
                scale__set = TRUE;
                --argc;
                argc_next = 1;
                break;
            case 'z':
                zoom = TRUE;
                break;

/*
 * long option keys
 */
	    case '-':
		if (strcmp("-help", *argv) == 0) {
                    printf("\n%s", argv[0]);
                    printf("\n%s", HELP);
                    printf("\n%s", USAGE);
                    exit(0);
                } else if (strcmp("-print", *argv) == 0) {
		    print_par = TRUE;
                } else if (strcmp("-version", *argv) == 0) {
                    printf("%s\n", RCSID);
                    exit(0);
                } else {
		    gpiv_error("%s: unknown option: %s", argv[0], *argv);
		}
		argc_next = 1;
		break;

            default:
                fprintf (stderr,USAGE);
                exit(1);
                break;
            }
    }

     if(argc != 1) { 
	  gpiv_error("%s: %s", argv[0], USAGE);
     }
     strcpy(fname, argv[0]); 


}


static gchar *
make_fname (char *fname_in, 
            char *fname_header
            )
/*-----------------------------------------------------------------------------
 * function to generate filenames
 */
{
    gchar *err_msg = NULL;
    gchar *fname_base = NULL;

    if (fname_in == NULL ) {
        err_msg = "make_fname: \"fname_in == NULL\"";
        return (err_msg);
    }
 
    /*
     * Stripping filename
     */
    fname_base = g_strdup(fname_in);
    strtok(fname_base, ".");

    /*
     * filenames for output
     */
    gpiv_io_make_fname(fname_base, GPIV_EXT_HEADER, fname_header);
    if (print_par) printf("# Header data file: %s\n", fname_header);
     
    g_free (fname_base);
    return (err_msg);
}



int 
main(int argc, 
     char *argv[]
     )
/*-----------------------------------------------------------------------------
 * main program to convert PIV data gnuplot data
 */
{
    FILE *fp;
    gchar *err_msg = NULL;
    gchar fname_in[GPIV_MAX_CHARS], 
        fname_header[GPIV_MAX_CHARS];

/*     gchar  c_line[GPIV_MAX_LINES_C][GPIV_MAX_CHARS]; */
/*     gint nc_lines = 0, var_scale = 0; */
    GpivPivData *piv_data = NULL;
    GpivImagePar *image_par = g_new0 (GpivImagePar, 1);;
    GpivPivPar *piv_par = g_new0 (GpivPivPar, 1);
    

/* 
 *Initializing parameters
 * General
 */
    gpiv_img_parameters_set (image_par, FALSE);
    gpiv_piv_parameters_set (piv_par, FALSE);


    command_args (argc, argv, fname_in);
    make_fname (fname_in, fname_header);

/*
 * Reads image header data from file.h and/or resource files 
 * if not overridden by the commandline options
 */
    gpiv_scan_parameter ("", fname_header, image_par, print_par);
    gpiv_scan_parameter ("", PARFILE, image_par, print_par);
    if ((err_msg =
         gpiv_scan_resourcefiles(GPIV_IMGPAR_KEY, image_par, print_par))
        != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);    

/*
 * Reads piv parameters from PARFILE and/or resource files 
 * if not overridden by the commandline options
 */
    gpiv_scan_parameter (GPIV_PIVPAR_KEY, PARFILE, piv_par, print_par);
    if ((err_msg =
         gpiv_scan_resourcefiles (GPIV_PIVPAR_KEY, piv_par, print_par))
        != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);
/*
 * Check if all parameters have been read
 */
/*     gpiv_img_check_header_read(GPIV_IMAGE_PAR_KEY); */
    if ((err_msg =
         gpiv_piv_check_parameters_read (piv_par, NULL))
        != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);
/*
 * As piv_data.nx and piv_data.ny are not known, the input data file will 
 * first be read
 */
    if ((fp = fopen (fname_in, "r")) == NULL) {
        gpiv_error ("%s: failing opening %s", argv[0], fname_in);
    }

    if ((piv_data = gpiv_read_pivdata (fp)) == NULL) {
        gpiv_error ("%s: failing gpiv_fread_pivdata", argv[0]);
    }
    fclose (fp);
/* 
 * plotting with gnuplot
 */
    gpiv_piv_gnuplot (strtok (g_strdup (fname_in), "."), scale, 
                       GNUPLOT_DISPLAY_COLOR, GNUPLOT_DISPLAY_SIZE,
                       image_par, piv_par, piv_data);

/*
 * Freeing allocated memory of matrices
 */
     gpiv_free_pivdata (piv_data);
     exit (0);
}






