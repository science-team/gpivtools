/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*---------------------------------------------------------------------------

   piv2h5 - converts PIV data to hdf5 format data

   Copyright (C) 2002, 2003, 2004 Gerber van der Graaf

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

------------------------------------------------------------------------*/

#include <stdio.h> 
#include <stdlib.h> 
#include <time.h>
#include <gpiv.h>

/* #define PARFILE "scale.par"    */  /* Parameter file name */
#define PARFILE "gpivrc"     /* Parameter file name */
#define USAGE "\
Usage: piv2hdf [-e] [-h | --help] [-i] [-p | --print] [-v | --version] \n\
               [-V | --verbose] filename \n\
\n\
keys: \n\
-e:                    exclude data \n\
-h | --help:           this on-line help \n\
-i:                    includes image data \n\
-p | --print:          print parameters to stdout \n\
-v | --version:        version number \n\
-V | --verbose:        program behaves more verbose \n\
filename:              input PIV datafile, including .piv extension \n\
"

#define HELP  "\
piv2hdf - converts ASCII PIV-data and derived scalars to hdf5 formatted data"

#define RCSID "$Id: piv2hdf.c,v 1.12 2008-09-25 13:08:34 gerber Exp $"

gboolean print_par = FALSE,  verbose = FALSE, exclude_data = FALSE, 
    include_image = FALSE;

void 
command_args(gint argc, 
             gchar *argv[], 
             gchar fname[GPIV_MAX_CHARS]
             )
/*-----------------------------------------------------------------------------
 * Command line argument handling
 */
{
    gchar c = '\0';
    gint argc_next;


    while (--argc > 0 && (*++argv)[0] == '-') {

/*
 * argc_next is set to 1 if the next cmd line argument has to be searched for; 
 * in case that the command line argument concerns more than one char or cmd 
 * line argument needs a parameter 
 */
        argc_next = 0;
	while (argc_next == 0 && (c = *++argv[0]))

            switch (c) {
            case 'v':
                printf("%s\n", RCSID); 
                exit(0);
                break;
            case 'V':
                verbose = TRUE;
                break;
            case 'e':
                exclude_data = TRUE;
                break;
            case 'h':
                printf("%s\n", argv[0]); 
                printf("%s\n",HELP);
                printf("%s\n",USAGE);
                exit(0);
                break;
            case 'i':
                include_image = TRUE;
                break;
            case 'p':
                print_par = TRUE;
                break;

/*
 * long option keys
 */
	    case '-':
		if (strcmp("-help", *argv) == 0) {
                    printf("\n%s", argv[0]);
                    printf("\n%s", HELP);
                    printf("\n%s", USAGE);
                    exit(0);
                } else if (strcmp("-print", *argv) == 0) {
		    print_par = TRUE;
                } else if (strcmp("-version", *argv) == 0) {
                    printf("%s\n", RCSID);
                    exit(0);
                } else if (strcmp("-verbose", *argv) == 0) {
                    verbose = TRUE;
                } else {
		    gpiv_error("%s: unknown option: %s", argv[0], *argv);
		}
		argc_next = 1;
		break;

            default:
                gpiv_error(USAGE);
                break;
            }
     }

     if(argc != 1) { 
	  gpiv_error("%s: %s", argv[0], USAGE);
     }
     strcpy(fname, argv[0]); 


}


gchar *
make_fname(gchar *fname_in, 
           gchar *fname_img, 
           gchar *fname_vor, 
           gchar *fname_nstrain, 
           gchar *fname_sstrain, 
           gchar *fname_out,
           gchar *fname_parameter
           )
/*-----------------------------------------------------------------------------
 * generates filenames
 */
{
    gchar *err_msg = NULL;
    gchar *fname_base = NULL;

    if (fname_in == NULL ) {
        err_msg = "make_fname: \"fname_in == NULL\"";
        return (err_msg);
    }
 
    /*
     * Stripping filename
     */
    fname_base = g_strdup(fname_in);
    strtok(fname_base, ".");

    /*
     * filenames for output PIV data
     */
    if (include_image) {
        gpiv_io_make_fname(fname_base, GPIV_EXT_PNG_IMAGE, fname_img);
        if (verbose) printf("# Image data file: %s\n", fname_img);
    }

    if (exclude_data == FALSE) {
         
        gpiv_io_make_fname(fname_base, GPIV_EXT_VOR, fname_vor);
        if (verbose) printf("# Input data file: %s\n", fname_vor);
         
        gpiv_io_make_fname(fname_base, GPIV_EXT_NSTR, fname_nstrain);
        if (verbose) printf("# Input data file: %s\n", fname_nstrain);
         
        gpiv_io_make_fname(fname_base, GPIV_EXT_SSTR, fname_sstrain);
        if (verbose) printf("# Input data file: %s\n", fname_sstrain);
         
    }

    gpiv_io_make_fname(fname_base, GPIV_EXT_GPIV, fname_out);
    if (verbose) printf("# Output file: %s\n",fname_out);
     
    gpiv_io_make_fname(fname_base, GPIV_EXT_PAR, fname_parameter);

    g_free (fname_base);
    return (err_msg);
}



   



int 
main(int argc, 
     char *argv[]
     )
/*-----------------------------------------------------------------------------
 * main routine to converts ASCII PIV-data to hdf5 formatted data
 */
{
    gchar *err_msg = NULL;
    FILE *fp = NULL;
    gchar fname_piv[GPIV_MAX_CHARS], 
        fname_out[GPIV_MAX_CHARS], 
        fname_vor[GPIV_MAX_CHARS],
        fname_nstrain[GPIV_MAX_CHARS],
        fname_sstrain[GPIV_MAX_CHARS],
        fname_img[GPIV_MAX_CHARS],
        fname_parameter[GPIV_MAX_CHARS];

/*     time_t itime; */
/*     gchar  c_line[GPIV_MAX_LINES_C][GPIV_MAX_CHARS]; */
/*     gint nc_lines=0, var_scale=0; */

    GpivPivPar *piv_par = g_new0 (GpivPivPar, 1);
    GpivValidPar *valid_par = g_new0 (GpivValidPar, 1);
    GpivPostPar *post_par = g_new0 (GpivPostPar, 1);

    GpivImage *image = NULL;
    GpivPivData *piv_data = NULL;
    GpivScalarData *sc_data = NULL;

    gboolean stored__post_par = FALSE;

/*
 * Initializing parameters
 */
    gpiv_piv_parameters_set(piv_par, FALSE);
    gpiv_valid_parameters_set(valid_par, FALSE);
    gpiv_post_parameters_set(post_par, FALSE);


    command_args(argc, argv, fname_piv);
    if ((err_msg = 
         make_fname(fname_piv,  fname_img, fname_vor, fname_nstrain, 
                    fname_sstrain, fname_out, fname_parameter))
        != NULL) {
        gpiv_error ("%s: %s\n", argv[0], err_msg);
    }
	  
/*
 * Reads piv program parameters from fname_parameter
 */
    gpiv_scan_parameter (GPIV_PIVPAR_KEY, fname_parameter, piv_par, 
                         print_par);
    gpiv_scan_parameter (GPIV_VALIDPAR_KEY, fname_parameter, valid_par, 
                         print_par);
    gpiv_scan_parameter (GPIV_POSTPAR_KEY, fname_parameter, post_par, 
                         print_par);

#undef USE_RESOURCES
#ifdef USE_RESOURCES
    if ((err_msg =
         gpiv_scan_resourcefiles (GPIV_PIVPAR_KEY, piv_par, print_par))
        != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);
    if ((err_msg =
         gpiv_scan_resourcefiles (GPIV_VALIDPAR_KEY, valid_par, print_par))
        != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);
    if ((err_msg =
         gpiv_scan_resourcefiles (GPIV_POSTPAR_KEY, post_par, print_par))
        != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);
#endif /* USE_RESOURCES */

/* 
 * writing to output int hdf5
 */
    if ((err_msg = gpiv_fcreate_hdf5 (fname_out)) != NULL)  {
        gpiv_error ("%s: %s", argv[0], err_msg);
    }


/*
 * Image
 */
    if (include_image) {
        if ((fp = fopen(fname_img, "r")) != NULL) {
            if ((image = gpiv_read_png_image (fp)) == NULL) {
                fclose(fp);
                gpiv_error ("%s: faling gpiv_read_png_image", argv[0]);
            }
            fclose(fp);
            fp = NULL;

            if ((err_msg = gpiv_fwrite_hdf5_image (fname_out, image, TRUE)) 
                != NULL) {
                gpiv_error ("%s: %s", argv[0], err_msg);
            }
        }
    }


    if (exclude_data == FALSE) {
/*
 * Piv data, PIV and validation parameters
 */
         if ((fp = fopen (fname_piv, "r")) != NULL) {
             if ((piv_data = gpiv_read_pivdata (fp)) == NULL) {
                fclose(fp);
                gpiv_error ("%s: faling gpiv_read_pivdata", argv[0]);
            }
            fclose(fp);
            fp = NULL;

            if ((err_msg = gpiv_fwrite_hdf5_pivdata (fname_out, piv_data,  "PIV", 
                                                     TRUE))
                != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);
            


             if (piv_par != NULL) {
                 if ((err_msg = 
                      gpiv_piv_fwrite_hdf5_parameters (fname_out, piv_par))
                     != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);
             }

             if (valid_par != NULL) {
                 if ((err_msg = 
                      gpiv_valid_fwrite_hdf5_parameters (fname_out, valid_par))
                     != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);
             }

         }

/*
 * Scalar data: vorticity
 */
         if ((fp = fopen (fname_vor, "r")) != NULL) {
             if ((sc_data = gpiv_read_scdata (fp)) == NULL) {
                 fclose(fp);
                 gpiv_error ("%s: faling gpiv_read_scdata", argv[0]);
             }
             fclose(fp);
             fp = NULL;

             if ((err_msg = gpiv_fwrite_hdf5_scdata (fname_out, sc_data, 
                                                     "VORTICITY", TRUE))
                 != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);


             if (post_par != NULL && stored__post_par == FALSE) {
                 stored__post_par = TRUE;
                 if ((err_msg = 
                      gpiv_post_fwrite_hdf5_parameters (fname_out, post_par))
                     != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);
             }
         }

/*
 * Scalar data: sstrain
 */
         if ((fp = fopen (fname_sstrain, "r")) != NULL) {

             if ((sc_data = gpiv_read_scdata (fp)) == NULL) {
                 fclose(fp);
                 gpiv_error ("%s: faling gpiv_read_scdata", argv[0]);
             }
             fclose(fp);
             fp = NULL;

             if ((err_msg =
                  gpiv_fwrite_hdf5_scdata(fname_out, sc_data, 
                                          "SHEAR_STRAIN", TRUE))
                != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);

             if (post_par != NULL && stored__post_par == FALSE) {
                 stored__post_par = TRUE;
                 if ((err_msg =
                      gpiv_post_fwrite_hdf5_parameters (fname_out, post_par))
                     != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);
             }
         }
         
/*
 * Scalar data: normal strain
 */
         if((fp=fopen(fname_nstrain, "r")) != NULL) {
             if ((sc_data = gpiv_read_scdata (fp)) == NULL) {
                 fclose(fp);
                 gpiv_error ("%s: faling gpiv_read_scdata", argv[0]);
             }
             fclose(fp);
             fp = NULL;


             if ((err_msg =
                  gpiv_fwrite_hdf5_scdata(fname_out, sc_data, 
                                     "NORMAL_STRAIN", TRUE))
                 != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);

             if (post_par != NULL && stored__post_par == FALSE) {
                 stored__post_par = TRUE;
                 if ((err_msg =
                      gpiv_post_fwrite_hdf5_parameters (fname_out, post_par))
                     != NULL)  gpiv_error ("%s: %s", argv[0], err_msg);
             }
         }
     }


    exit(0);
}
