/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*-----------------------------------------------------------------------------

   t-corr - calculates the velocity correlation as function of time 
            (Eulerian correlation) from a series PIV data sets

   Copyright (C) 2002, 2003, 2004 Gerber van der Graaf

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

-----------------------------------------------------------------------------*/

#include <stdio.h> 
#include <stdlib.h> 
#include <assert.h> 
#include <math.h>
#include <gpiv.h>

/* #define PARFILE "scale.par"    */  /* Parameter file name */
#define PARFILE "gpivrc"     /* Parameter file name */
#define MAX_PIVSETS 1000     /* Maximum number of PIV data sets */
#define MIN_SAMPLES 20       /* Minimum number of samples used for estimation */
#define USAGE "\
Usage: t-corr [-h | --help] [-p | --print] [-v | --version] \n\
[-f | --file_first N]  [-l | --file_last N] [-x | --point_x F] \n\
[-y | --point_y F] file_basename\n\
\n\
keys: \n\
-h | --help:           this on-line help \n\
-p | --print:          print parameters to stdout \n\
-v | --version:        version number \n\
-f | --file_first N    First file number of the series (default: 0) \n\
-l | --file_last N     Last file number of the series (default: 0) \n\
-x | --point_x F       x-position of data set to be analysed (default: 0.0) \n\
-y | --point_y F       y-position of data set to be analysed (default: 0.0) \n\
file_basename          File basename of a numbered series of PIV data sets \n\
"

#define HELP  "\
t-corr - calculates the Eulerian correlation from a series of PIV data sets"

#define RCSID "$Id: t-corr.c,v 1.6 2008-09-25 13:08:34 gerber Exp $"

gboolean print_par = FALSE;
typedef struct _TimeCorrPar TimeCorrPar;
struct _TimeCorrPar {
    gint first_file;
    gint last_file;
    gboolean first_file__set;
    gboolean last_file__set;

    gfloat point_x;
    gfloat point_y;
    gboolean  point_x__set;
    gboolean  point_y__set;
};


void 
command_args(int argc, 
             char *argv[], 
             char fname[GPIV_MAX_CHARS],
             TimeCorrPar *time_corr_par
             )
/* ----------------------------------------------------------------------------
 * Command line argument handling
 */
{
    char c = '\0';
    int argc_next;

    while (--argc > 0 && (*++argv)[0] == '-') {

/*
 * argc_next is set to 1 if the next cmd line argument has to be searched for; 
 * in case that the command line argument concerns more than one char or cmd 
 * line argument needs a parameter 
 */

        argc_next = 0;
	while (argc_next == 0 && (c = *++argv[0]))
            
            switch (c) {
            case 'v':
                printf("%s\n", RCSID); 
                exit(0);
                break;
            case 'h':
                printf("%s\n", argv[0]); 
                printf("%s\n",HELP);
                printf("%s\n",USAGE);
                exit(0);
                break;
            case 'p':
                print_par = TRUE;
                break;
/*
 * First file number N of the series (default: 0)
*/
	    case 'f':
                time_corr_par->first_file = atoi(*++argv);
                time_corr_par->first_file__set = TRUE;
		argc_next = 1;
		--argc;
                break;
/*
 * Last file number N of the series (default: 0)
*/
	    case 'l':
                time_corr_par->last_file = atoi(*++argv);
                time_corr_par->last_file__set = TRUE;
		argc_next = 1;
		--argc;
                break;
/*
 * X-position (default: 0.0)
*/
	    case 'x':
                time_corr_par->point_x = atof(*++argv);
                time_corr_par->point_x__set = TRUE;
		argc_next = 1;
		--argc;
                break;
/*
 * Y-position (default: 0.0)
*/
	    case 'y':
                time_corr_par->point_y = atof(*++argv);
                time_corr_par->point_y__set = TRUE;
		argc_next = 1;
		--argc;
                break;
/*
 * long option keys
 */
	    case '-':
		if (strcmp("-help", *argv) == 0) {
                    printf("\n%s", argv[0]);
                    printf("\n%s", HELP);
                    printf("\n%s", USAGE);
                    exit(0);
                } else if (strcmp("-print", *argv) == 0) {
		    print_par = TRUE;
                } else if (strcmp("-version", *argv) == 0) {
                    printf("%s\n", RCSID);
                    exit(0);
                } else if (strcmp("-file_first", *argv) == 0) {
                    time_corr_par->first_file = atoi(*++argv);
                    time_corr_par->first_file__set = TRUE;
                    argc_next = 1;
                    --argc;
                } else if (strcmp("-file_last", *argv) == 0) {
                    time_corr_par->last_file = atoi(*++argv);
                    time_corr_par->last_file__set = TRUE;
                    argc_next = 1;
                    --argc;
                } else if (strcmp("-point_x", *argv) == 0) {
                    time_corr_par->point_x = atof(*++argv);
                    time_corr_par->point_x__set = TRUE;
                    argc_next = 1;
                    --argc;
                } else if (strcmp("-point_y", *argv) == 0) {
                    time_corr_par->point_y = atof(*++argv);
                    time_corr_par->point_y__set = TRUE;
                    argc_next = 1;
                    --argc;
                } else {
		    gpiv_error("%s: unknown option: %s", argv[0], *argv);
		}
		argc_next = 1;
		break;

            default:
                gpiv_error(USAGE);
                break;
            }
    }

    if(argc != 1) { 
        gpiv_error("%s: %s", argv[0], USAGE);
    }

    strcpy(fname, argv[0]);
}



void 
make_fname_out(char *fname, 
               char *fname_out
               )
/* ----------------------------------------------------------------------------
 * define filenames
 */
{     
    gpiv_io_make_fname(fname, ".tco", fname_out);
    if (print_par) printf("# Output data file: %s\n",fname_out);
    
}



void 
make_fname_in(char *fname, 
              int number,
              char *fname_in 
              )
/* ----------------------------------------------------------------------------
 * define numbered input filenames
 */
{     
#define GPIV_EXT_SCALED_PIV  ".sc.piv"		/* Extension of scaled piv file name (ASCII format) */
    snprintf(fname_in, GPIV_MAX_CHARS, "%s%d%s", fname, number, GPIV_EXT_SCALED_PIV);
    if (print_par) printf("# Input file: %s\n",fname_in);
}



static void
time_corr_par__set (TimeCorrPar *time_corr_par, 
                    gboolean flag
                    )
/* ----------------------------------------------------------------------------
 */
{
    time_corr_par->first_file__set = flag;
    time_corr_par->last_file__set = flag;
    time_corr_par->point_x__set = flag;
    time_corr_par->point_y__set = flag;
}



int 
main(int argc, 
     char *argv[]
     )
/* ----------------------------------------------------------------------------
 */
{
    FILE *fp = NULL;
    gchar *err_msg = NULL;
    gchar fname[GPIV_MAX_CHARS], 
        fname_in[GPIV_MAX_CHARS],
        fname_out[GPIV_MAX_CHARS];
    guint i, j, k, l, ndata_sets = 0;
    

    gfloat *dx = NULL, *dy = NULL;
    gint *flag = NULL;

    gfloat mean_dx = 0.0, mean_dy = 0.0, sdev_dx = 0.0, sdev_dy = 0.0;
    gint count = 0;
    gfloat dx_sum = 0.0, dy_sum = 0.0, dx_sum_q = 0.0, dy_sum_q = 0.0;
    
    gfloat *corr_dx, *corr_dy;
    gfloat *dx_sum_cov, *dy_sum_cov;
    gint *count_cov;

    GpivPivData *in_data = NULL;
    TimeCorrPar *time_corr_par = g_new0 (TimeCorrPar, 1);;


/*
 * Default parameter values
 */
    time_corr_par__set (time_corr_par, FALSE);

    command_args (argc, argv, fname, time_corr_par);
    make_fname_out (fname, fname_out);

    if (!time_corr_par->first_file__set) time_corr_par->first_file = 0;
    if (!time_corr_par->last_file__set) time_corr_par->last_file = 0;

/*
 * Check parameters
 */
    if (time_corr_par->last_file < time_corr_par->first_file
        || (time_corr_par->last_file - time_corr_par->first_file) > MAX_PIVSETS) {
        gpiv_error("%s: last_file smaller than first_file or total number of data sets exceeds MAX_PIVSETS=%d",
                   argv[0], MAX_PIVSETS);
    }


    if (print_par) {
        g_message ("first_file = %d", time_corr_par->first_file);
        g_message ("last_file = %d", time_corr_par->last_file);
        g_message ("point_x = %f", time_corr_par->point_x);
        g_message ("point_y = %f", time_corr_par->point_y);
    }


    ndata_sets = time_corr_par->last_file - time_corr_par->first_file + 1;
    gpiv_warning("first_file=%d last_file=%d ndata_sets=%d", 
                 time_corr_par->first_file, time_corr_par->last_file, ndata_sets);
    flag = gpiv_ivector (ndata_sets);
    dx = gpiv_vector (ndata_sets);
    dy = gpiv_vector (ndata_sets);

/*
 * Obtaining the data at the sepecified point from all sets
 */

    for (i = 0; i < ndata_sets; i++) {
        make_fname_in (fname, i + time_corr_par->first_file, fname_in);
        if (i == 0) {

            if ((fp = fopen (fname_in, "r")) == NULL) {
                gpiv_error ("%s: failing opening %s", argv[0], fname_in);
            }

            if ((in_data = gpiv_read_pivdata (fp)) != NULL)  {
                gpiv_error ("%s: %s", argv[0], err_msg);
            }
            fclose (fp);

/*
 * Filtering specified point
 */

            for (k = 0; k < in_data->ny; k++) {
                for (l = 0; l < in_data->nx; l++) {
                    if (in_data->point_x[k][l] == time_corr_par->point_x
                        && in_data->point_y[k][l] == time_corr_par->point_y
                        ) {
                        flag[i] = in_data->peak_no[k][l];
                        dx[i] = in_data->dx[k][l];
                        dy[i] = in_data->dy[k][l];
                    }
                }
            }

        }

        gpiv_free_pivdata (in_data);
    }

/* t_avg()
 * Calculate mean and variances
 */

    for (i = 0; i < ndata_sets; i++) {
        if (flag[i] != -1) {
            count++;
            dx_sum = dx_sum + dx[i];
            dy_sum = dy_sum + dy[i];
            dx_sum_q = dx_sum_q + dx[i] * dx[i];
            dy_sum_q = dy_sum_q + dy[i] * dy[i];
        }
    }


    if (count != 0) {
        mean_dx = dx_sum / count;
        sdev_dx = sqrt(dx_sum_q / (count - 1) 
                       - dx_sum * dx_sum / (count * (count -1)));
        
        mean_dy = dy_sum / count;
        sdev_dy = sqrt(dy_sum_q / (count - 1) 
                       - dy_sum * dy_sum / (count * (count -1)));
        gpiv_warning("t-corr: mean_dx=%f sdev_dx=%f", mean_dx, sdev_dx);
        gpiv_warning("t-corr: mean_dy=%f sdev_dy=%f", mean_dy, sdev_dy);
    } else {
        gpiv_warning("t-corr: count equal to zero");
    }

/* t_corr()
 * Calculate correlation
 */


    if (count != 0) {
        count_cov = gpiv_ivector(ndata_sets);
        dx_sum_cov = gpiv_vector(ndata_sets);
        dy_sum_cov = gpiv_vector(ndata_sets);
        corr_dx = gpiv_vector(ndata_sets);
        corr_dy = gpiv_vector(ndata_sets);
        
        for (i = 0; i < ndata_sets; i++) {
            for (j = 0; j <= i; j++) {
                if (flag[i] != -1 && flag[j] != -1) {
/*                     g_message("i=%d j=%d cor[%d] =>dx[%d]=%f * dx[%d]=%f",  */
/*                               i, j, */
/*                               i - j, */
/*                               i, dx[i] - mean_dx, */
/*                               j, dx[j] - mean_dx */
/*                               ); */

                    count_cov[i-j]++;
                    dx_sum_cov[i-j] = dx_sum_cov[i-j] 
                        + (dx[i] - mean_dx) * (dx[j] - mean_dx);
                    dy_sum_cov[i-j] = dy_sum_cov[i-j] 
                        + (dy[i] - mean_dy) * (dy[j] - mean_dy);
                }
            }
        }
        
/*     
 * corr_dx[0] = (2 * sdev_dx * sdev_dx)
 * corr_dy[0] = (2 * sdev_dy * sdev_dy)
 */

        printf("# i   corr_dx\n");
        for (i = 0; i < ndata_sets - MIN_SAMPLES; i++) {
            corr_dx[i] = dx_sum_cov[i] / dx_sum_cov[0] 
/*                 * (float) count_cov[i] / (float) ndata_sets */
                ;
            printf("%d  %f\n", i, corr_dx[i]);
        }
        
        printf("\n\n# i   corr_dy\n");
        for (i = 0; i < ndata_sets - MIN_SAMPLES; i++) {
            corr_dy[i] = dy_sum_cov[i] / dy_sum_cov[0]
/*                * (float) count_cov[i] / (float) ndata_sets  */
                ;
            printf("%d  %f\n", i, corr_dy[i]);
        }
        
        gpiv_free_ivector (count_cov);
        gpiv_free_vector  (dx_sum_cov);
        gpiv_free_vector  (dy_sum_cov);
        gpiv_free_vector  (corr_dx);
        gpiv_free_vector  (corr_dy);
    }
    
    gpiv_free_vector (dy);
    gpiv_free_vector (dx);
    gpiv_free_ivector (flag);
    exit (0);
}



