
/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8 c-style: "K&R" -*- */

/**************************************************************************
**       Title: grab one gray image using libdc1394
**    $RCSfile: rec_image.c,v $
**   $Revision: 1.7 $$Name:  $
**       $Date: 2007-11-23 16:18:55 $
**   Copyright: LGPL $Author: gerber $
** Description:
**
**    Get one gray image using libdc1394 and store it as portable gray map
**    (pgm). Based on 'samplegrab' from Chris Urmson 
**
**-------------------------------------------------------------------------
**
**  $Log: rec_image.c,v $
**  Revision 1.7  2007-11-23 16:18:55  gerber
**  release 0.5.0: Kafka
**
**  Revision 1.6  2007-01-29 14:26:54  gerber
**  Added image formats png, gif, tif, pgm, bpm. Added gpiv_aint
**
**  Revision 1.5  2006-09-18 07:36:16  gerber
**  split up of trigger and camera (image recording)
**
**  Revision 1.4  2006/01/31 14:18:03  gerber
**  version 0.3.2
**
**  Revision 1.3  2005/02/26 16:29:19  gerber
**  parameter flags (parameter_logic) defined as gboolean, added scale_type to struct __GpivPostPar for inverse scaling
**
**  Revision 1.2  2005/01/19 15:45:58  gerber
**  all executables start with gpiv_
**
**  Revision 1.1  2005/01/19 10:36:03  gerber
**  Initiation of Data Acquisition; triggering of lasers and camera, recording images
**
**  Revision 1.2  2001/09/14 08:10:41  ronneber
**  - some cosmetic changes
**
**  Revision 1.1  2001/07/24 13:50:59  ronneber
**  - simple test programs to demonstrate the use of libdc1394 (based
**    on 'samplegrab' of Chris Urmson
**
**
**************************************************************************/

#include <stdio.h>
#ifdef ENABLE_CAM
#include <libraw1394/raw1394.h>
#include <libdc1394/dc1394_control.h>
#endif /* ENABLE_CAM */
#include <stdlib.h>
#include <getopt.h>
#include <gpiv.h>
#include "system.h"
#ifdef ENABLE_CAM
#include "rec_image.h"
#endif /* ENABLE_CAM */
#include <unistd.h> /* unsigned int sleep(unsigned int seconds); */


#define EXIT_FAILURE 1
#define RCSID "$Id: rec_image.c,v 1.7 2007-11-23 16:18:55 gerber Exp $"

#ifdef ENABLE_CAM

char *xmalloc ();
char *xrealloc ();
char *xstrdup ();


char *program_name;

/* Option flags and variables */

#define USAGE "\
Usage: gpiv_rec_image [-h | --help] -v | --version] -V | --verbose] [-x | --x_corr] \n\
                      [filename] > stdout \n\
\n\
keys: \n\
-h | --help            this on-line help \n\
-v | --version:        version number \n\
-V | --verbose:        program prints more information \n\
-x | --x_corr          record two succesive images for cross-correlation \n\
filename:              image filename. Overrides stdout. \n\
                       Output will be a .png formatted image \n\
"

#define HELP "\
gpiv_recimg captures images from a IIDC-compliant camera with IEE1394 connection \n\
"

/*
 * Global parameters and variables
 */
gboolean use_stdin_stdout = FALSE;
gboolean verbose = FALSE;		/* --verbose */
gboolean print_par = FALSE;


static void 
command_args (int argc, 
              char *argv[], 
              char fname[GPIV_MAX_CHARS],
              GpivImagePar *image_par
              )
/* ----------------------------------------------------------------------------
 * Command line argument handling
 */
{
        char c = '\0';
        gint argc_next;

        while (--argc > 0 && (*++argv)[0] == '-') {

                /*
                 * argc_next is set to 1 if the next cmd line argument has to be searched for; 
                 * in case that the command line argument concerns more than one char or cmd 
                 * line argument needs a parameter 
                 */

                argc_next = 0;
                while (argc_next == 0 && (c = *++argv[0]))
            
                        switch (c) {
                        case 'v':
                                printf("%s\n", RCSID); 
                                exit(0);
                                break;

                        case 'h':
                                printf("%s\n", RCSID); 
                                printf("%s\n",HELP);
                                printf("%s\n",USAGE);
                                exit(0);
                                break;

                        case 'V':
                                verbose = TRUE;
                                break;

                        case 'x':
                                image_par->x_corr = TRUE;
                                image_par->x_corr__set = TRUE;
                                break;
                                /*
                                 * long option keys
                                 */
                        case '-':
                                if (strcmp("-help", *argv) == 0) {
                                        printf("\n%s", RCSID);
                                        printf("\n%s", HELP);
                                        printf("\n%s", USAGE);
                                        exit(0);
                                } else if (strcmp("-verbose", *argv) == 0) {
                                        verbose = TRUE;
                                } else if (strcmp("-version", *argv) == 0) {
                                        printf("%s\n", RCSID);
                                        exit(0);
                                } else if (strcmp("-x_corr", *argv) == 0) {
                                        image_par->x_corr = TRUE;
                                        image_par->x_corr__set = TRUE;
                                } else {
                                        gpiv_error ("%s: unknown option: %s", RCSID, *argv);
                                }
                                argc_next = 1;
                                break;

                        default:
                                gpiv_error ("%s: unknown option: %s", RCSID, *argv);
                                break;
                        }
        }

        /*
         * Check if filename or stdin /stdout is used
         */
        if (argc == 1) {
                use_stdin_stdout = FALSE;
                strcpy(fname, argv[argc - 1]);
        } else if (argc == 0) {
                use_stdin_stdout = TRUE;
                print_par = FALSE;
        } else {
                gpiv_error("\n%s: unknown argument: %s: %s", RCSID, *argv, USAGE);
        }



}


int 
main(int argc, 
     char *argv[]
     )
/*-----------------------------------------------------------------------------
 */ 
{
        FILE* imagefile;
        dc1394_cameracapture capture;
        quadlet_t value;
        GpivCamVar *cam_var = NULL;
        gchar *err_msg = NULL;
        gchar fname[GPIV_MAX_CHARS];

        unsigned char **lframe1 = NULL,  **lframe2 = NULL;
        guint16 **img1 = NULL, **img2 = NULL;
        GpivImagePar gpiv_image_par;
        gint i, j;
/*
 * from coriander:
 */
/*         chain_t **image_pipes; */
/*         Format7Info *format7_info; */
/*         UIInfo *uiinfos; */
/*         SelfIdPacket_t *selfids; */


        program_name = "gpiv_recimg";
        gpiv_img_parameters_set (&gpiv_image_par, FALSE);
        gpiv_image_par.x_corr = FALSE;
        command_args (argc, argv, fname, &gpiv_image_par);
        if (verbose) printf ("starting %s\n", program_name);

        if ((cam_var = gpiv_cam_get_camvar (verbose)) == NULL) {
                g_error("from: rec_image->main\n from: gpiv_cam_get_camvar\n %s", 
                        err_msg);
        }

/*
 * coriander: IsoStartThread->on_button_receive
 */
        cam_var->misc_info[0].format = FORMAT_VGA_NONCOMPRESSED;
        cam_var->misc_info[0].mode = MODE_640x480_MONO;
        cam_var->misc_info[0].framerate = FRAMERATE_7_5;
        if (dc1394_setup_capture (cam_var->camera[0].handle, 
                                  cam_var->camera[0].id, 
                                  cam_var->misc_info[0].iso_channel, 
                                  cam_var->misc_info[0].format, 
                                  cam_var->misc_info[0].mode, 
                                  /* maxspeed */ SPEED_400,
                                  cam_var->misc_info[0].framerate, 
                                  &capture)
            != DC1394_SUCCESS) {
/*                         info->receive_method=RECEIVE_METHOD_RAW1394; */
                dc1394_release_camera (cam_var->handle, &capture);
                raw1394_destroy_handle (cam_var->handle);
                g_error ("unable to setup camera-\n"
                         "check line %d of %s to make sure\n"
                         "that the video mode, framerate and format are\n"
                         "supported by your camera\n",
                         __LINE__,__FILE__);
        }  else {
                if (verbose) g_message("dc1394_setup_capture: succes");
        }

/* int */
/* dc1394_set_exposure(raw1394handle_t handle, nodeid_t node, */
/*                     unsigned int exposure); */

        dc1394_auto_on_off(cam_var->camera[0].handle, cam_var->camera[0].id,
                           FEATURE_EXPOSURE, 1);
                                          /* 0=off, nonzero=on */
/* 
 * set trigger mode
 * BUGFIX: disabled temporarly
 */
        if( dc1394_set_trigger_mode(cam_var->handle, 
                                    /* capture.node */ cam_var->camera[0].id, 
                                    TRIGGER_MODE_0)
            != DC1394_SUCCESS) {
                if (verbose) g_message("unable to set camera trigger mode\n");
        } else {
                if (verbose) g_message("dc1394_set_trigger_mode: succes");
        }
/*
 * report camera's feature set
 */
        if (verbose) dc1394_print_feature_set (&cam_var->feature_set[0]);

/*
 * test on single feature
 */
        if (verbose 
            && dc1394_query_basic_functionality (cam_var->handle, 
                                                 capture.node, 
                                                 &value)
            != DC1394_SUCCESS) {
                g_warning("dc1394_query_basic_functionality: failed\n");
        } else {
                if ((value>>(8*sizeof(quadlet_t)-19-1)) != 0) {
                        if (verbose) g_message("single shot supperted");
                } else {
                        if (verbose) g_message("single shot _NOT_ supperted");
                }
        }

/*
 * have the camera start sending us data
 * on_iso_start_clicked
 */
        if (dc1394_start_iso_transmission (cam_var->handle, capture.node)
            != DC1394_SUCCESS) {
                dc1394_release_camera(cam_var->handle, &capture);
                raw1394_destroy_handle(cam_var->handle);
                g_error("unable to start camera iso transmission");
        } else {
                if (verbose) g_message("dc1394_start_iso_transmission: succes");
        }

        sleep(1);
/*
 * capture one or two frames
 * on_service_iso_toggled->IsoStartThread
 */
        if (dc1394_single_capture (cam_var->handle, &capture) != DC1394_SUCCESS) {
                dc1394_release_camera(cam_var->handle, &capture);
                raw1394_destroy_handle(cam_var->handle);
                g_error("unable to capture a frame");
        } else {
                if (verbose) g_message("dc1394_single_capture: succes");
        }
        lframe1 = gpiv_ucmatrix (capture.frame_height, capture.frame_width);
        memcpy( lframe1[0], (const char *) capture.capture_buffer, 
                sizeof(char) * capture.frame_width * capture.frame_height);

        if (gpiv_image_par.x_corr) {
                if (dc1394_single_capture (cam_var->handle, &capture) != DC1394_SUCCESS) {
                        dc1394_release_camera(cam_var->handle, &capture);
                        raw1394_destroy_handle(cam_var->handle);
                        g_error("unable to capture a frame");
                } else {
                        if (verbose) g_message("dc1394_single_capture: succes");
                }
        }
        lframe2 = gpiv_ucmatrix (capture.frame_height, capture.frame_width);
        memcpy( lframe2[0], (const char *) capture.capture_buffer, 
                sizeof(char) * capture.frame_width * capture.frame_height);
/*
 * Stop data transmission
 */
        if (dc1394_stop_iso_transmission(cam_var->handle, capture.node) 
            != DC1394_SUCCESS) {
                g_warning("couldn't stop the camera?\n");
        } else {
                if (verbose) g_message("dc1394_stop_iso_transmissio: succes");
        }

/*
 * save image as 'Image.pgm'
 */
        if (use_stdin_stdout) {
                imagefile = stdout;
        } else {
                if((imagefile = fopen(fname, "wb")) == NULL) {
                        dc1394_release_camera(cam_var->handle, &capture);
                        raw1394_destroy_handle(cam_var->handle);
                        g_error( "Can't create %s", fname);
                }
        }
    
        if (verbose) g_message("width = %d  height = %d",
                                    capture.frame_width, 
                                    capture.frame_height);

/*
 * Printing to png image file
 */
        gpiv_image_par.nrows = capture.frame_height;
        gpiv_image_par.nrows__set = TRUE;
        gpiv_image_par.ncolumns = capture.frame_width;
        gpiv_image_par.ncolumns__set = TRUE;
/* BUGFIX: dc1394_query_format7_data_depth does not work */
/*         dc1394_query_format7_data_depth(cam_var->handle,  capture.node, */
/*                                         cam_var->misc_info[0].mode, */
/*                                         &gpiv_image_par.depth); */
/*         if (verbose) g_message ("%s:: depth from query = %d", program_name, gpiv_image_par.depth); */
        gpiv_image_par.depth = 8;
        gpiv_image_par.depth__set = TRUE;
        g_snprintf (gpiv_image_par.software, GPIV_MAX_CHARS, "%s", program_name);
        gpiv_image_par.software__set = TRUE;
        g_snprintf (gpiv_image_par.source, GPIV_MAX_CHARS, "Vendor: %s Model: %s", 
                    cam_var->camera[0].vendor,  cam_var->camera[0].model);
        gpiv_image_par.source__set = TRUE;
        if (verbose) gpiv_img_print_header (gpiv_image_par);
        
        img1 = gpiv_alloc_img(gpiv_image_par);
        if (gpiv_image_par.x_corr) {
                img2 = gpiv_alloc_img(gpiv_image_par);
        } else {
                img2 = img1;
        }

/*
 * copying capture_buffer to a 2-dimensional array of unsigned char
 * in order to get it in 2-dimensional array img1 of guint16, as used in the 
 * gpiv programs and libraries.
 */
        for (i = 0; i < gpiv_image_par.nrows; i++) {
                for (j = 0; j < gpiv_image_par.ncolumns; j++) {
                        img1[i][j] = (guint16) lframe1[i][j];
                }
        }
        gpiv_free_ucmatrix(lframe1);
        if (gpiv_image_par.x_corr) {
                for (i = 0; i < gpiv_image_par.nrows; i++) {
                        for (j = 0; j < gpiv_image_par.ncolumns; j++) {
                                img2[i][j] = (guint16) lframe2[i][j];
                        }
                }
                gpiv_free_ucmatrix(lframe2);
        }
 
        if ((err_msg = 
             gpiv_write_png_image (imagefile, &img1, &img2, &gpiv_image_par, TRUE))
            != NULL) {
                gpiv_error ("%s: %s\n", argv[0], err_msg);
        }
        if (use_stdin_stdout == FALSE) fclose (imagefile);

/*
 *  Close camera
 */
        dc1394_release_camera(cam_var->handle, &capture);

        free(cam_var->camera);
        free(cam_var->feature_set);
        free(cam_var->misc_info);
/*         free(image_pipes); */
/*         free(format7_info); */
/*         free(uiinfos); */
/*         free(selfids); */

        raw1394_destroy_handle(cam_var->handle);
        return 0;
}



void
GetFormat7Capabilities(raw1394handle_t handle, 
                       nodeid_t node, 
                       Format7Info *info
)
/*-----------------------------------------------------------------------------
 */
{
        int i, f, err;
        quadlet_t value;
  
        err = dc1394_query_supported_modes(handle, node, 
                                           FORMAT_SCALABLE_IMAGE_SIZE, &value);
        if (!err) {
                g_error("Could not query Format7 supported modes");
        } else {
                for (i = 0, f = MODE_FORMAT7_MIN; f < MODE_FORMAT7_MAX; f++, i++) {
                        info->mode[i].present = (value & (0x1<<(31-i)) );
/*
 * Check for mode presence before query
 */

                        if (info->mode[i].present) {
                                err=1;
                                err *= dc1394_query_format7_max_image_size
                                        (handle,node,f,
                                         &info->mode[i].max_size_x,
                                         &info->mode[i].max_size_y);
                                err *= dc1394_query_format7_unit_size
                                        (handle,node,f,&info->mode[i].step_x,
                                         &info->mode[i].step_y);
                                err *= dc1394_query_format7_image_position
                                        (handle,node,f,&info->mode[i].pos_x,
                                         &info->mode[i].pos_y);
                                err *= dc1394_query_format7_image_size
                                        (handle,node,f,&info->mode[i].size_x,
                                         &info->mode[i].size_y);
                                
                                err *= dc1394_query_format7_pixel_number
                                        (handle,node,f,&info->mode[i].pixnum);
                                err *= dc1394_query_format7_byte_per_packet
                                        (handle,node,f,&info->mode[i].bpp);
                                err *= dc1394_query_format7_packet_para
                                        (handle,node,f,&info->mode[i].min_bpp,
                                         &info->mode[i].max_bpp);
                                err *= dc1394_query_format7_total_bytes
                                        (handle, node, f,
                                         &info->mode[i].total_bytes);
                                
/*
  dc1394_query_format7_total_bytes(raw1394handle_t handle, nodeid_t node,
  unsigned int mode, unsigned long long int *total_bytes);
*/
/*
 * TODO: get color coding id
 */
                                err *= dc1394_query_format7_color_coding
                                        (handle,node,f,
                                         &info->mode[i].
                                         color_coding);
                                if (!err) g_error("Got a problem querying format7 capabilitie");
                        }
                }
        }

        info->edit_mode = MODE_FORMAT7_MIN;
}

#else /* ENABLE_CAM */

int 
main(int argc, 
     char *argv[]
     )
/*-----------------------------------------------------------------------------
 */ 
{
        g_error("%s: this is dead code as ENABLE_CAM is not set",
                argv[0]);
        return -1;
}

#endif /* ENABLE_CAM */
