/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002 Gerber van der Graaf

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * camera functions for data acquisition
 * $Log: rec_image.h,v $
 * Revision 1.1  2005/01/19 16:03:32  gerber
 * added rec_image.h
 *
 */

#ifndef DAC_CAM_H
#define DAC_CAM_H


/*
 * From coriander: definitions.h
 */
typedef struct _Format7ModeInfo
{

  dc1394bool_t present;

  unsigned int size_x;
  unsigned int size_y;
  unsigned int max_size_x;
  unsigned int max_size_y;

  unsigned int pos_x;
  unsigned int pos_y;

  unsigned int step_x;
  unsigned int step_y;

  unsigned int color_coding_id;
  quadlet_t color_coding;

  unsigned int pixnum;

  unsigned int bpp; // bpp is byte_per_packet
  unsigned int min_bpp;
  unsigned int max_bpp;

  unsigned int total_bytes;

} Format7ModeInfo;

typedef struct _Format7Info
{
  Format7ModeInfo mode[NUM_MODE_FORMAT7];
  int edit_mode;

} Format7Info;

typedef struct _UIInfo
{
  int want_to_display;
  int test_pattern;
  int all_auto;
  int all_man;
  int all_lock;
} UIInfo;



/*
 * From coriander: tools.h
 */
void
GetFormat7Capabilities(raw1394handle_t handle, 
		       nodeid_t node, 
		       Format7Info *info);

#endif /* DAC_CAM_H */
