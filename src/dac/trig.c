/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*-----------------------------------------------------------------------------
   gpiv_trig - triggers a (double Nd_YAGG) laser on a (CCD) camera

   Copyright (C) 2005 Gerber van der Graaf
                      Julio Soria

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <sys/types.h>
#include <getopt.h>
#include "system.h"
#ifdef ENABLE_TRIG
#include <rtai.h>
#endif
#include <sys/ioctl.h>
#include <sys/time.h>

#include <gpiv.h>


#ifdef ENABLE_TRIG

#define EXIT_FAILURE 1

/* char *xmalloc (); */
/* char *xrealloc (); */
/* char *xstrdup (); */


static void usage (int status);
/*
 * The name the program was run with, stripped of any leading path.
 */
char *program_name;

/* getopt_long return codes */
enum {DUMMY_CODE=129
};

/* Option flags and variables */

gboolean interactive;		/* --interactive */
gboolean verbose;		/* -- verbose output during running process */

static struct option const long_options[] =
{
    {"interactive", no_argument, 0, 'i'},
    {"verbose", no_argument, 0, 'V'},
    {"help", no_argument, 0, 'h'},
    {"version", no_argument, 0, 'v'},
    {NULL, 0, NULL, 0}
};

static int 
command_args (int argc, 
              char **argv);

static int 
interactive_func(void);

static int 
initialise (GpivTrigPar *trig_par
/* GpivTrigTime *ttime */
	    );

/*
 * Set all the option flags according to the switches specified.
 * Return the index of the first non-option argument.
 */

static int
command_args (int argc, 
              char *argv[]
              )
/*-----------------------------------------------------------------------------
 */
{
    int c;


    while ((c = getopt_long (argc, argv, 
                             "i"	/* interactive */
                             "V"	/* verbose */
                             "h"	/* help */
                             "v",	/* version */
                             long_options, (int *) 0)) != EOF) {
        switch (c) {
        case 'i':		/* --interactive */
            interactive = TRUE;
            break;
	case 'V':		/* --verbose */
            verbose = TRUE;
            break;
	case 'v':
            printf ("gpiv_trig %s\n", VERSION);
            exit (0);
	case 'h':
            usage (0);
	default:
            usage (EXIT_FAILURE);
	}
    }

    return 0;
}



static void
usage (int status
       )
/*-----------------------------------------------------------------------------
 */
{
    printf (_("%s - \
triggers a (double Nd_YAGG) laser on a CCD camera\n"), program_name);
    printf (_("Usage: %s [OPTION]... [FILE]...\n"), program_name);
    printf (_("\
Options:\n\
  -i, --interactive          prompt for confirmation\n\
  -V, --verbose              print more information\n\
  -h, --help                 display this help and exit\n\
  -v, --version              output version information and exit\n\
"));
    exit (status);
}



static int 
initialise (GpivTrigPar *trig_par
/*             GpivTrigTime *ttime */
	    ) 
/*-----------------------------------------------------------------------------
 */
{
    int ok = 0;
    float dtime, acq_time, increment;
    int mode, cycles;

/*
 * private user input variables
 * hardwire some of the timings ....
 */

    if (verbose) printf("\nEntering initialise\n");
    trig_par->ttime.laser_trig_pw = (RTIME)(0.02*1000*1000);  /* laser trigger pulse width 0.02ms */
    trig_par->ttime.time2laser    = (RTIME)(0.19*1000*1000);  /* time from laser trigger->Hi
					      until laser pulse: 0.19ms  */


    printf("\n\tmode:\n");
    printf("\t\t1 - indefinite periodic\n");
    printf("\t\t2 - duration\n");
    printf("\t\t3 - interrupt one shot\n");
    printf("\t\t4 - interrupt periodic\n");
    printf("\t\t5 - incremented dt\n");
    printf("\t\t6 - double exposure\n");
    printf("\t\t9 - exit\n");
    printf("\t(1/2/3/4/5/6/9) > ");
    scanf("%d", &mode);
    trig_par->ttime.mode = mode;
    
    if (trig_par->ttime.mode == 9) {
        return 1;
    }
    
/*
 * ask for other timings ....
 */
    printf("\n\tAcquisition parameters: \n");

    printf("\ttime between 2 laser exposures in ms > ");
    scanf("%f", &dtime);
    trig_par->ttime.dt = (RTIME)1000 * 1000 * dtime;
    
/* gpiv_trig_test_parameter(GpivTrigPar * trig_par */
/*                         ); */
    if ((trig_par->ttime.mode == GPIV_TIMER_MODE__PERIODIC) 
        || (trig_par->ttime.mode == GPIV_TIMER_MODE__DURATION) 
        || (trig_par->ttime.mode == GPIV_TIMER_MODE__TRIGGER_IRQ) 
        || (trig_par->ttime.mode == GPIV_TIMER_MODE__DOUBLE)) {
        printf("\tdouble frame acquisition period in ms (typ. 250 ms) > ");
        scanf("%f", &acq_time);
        trig_par->ttime.cam_acq_period = (RTIME)1000 * 1000 * acq_time;
    } else {
        trig_par->ttime.cam_acq_period  = (trig_par->ttime.dt) * 2;
    }
    
    if ((trig_par->ttime.mode == GPIV_TIMER_MODE__DURATION) 
        || (trig_par->ttime.mode == GPIV_TIMER_MODE__ONE_SHOT_IRQ) 
        || (trig_par->ttime.mode == GPIV_TIMER_MODE__TRIGGER_IRQ) 
        || (trig_par->ttime.mode == GPIV_TIMER_MODE__INCREMENT)) {
        printf("\tnumber of cycles > ");
        scanf("%d", &cycles);
        trig_par->ttime.cycles = cycles;
    } else {
        trig_par->ttime.cycles = 1;
    }
    
    if (trig_par->ttime.mode == GPIV_TIMER_MODE__INCREMENT) {
        printf("\tincrement in ms > ");
        scanf("%f", &increment);
        trig_par->ttime.increment = (RTIME)1000 * 1000 * increment;
    } else {
        trig_par->ttime.increment = 1;
    }

    ok = 1;
    if (verbose) printf("\nLeaving initialise\n");
    return ok;
}



static int
interactive_func (void
		  )
/*-----------------------------------------------------------------------------
 */
{
    GpivTrigPar trig_per;
/*     GpivTrigTime ttime = trig_per.ttime; */
    int    init, start, stop, error;    /* file pointer int */
    int    on = 1, off = 0, param_ok;
    
    if (verbose)   printf("\nEntering interactive_func\n");
    if (!gpiv_trig_openrtfs(&init, &start, &stop, &error)) {
        printf("\n\nFAIL IN FIFO OPEN, BYE ..... \n\n");
    }

    printf("\n\n\tProgram to control camera with external triggering & laser\n");

    while (1) {
        if(!initialise(&trig_per /* timing */))  {
            printf("\n\nPROBLEM INITIALISING PROGRAM, BYE ..... \n\n");
            exit(0);
        }

        if (trig_per.ttime.mode == 9) {
            printf("\n\tEx(c)iting program\n\n");
            break;
        }

        printf("\n\tWriting details to RT module\n\n");
/*
 * write the timing details to /dev/rtf/1
 */

        if((write(init, &trig_per.ttime, sizeof(GpivTrigTime))) < 0)  {
            printf("\n\nfail in  setting Camera and Laser timing ...\n\n");
            exit(0);
        }

        if((read(error, &param_ok, sizeof(int))) < 0) {
            printf("\n\nfail in receipt of confirmation ...\n\n");
            exit(0);
        }
        
        if (param_ok != 1) {
            printf("\n\tInvalid parameters entered \n");
        
        } else {
            printf("\n\tParameters :\n");
            printf("\t\tcam_acq_period: %lld\n", trig_per.ttime.cam_acq_period);
            printf("\t\tlaser_trig_pw:  %lld\n", trig_per.ttime.laser_trig_pw);
            printf("\t\ttime2laser:     %lld\n", trig_per.ttime.time2laser);
            printf("\t\tdt:             %lld\n", trig_per.ttime.dt);
            printf("\t\tmode:           %d\n", trig_per.ttime.mode);
            printf("\t\tcycles:         %d\n", trig_per.ttime.cycles);
            printf("\t\tincrement:      %d\n", (int) trig_per.ttime.increment);
            
            printf("\n\n\tStart on <enter>  >");
            getchar();
            getchar();

            if((write(start, &on, sizeof(int))) < 0) {
                printf("\n\nfail in starting camera and laser timing ...\n\n");
                exit(0);
            }

            printf("\n\tStop on <enter>   >");
            getchar();
            
            if((write(stop, &off, sizeof(int))) < 0)  {
                printf("\nfail in stopping camera and laser timing ...\n\n");
                exit(0);
            }
        }
        
    }
    
    if (verbose)   printf("\nLeaving interactive_func\n");
    return 0;
}



int
main (int argc, 
      char *argv[]
      )
/*-----------------------------------------------------------------------------
 * Main program to send trigger pulses 
 */
{
    int i, return_val = 0;
    
    program_name = argv[0];
    
    i = command_args (argc, argv);
    if (interactive) {
        return_val = interactive_func();
    }

    exit (return_val);
}



#else /* ENABLE_TRIG */

int 
main(int argc, 
     char *argv[]
     )
/*-----------------------------------------------------------------------------
 */ 
{
    g_error("%s: this is dead code as ENABLE_TRIG has not been set during building the program", argv[0]);
    return -1;
}

#endif /* ENABLE_TRIG */
